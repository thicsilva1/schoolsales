
    $(function(){
        carregarKits();

        function adicionarKit(kit){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i in items){
              if (items[i].id==kit.id && items[i].student==kit.student){
                return;
              }
            }
            items.push(kit);
            sessionStorage.setItem('kitCart', JSON.stringify(items));
          } else {
            sessionStorage.setItem('kitCart', JSON.stringify([kit]));
          }
          carregarKits();
        }

        function atualizarKit(kit){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i in items){
              if (items[i].id==kit.id && items[i].student==kit.student){
                items[i].quantity=kit.quantity;
                sessionStorage.setItem('kitCart', JSON.stringify(items));
                carregarKits();
                return;
              }
            }
            sessionStorage.setItem('kitCart', JSON.stringify(items));
            carregarKits();
          }
        }

        function removerKit(kit){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            var toUpdate = items.find(function(item){
              if (item.id==kit.id && item.student==kit.student){
                return item;
              }
            });
            items.splice(toUpdate,1);
            sessionStorage.setItem('kitCart', JSON.stringify(items));
            carregarKits();
          }
        }

        function carregarKits(){
          $('span[data-student="{{$student->id}}"]').each(function(){
            $(this).html('0');
          })
          if (sessionStorage.getItem('kitCart')){
            var student = $('.btn.select').first().data('student');
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i=0; i<items.length; i++){
              for (var j in items[i].products){
                $('span[data-product="'+ items[i].products[j].id +'"][data-kit="'+ items[i].products[j].kit+'"][data-student="'+ items[i].products[j].student+'"]').html(items[i].products[j].quantity);
              }
              $('input[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').val(items[i].quantity);
              $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').data('selected', true);
              $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').data('quantity', items[i].quantity);
              $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').attr('disabled', false);
              $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').addClass('btn-success');
              $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').html('<i class="fa fa-check" aria-hidden="true"></i>Selecionado');
              var button = $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]');
              var prodAdd = button.closest('.row').find('button.fa-plus-square');
              var prodDel = button.closest('.row').find('button.fa-minus-square');
              prodAdd.each(function(){
                $(this).attr('disabled', false);
              });
              prodDel.each(function(){
                $(this).attr('disabled', false);
              });


            }
          }
          carregarTotal();
        }

        function adicionarProduto(produto){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i in items){
              if (items[i].id==produto.kit && items[i].student==produto.student){
                for (var j in items[i].products) {
                  if (items[i].products[j].id==produto.id){
                    console.log(items[i].products[j]);
                    items[i].products[j].quantity++;
                    sessionStorage.setItem('kitCart', JSON.stringify(items));
                    carregarKits();
                    return ;
                  }
                }
                items[i].products.push(produto);
              }
            }
            sessionStorage.setItem('kitCart', JSON.stringify(items));
          }
          carregarKits();
        }

        function subtrairProduto(produto){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i in items){
              if (items[i].id==produto.kit && items[i].student==produto.student){
                for (var j in items[i].products) {
                  if (items[i].products[j].id==produto.id){
                    if (items[i].products[j].quantity>1){
                      items[i].products[j].quantity--;
                    } else {
                      items[i].products.splice(items[i].products[j],1);
                    }
                    sessionStorage.setItem('kitCart', JSON.stringify(items));
                    carregarKits();
                    return ;
                  }
                }

              }
            }
          }
          carregarKits();
        }

        function carregarTotal(){
          var total=0;
          var inputs='';
          var student = $('.btn.select').first().data('student');
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i in items){
              if(items[i].student==student){
                for (var j in items[i].products){
                  total += parseFloat(items[i].products[j].value  * items[i].products[j].quantity);
                  inputs += '<input type="hidden" name="prod_ids['+items[i].id+'][]" value="'+items[i].products[j].id+'">';
                  inputs += '<input type="hidden" name="prod_quantity['+items[i].id+'][]" value="'+items[i].products[j].quantity+'">';
                  inputs += '<input type="hidden" name="prod_value['+items[i].id+'][]" value="'+items[i].products[j].value+'">';
                }
                total += parseFloat(items[i].value  * items[i].quantity);
                inputs += '<input type="hidden" name="kit_ids[]" value="'+items[i].id+'">';
                inputs += '<input type="hidden" name="kit_quantity[]" value="'+items[i].quantity+'">';
                inputs += '<input type="hidden" name="kit_student[]" value="'+items[i].student+'">';
                inputs += '<input type="hidden" name="kit_value[]" value="'+items[i].value+'">';
              }
            }
          }
          if (sessionStorage.getItem('produtoCart')){
            var items = JSON.parse(sessionStorage.getItem('produtoCart'));
            for (var i=0; i<items.length; i++){
              if(items[i].student==student ){
                total += parseFloat(items[i].value  * items[i].quantity);
                inputs += '<input type="hidden" name="prod_ids[]" value="'+items[i].id+'">';
                inputs += '<input type="hidden" name="prod_quantity[]" value="'+items[i].quantity+'">';
                inputs += '<input type="hidden" name="prod_value[]" value="'+items[i].value+'">';
              }
            }
          }
          inputs += '<input type="hidden" name="total" value="'+total+'">';
          $('#total').html(realFormat(total.toFixed(2)));
          $('#input-hiddens').html(inputs);
        }

        function realFormat(numero){
          var numero = numero.split('.');
          numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
          return numero.join(',');
        }
    });
