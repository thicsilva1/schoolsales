<?php

namespace App\Console\Commands;

use App\Models\DatabaseLog;
use App\Models\Student;
use Illuminate\Console\Command;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class RestoreDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:restore {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restaura um backup do site';
    protected $databaseLog;
    protected $student;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DatabaseLog $databaseLog)
    {
        parent::__construct();
        $this->databaseLog = $databaseLog;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ds = DIRECTORY_SEPARATOR;
        $process = new Process(sprintf(
            'mysql -u%s -p%s < %s',
            config('database.connections.mysql.username'),
            config('database.connections.mysql.password'),
            storage_path('app') . $ds . $this->argument('file')
        ));
        try {

            $process->mustRun();
            $total = Student::all()->count();
            $this->databaseLog->create([
                'type' => 'Restauração',
                'status' => 'Completo',
                'description' => 'Restaurado ' . $total . ' alunos',
            ]);
            $this->info('The restore has been proceed successfully.');
        } catch (ProcessFailedException $exception) {
            $this->databaseLog->create([
                'type' => 'Restauração',
                'status' => 'Erro',
                'description' => $exception->getMessage(),
            ]);
            $this->error('The restore process has been failed.');
        }
    }
}
