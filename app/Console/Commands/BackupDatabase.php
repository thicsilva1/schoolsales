<?php

namespace App\Console\Commands;

use App\Models\DatabaseLog;
use Ifsnop\Mysqldump as IMysqldump;
use Illuminate\Console\Command;

class BackupDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Executa um backup';
    protected $process;
    protected $filename;
    protected $databaseLog;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DatabaseLog $databaseLog)
    {
        parent::__construct();
        $this->databaseLog = $databaseLog;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ds = DIRECTORY_SEPARATOR;
        $user = config('database.connections.mysql.username');
        $pass = config('database.connections.mysql.password');
        $host = config('database.connections.mysql.host');
        $port = config('database.connections.mysql.port');
        $db = config('database.connections.mysql.database');
        $filename = storage_path('app' . $ds . 'backups' . $ds . 'backup-' . date('Y-m-d_H-i-s') . '.sql');
        try {

            $dump = new IMysqldump\Mysqldump('mysql:host=' . $host . ';dbname=' . $db . ';port=' . $port, $user, $pass);
            $dump->start($filename);

            $this->databaseLog->create([
                'type' => 'Backup',
                'status' => 'Completo',
            ]);

            $this->info('The backup has been proceed successfully.');
        } catch (Exception $exception) {

            $this->databaseLog->create([
                'type' => 'Backup',
                'status' => 'Erro',
                'description' => $exception->getMessage(),
            ]);

            $this->error('The backup process has been failed.');
        }
    }
}
