<?php

namespace App\Console\Commands;

use App\Models\Coupon;
use App\Models\DatabaseLog;
use App\Models\Kit;
use App\Models\KitOrder;
use App\Models\Log;
use App\Models\NotReceive;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Relative;
use App\Models\Student;
use App\Models\Team;
use App\Models\Text;
use Illuminate\Console\Command;

class EmptyDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:empty';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all content of tables';

    protected $databaseLog;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DatabaseLog $databaseLog)
    {
        parent::__construct();
        $this->databaseLog = $databaseLog;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Coupon::truncate();
        Kit::truncate();
        KitOrder::truncate();
        Log::truncate();
        NotReceive::truncate();
        Order::truncate();
        OrderProduct::truncate();
        Payment::truncate();
        Product::truncate();
        Relative::truncate();
        Student::truncate();
        Team::truncate();        
        $this->databaseLog->create([
            'type' => 'Limpeza',
            'status' => 'Completo',
        ]);
    }
}
