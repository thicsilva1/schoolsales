<?php

namespace App\Exports;

use App\Models\KitOrder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Events\AfterSheet;

class OrdersExport implements FromQuery, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct(array $ids)
    {
        $this->ids = $ids;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:AW1'; // All headers
                $event->sheet->styleCells($cellRange)->getFont()->setSize(14);
            },
        ];
    }

    public function query()
    {
        return KitOrder::whereIn('order_id', $this->ids)->with('products');
    }

    public function map($kitOrder): array
    {

        return [
            $kitOrder->order->relative->name,
            $kitOrder->order->relative->email,
            $kitOrder->order->relative->phone,
            $kitOrder->order->relative->cpf,
            $kitOrder->student->name,
            $kitOrder->student->original_name,
            $kitOrder->student->team->school,
            $kitOrder->student->team->name,
            //$kitOrder->student->type,
            $kitOrder->student->period,
            $kitOrder->order->id,
            $kitOrder->order->created_at->format('d/m/Y H:i:s'),
            $kitOrder->order->payment_status,
            'R$ ' . number_format($kitOrder->order->total,2,',','.'),
            optional($kitOrder->order->coupon)->code,
            $kitOrder->kit->name,
            $kitOrder->quantity,
            'R$ ' . number_format($kitOrder->value,2,',','.'),
            $kitOrder->products->map(function($product){
                return [
                    'Produto' => $product->product->name,
                    'Quantidade' => $product->quantity,
                    'Valor' => 'R$ ' . number_format($product->value, '2', ',', '.')
                ];
            }),
        ];
    }

    public function headings(): array
    {
        return [
            'Nome Comprador',
            'Email',
            'Telefone',
            'CPF',
            'Nome Aluno',
            'Nome Original',
            'Escola',
            'Turma',
            //'Tipo',
            'Período',
            '# Pedido',
            'Data Compra',
            'Status',
            'Valor Total',
            'Cupom Desconto',
            'Kit',
            'Quantidade',
            'Valor',
            'Produtos Adicionais',
        ];
    }

}
