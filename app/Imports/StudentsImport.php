<?php

namespace App\Imports;

use App\Models\Student;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class StudentsImport implements ToCollection, WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            // Select by sheet index
            0 => new StudentsImport(),
        ];
    }

    public function collection(Collection $rows)
    {
        $validation = Validator::make($rows->toArray(),[
            '*.0' => 'required|unique:students,name',
            '*.1' => 'required|exists:teams,id',
            '*.2' => 'required',
            '*.3' => 'required|unique:students,access_key',
            '*.4' => 'required',
            '*.7' => 'required',
        ], [
            '*.0.required' => 'Linha: :attribute, Nome precisa ser preenchido ' ,
            '*.0.unique' => 'Linha: :attribute, Aluno já existe',
            '*.1.required' => 'Linha: :attribute, Turma precisa ser preenchido',
            '*.1.exists' => 'Linha: :attribute, Turma não existe no banco de dados',
            '*.2.required' => 'Linha: :attribute, Período precisa ser preenchido',
            '*.3.required' => 'Linha: :attribute, Chave de acesso precisa ser preenchido',
            '*.3.unique' => 'Linha: :attribute, Chave de acesso já existe',
            '*.4.required' => 'Linha: :attribute, Desconto precisa ser preenchido',
            '*.7.required' => 'Linha: :attribute, Receber sem compromisso precisa ser preenchido',
        ]);

        $validation->validate();

        foreach ($rows as $row) {


            Student::create([
                'name' => $row[0],
                'team_id' => $row[1],
                'period' => mb_strtoupper($row[2]),
                'access_key' => $row[3],
                'discount' => $row[4],
                'photo' => $row[5],
                'original_name' => $row[6],
                'receive_without_commitment' => $row[7],
            ]);

        }
    }

}
