<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Setting;

class CheckStudent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $settings = Setting::find(1);

		if (!session()->has('student_id') || !$settings->site_enabled) {
            return redirect()->route('auth');
        }

        return $next($request);
    }
}
