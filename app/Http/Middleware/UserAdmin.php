<?php

namespace App\Http\Middleware;

use Closure;

class UserAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->isAdmin()){

            return $next($request);
        }
        session()->flash('alert', ['type' => 'error', 'message' => 'Você não possui permissão para acessar esse item!']);
        return redirect()->back();
    }
}
