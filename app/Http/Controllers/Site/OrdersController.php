<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use App\Models\KitOrder;
use App\Models\Log;
use App\Models\NotReceive;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Payment;
use App\Models\Relative;
use App\Models\Student;
use App\Models\Text;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function checkout(Request $request)
    {

        session()->put('items', $request->all());
        $items = session()->get('items');

        $student = Student::findOrFail(session()->get('student_id'));
        $relative = Relative::findOrFail(session()->get('relative_id'));

        if (isset($items['order'])) {
            $order = Order::findOrFail($items['order']);
        } else {
            $order = Order::create([
                'relative_id' => $relative->id,
                'type' => 'Venda',
                'payment_status' => 'CARRINHO',
            ]);

            $log = new Log;
            $log->student_id = $student->id;
            $log->relative_id = $relative->id;
            $log->description = 'Gerado novo pedido de compra: #' . $order->id;
            $log->save();
        }

        $ids = $items['kit_ids'];
        $kitQtys = $items['kit_quantity'];
        $kitValues = $items['kit_value'];

        $nr = NotReceive::where('relative_id', session()->get('relative_id'))
            ->where('student_id', session()->get('student_id'))->first();
        if ($nr) {
            $nr->delete();
        }

        foreach ($ids as $i => $kit) {
            $kitOrder = KitOrder::where('order_id', $order->id)
                ->where('kit_id', $kit)
                ->where('student_id', $student->id)
                ->first();
            if($kitOrder){
                $kitOrder->quantity = $kitOrder->quantity+$kitQtys[$i];
                $kitOrder->save();
            } else {
                $kitOrder = KitOrder::create([
                    'order_id' => $order->id,
                    'kit_id' => $kit,
                    'student_id' => $student->id,
                    'quantity' => $kitQtys[$i],
                    'value' => $kitValues[$i],
                ]);
            }

            if (isset($items['prod_ids'][$kit])) {
                $productIds = $items['prod_ids'][$kit];
                $productQtys = $items['prod_quantity'][$kit];
                $productValues = $items['prod_value'][$kit];
                foreach ($productIds as $j => $product) {
                    $orderProduct = OrderProduct::create([
                        'kit_order_id' => $kitOrder->id,
                        'product_id' => $product,
                        'quantity' => $productQtys[$j],
                        'value' => $productValues[$j],
                    ]);
                }
            }
        }

        $order->save();
        $this->sumTotal($order);

        session()->forget('items');

        session()->forget('order_id');
        session()->put('order_id', $order->id);

        return response()->json(['success' => true, 'msg' => $order]);
    }

    public function checkoutWithout(Request $request)
    {
        session()->put('items', $request->all());
        $items = session()->get('items');

        $student = Student::findOrFail(session()->get('student_id'));
        $relative = Relative::findOrFail(session()->get('relative_id'));

        $order = Order::create([
            'relative_id' => $relative->id,
            'type' => 'Receber Sem Compromisso',
            'payment_status' => 'SEM COMPROMISSO',
        ]);

        $log = new Log;
        $log->student_id = $student->id;
        $log->relative_id = $relative->id;
        $log->description = 'Gerado novo pedido SEM COMPROMISSO: #' . $order->id;
        $log->save();

        $ids = $items['kit_ids'];
        $kitQtys = $items['kit_quantity'];
        $kitValues = $items['kit_value'];

        foreach ($ids as $i => $kit) {
            $kitOrder = KitOrder::create([
                'order_id' => $order->id,
                'kit_id' => $kit,
                'student_id' => $student->id,
                'quantity' => $kitQtys[$i],
                'value' => 0,
            ]);

            if (isset($items['prod_ids'][$kit])) {
                $productIds = $items['prod_ids'][$kit];
                $productQtys = $items['prod_quantity'][$kit];
                $productValues = $items['prod_value'][$kit];
                foreach ($productIds as $j => $product) {
                    $orderProduct = OrderProduct::create([
                        'kit_order_id' => $kitOrder->id,
                        'product_id' => $product,
                        'quantity' => $productQtys[$j],
                        'value' => 0,
                    ]);
                }
            }
        }

        $order->subtotal = 0;

        $order->total = 0;

        $order->save();

        session()->forget('order_id');
        session()->put('order_id', $order->id);
        session()->forget('items');

        return response()->json(['success' => true, 'msg' => $order]);

    }

    public function viewOrder(Order $order)
    {
        session()->forget('cart');
        $order = Order::findOrFail($order->id);
        if ($order->relative_id != session()->get('relative_id')) {
            session()->flash('alert', ['type' => 'error', 'message' => 'Você não tem permissão para visualizar esse pedido!']);
            return redirect()->route('home');
        }
        $text = Text::where('reference', 'payment')->first();

        if ($order->type != 'Venda') {
            return view('site.checkout-without', compact('order', 'text'));
        }

        return view('site.checkout', compact('order', 'text'));
    }

    public function updateKit(Request $request, KitOrder $kit)
    {
        $kit = KitOrder::findOrFail($kit->id);
        $kit->quantity = $request->kit_quantity;
        $kit->save();
        $order = Order::findOrFail($kit->order_id);

        $this->sumTotal($order);
        return redirect()->back();
    }

    public function deleteKit(KitOrder $kit)
    {
        $kit = KitOrder::findOrFail($kit->id);
        $order = Order::findOrFail($kit->order_id);
        $kit->delete();

        if ($order->kits->count() == 0) {
            $order->delete();
            session()->forget('order_id');
            return redirect()->route('home');
        }

        $this->sumTotal($order);
        return redirect()->back();
    }

    public function updateProduct(Request $request, OrderProduct $product)
    {

        $product = OrderProduct::findOrFail($product->id);
        $product->quantity = $request->product_quantity;
        $product->save();
        $order = Order::findOrFail($product->kit->order_id);
        $this->sumTotal($order);
        return redirect()->back();
    }

    public function deleteProduct(OrderProduct $product)
    {
        $product = OrderProduct::findOrFail($product->id);
        $order = Order::findOrFail($product->kit->order_id);
        $product->delete();
        $this->sumTotal($order);
        return redirect()->back();
    }

    public function applyCoupon(Request $request)
    {
        $request->validate([
            'coupon' => 'required|string',
        ]);

        $coupon = Coupon::where('code', $request->coupon)
            ->first();

        if (!$coupon) {
            return response()->json(['error' => true, 'msg' => 'Cupom inválido!']);
        }

        if ($coupon->due_date < date('Y-m-d')) {
            return response()->json(['error' => true, 'msg' => 'Cupom expirou e não pode ser aplicado.']);
        }

        if ($coupon->orders->count() >= $coupon->max_amount) {
            return response()->json(['error' => true, 'msg' => 'Cupom já excedeu o limite estabelecido.']);
        }

        $order = Order::findOrFail($request->order);

        $order->coupon_id = $coupon->id;

        $order->save();

        $this->sumTotal($order);

        return response()->json(['success' => true, 'msg' => $coupon]);
    }

    public function notReceive(Request $request)
    {

        $nr = NotReceive::where('relative_id', $request->relative_id)
            ->where('student_id', $request->student_id)->first();

        if (!$nr) {
            $nr = NotReceive::create($request->all());
        }

        return view('site.notreceive', compact('nr'));
    }

    public function continueBuy(Request $request)
    {
        $student = Student::where('access_key', $request->key)->first();
        if (!$student) {
            return response()->json(['error' => true, 'msg' => 'Chave fornecida é inválida!']);
        }

        if ($student->id == session()->get('student_id')) {
            return response()->json(['error' => true, 'msg' => 'Você precisa digitar uma chave diferente, essa chave refere-se ao aluno atual.']);
        }

        /*  $relative = $student->relatives()->find(session()->get('relative_id'));
        if (!$relative) {
        return response()->json(['error' => true, 'msg' => 'Desculpe, mas seu cadastro não está associado a esse aluno.']);
        } */

        session()->forget('order_id');
        session()->put('order_id', $request->order);

        session()->forget('student_id');
        session()->put('student_id', $student->id);
        return response()->json(['success' => true]);
    }

    public function cancelOrder(Request $request)
    {
        $order = Order::findOrFail($request->order);
        $order->delete();

        session()->forget('order_id');
        session()->forget('cart');

        return response()->json(['success' => true]);
    }

    public function createPagSeguro(Request $request)
    {
        $payment = Payment::where('order_id', $request->order)->first();
        if ($payment) {
            $this->updateStatusPagSeguro($payment);
            return response()->json(['exists' => true]);
        }

        $order = Order::findOrFail($request->order);
        
        $data['email'] = env('PAGSEGURO_EMAIL');
        $data['token'] = env('PAGSEGURO_TOKEN');
        $data['currency'] = 'BRL'; 
        $count = 1;
        foreach ($order->kits as $kit){                    
            $data['itemId' . $count] = md5($kit->id . microtime());
            $data['itemDescription' . $count] = $kit->kit->name;
            $data['itemAmount' . $count] = $kit->value;
            $data['itemQuantity' . $count] = $kit->quantity;
            $count++;
            if ($kit->products->count()>0){
                foreach ($kit->products as $product){
                    $data['itemId' . $count] = md5($product->id . microtime());
                    $data['itemDescription' . $count] = $product->product->name;
                    $data['itemAmount' . $count] = $product->value;
                    $data['itemQuantity' . $count] = $product->quantity;
                    $count++;
                }
            }
        }

        if ($order->coupon){
            $data['extraAmount'] = number_format($order->discount * -1, 2);
        }
        
        $data['notificationUrl'] = route('pagseguro.notify');        

        $query = http_build_query($data);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('PAGSEGURO_URL_CHECKOUT'),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded;',
                'charset=ISO-8859-1',
            ),
            CURLOPT_POST => true,
            // CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => $query,
        ));
        $ret = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return response()->json(['error' => true, 'msg' => $err]);
        }        

        $xml = simplexml_load_string($ret);

        return response()->json(['success' => true, 'msg' => $xml]);

    }

    public function createPayment(Request $request)
    {
        $order = Order::findOrFail($request->order);
        $payment = Payment::create([
            'status' => 1,
            'order_id' => $order->id,
            'transaction_code' => $request->transaction,
        ]);
        $order->payment_id = $payment->id;
        $order->save();
        $status = $this->updateStatusPagSeguro($payment);

        return response()->json(['success' => true, 'msg' => $status]);

    }

    public function confirmedPagSeguro(Order $order)
    {
        $order = Order::findOrFail($order->id);
        return view('site.pagseguro', compact('order'));
    }

    protected function updateStatusPagSeguro(Payment $payment)
    {
        $payment = Payment::findOrFail($payment->id);
        $order = Order::where('payment_id', $payment->id)->first();

        if (!$order) {
            return null;
        }

        $data['email'] = env('PAGSEGURO_EMAIL');
        $data['token'] = env('PAGSEGURO_TOKEN');
        $query = http_build_query($data);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('PAGSEGURO_URL_TRANSACTION') . '/' . $payment->transaction_code . '?' . $query,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded;',
                'charset=ISO-8859-1',
            ),
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
        ));
        $ret = curl_exec($curl);
        curl_close($curl);
        $xml = simplexml_load_string($ret);
        $payment->status = $xml->status;
        $payment->save();
        switch ($payment->status) {
            case 3:
                $order->payment_status = 'PAGO';
                break;
            case 7:
                $order->payment_status = 'CANCELADO';
                break;
            default:
                $order->payment_status = 'PENDENTE';
                break;
        }
        $order->save();

        return $xml;
    }

    public function queryPagSeguro()
    {
        $payments = Payment::where('status', '<>', 3)
            ->where('status', '<>', 7)->get();
        $data = array();
        foreach ($payments as $payment) {
            $data[] = $this->updateStatusPagSeguro($payment);
        }
        return response()->json(['success' => true]);
    }

    public function updateWithNotification(Request $request)
    {
        $data['email'] = env('PAGSEGURO_EMAIL');
        $data['token'] = env('PAGSEGURO_TOKEN');
        $query = http_build_query($data);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('PAGSEGURO_URL_TRANSACTION') . '/notifications/' . $request->notificationCode . '?' . $query,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded;',
                'charset=ISO-8859-1',
                'access-control-allow-origin: https://sandbox.pagseguro.uol.com.br',
            ),
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
        ));
        $ret = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return response()->json(['error' => true]);
        }

        $xml = simplexml_load_string($ret);

        if ($xml->code) {
            $payment = Payment::where('transaction_code', $xml->code)->first();
            if (!$payment) {
                return response()->json(['msg' => 'Pagamento ainda não gravado no banco de dados'], 204);
            }
            $order = Order::where('payment_id', $payment->id)->first();
            $payment->status = $xml->status;
            $payment->save();
            switch ($payment->status) {
                case 3:
                    $order->payment_status = 'PAGO';
                    break;
                case 7:
                    $order->payment_status = 'CANCELADO';
                    break;
                default:
                    $order->payment_status = 'PENDENTE';
                    break;
            }
            $order->save();
        }

        return response()->json(['success' => true]);
    }

    protected function sumTotal(Order $order)
    {
        $order = Order::findOrFail($order->id);
        $order->subtotal = $order->kits->sum(function ($kit) {
            return ($kit->value * $kit->quantity) + $kit->products->sum(function ($product) {
                return $product->value * $product->quantity;
            });
        });
        if (!empty($order->coupon_id)) {
            if ($order->coupon->type == "Valor") {
                $order->discount = $order->coupon->value;
            } else {
                $order->discount = $order->subtotal * ($order->coupon->value / 100);
            }
        }
        $order->total = $order->subtotal - $order->discount;
        $order->save();
    }

}
