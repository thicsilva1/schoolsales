<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Kit;
use App\Models\Relative;
use App\Models\Setting;
use App\Models\Student;
use App\Models\Product;
use App\Models\Text;
use App\Models\Log;
use App\Models\NotReceive;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{

    public function index()
    {
        $id = session()->get('student_id');

        if (!session()->has('relative_id')){
            return redirect()->route('student');
        }

        $relative_id = session()->get('relative_id');

        $student = Student::find($id);
        $relative = Relative::find($relative_id);

        $nr = NotReceive::where('relative_id', $relative->id)
            ->where('student_id', $student->id)
            ->first();
        if ($nr){
            $nr->delete();
        }


        $kits = Kit::where('status', true)->where(function ($query) use ($student) {
            if ($student->team->type=='Infantil') {
                $query->where('kindergarten', true);
            }
            if ($student->team->type=='Fundamental') {
                $query->where('elementary_school', true);
            }
            if ($student->team->type=='Médio') {
                $query->where('high_school', true);
            }
            $query->orWhere('all_type', true);
        })
        ->orderBy('display_order', 'asc')
        ->paginate(10);

        $products = Product::where('status', true)->where(function ($query) use ($student) {
            if ($student->team->type=='Infantil') {
                $query->where('kindergarten', true);
            }
            if ($student->team->type=='Fundamental') {
                $query->where('elementary_school', true);
            }
            if ($student->team->type=='Médio') {
                $query->where('high_school', true);
            }
            $query->orWhere('all_type', true);
        })->orderBy('display_order')->get();


        $text = Text::where('reference', 'products')->first();
        $settings = Setting::find(1);
        return view('site.home', compact('student', 'relative', 'kits', 'text', 'settings', 'products'));
    }

    public function auth()
    {
        $settings = Setting::find(1);
        $text = Text::where('reference', 'login')->first();
        $textInactive = Text::where('reference', 'website_inactive')->first();
        return view('site.auth.login', compact('settings', 'text', 'textInactive'));
    }

    public function checkKey(Request $request)
    {
        $key = $request->key;
        $student = Student::where('access_key', $key)->where('status', 1)->first();
        if (!$student) {
            return response()->json(['success' => false], 422);
        }
        session()->put('student_id', $student->id);
        return response()->json(['success' => true, 'message' => $student]);
    }

    public function confirmStudent(Student $student)
    {
        return view('site.confirmar-aluno', compact('student'));
    }

    public function getStudent()
    {
        $id = session()->get('student_id');
        $student = Student::find($id);
        $relative = $student->relatives()->first();
        return view('site.confirm', compact('student', 'relative'));
    }

    public function createRelative(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'cpf' => 'required',
            'phone' => 'required',
        ]);

        $student =  Student::find(session()->get('student_id'));
        $relative = Relative::where('email', $request->email)->first();
        if (!$relative) {
            $relative = new Relative;
            $relative->fill($request->all());
            $relative->save();
        }

        $relative->fill($request->all());

        $relative->save();
        if (!$relative->students->find($student->id)){
            $relative->students()->attach($student);
            $log = new Log;
            $log->student_id = $student->id;
            $log->relative_id = $relative->id;
            $log->description = $relative->name . ' foi adicionado como responsável ao aluno ' . $student->name;
            $log->save();
        }

        $log = new Log;
        $log->student_id = $student->id;
        $log->relative_id = $relative->id;
        $log->description = 'Acesso com a chave: ' . $student->access_key;
        $log->save();

        session()->put('relative_id', $relative->id);
        $nr = NotReceive::where('relative_id', $relative->id)
            ->where('student_id', $student->id)
            ->first();

        if($nr){
            $text = Text::where('reference', 'not-receive')->first();
            return view('site.load-notreceive', compact('text'));
        }

        $cart = Order::where('relative_id', $relative->id)
            ->where('payment_status', 'CARRINHO')
            ->orderBy('created_at', 'desc')
            ->first();
        if ($cart){
            session()->put('cart', $cart->id);
        }

        return redirect()->route('home');
    }

    public function recoveryKey(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'resp_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => ['msg' => 'Verifique se os campos foram preenchidos corretamente.',
                $validator->errors()]]);
        }

        $student = Student::where('name', $request->name)->first();

        if (!$student) {
            return response()->json(['errors' => ['msg' => 'Aluno não encontrado!']]);
        }

        $mail['to'] = $request->email;
        $mail['name'] = $request->name;
        $mail['resp_name'] = $request->resp_name;
        $mail['key'] = $student->access_key;
        try {
            \Mail::send('site.email', $mail, function ($message) use ($mail) {
                $message->to($mail['to']);
                $message->subject('Solicitação de chave de acesso');
            });
        } catch (Exception $e) {
            return response()->json(['errors' => ['msg' => 'Erro ao enviar email solicitado. Tente novamente mais tarde.']]);
        }
        return response()->json(['success' => 1]);

    }

    public function signout()
    {
        session()->forget('student_id');
        session()->forget('relative_id');
        session()->forget('order_id');
        session()->forget('cart');
        return redirect()->route('home');
    }

    public function updateStudent(Request $request, Student $student)
    {
        $request->validate([
            'name' => 'required',
            'original_name' => 'required',
        ]);

        $student = Student::findOrFail($student->id);

        if ($student->name == $request->name) {
            return redirect()->back();
        }

        if ($student->original_name==''){
            $student->original_name = $request->original_name;
        }

        $student->name = $request->name;
        $student->save();

        return redirect()->back();
    }

}
