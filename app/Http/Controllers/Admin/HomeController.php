<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\Product;
use App\Models\Kit;
use App\Models\Order;
use App\Models\NotReceive;

class HomeController extends Controller
{
    public function index() {
		$students = Student::where('status', 1)->count();
		$products = Product::where('status', 1)->count();
        $ordersPaid = Order::where('payment_status', 'PAGO')->count();
        $ordersPending = Order::where('payment_status', '<>', 'PAGO')
            ->where('payment_status', '<>', 'SEM COMPROMISSO')->count();
        $ordersWithout = Order::where('payment_status', 'SEM COMPROMISSO')->count();
        $notReceive = NotReceive::count();
		$kits = Kit::where('status', 1)->count();
		return view('admin.dashboard', compact('students', 'products', 'kits', 'ordersPending', 'ordersPaid', 'notReceive', 'ordersWithout'));
    }

}
