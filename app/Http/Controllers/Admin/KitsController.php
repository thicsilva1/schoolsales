<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kit;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KitsController extends Controller
{
    public function index()
    {
        $kits = Kit::all();
        return view('admin.kits.index', compact('kits'));
    }

    public function create()
    {
        $products = Product::all();
        return view('admin.kits.create', compact('products'));
    }

    public function edit(Kit $kit)
    {
        $kit = Kit::findOrFail($kit->id);
        $products = Product::all();
        return view('admin.kits.edit', compact('kit', 'products'));
    }

    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'description' => 'required',
                'price' => 'required',
                'display_order' => 'required|numeric',
                'image' => 'required|image',
                'discount' => 'required'
            ], [
                'name.required' => 'Por favor, preencha o nome',
                'description.required' => 'Por favor, preencha a descrição',
                'price.required' => 'Por favor, preencha o preço',
                'display_order.required' => 'Por favor, preencha a ordem de exibição',
                'display_order.numeric' => 'Por favor, preencha com um número válido',
                'image.required' => 'Por favor, selecione uma imagem',
                'image.image' => 'Somente imagens são permitidas',
                'discount.required' => 'Por favor, preencha o desconto',
            ]
        );

        $validation->validate();

        $kit = Kit::create($request->all());

        if ($request->file('image') && $request->file('image')->isValid()){

            $file = $request->file('image');
            $extension = $file->extension();
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' .$extension;
            $path = public_path() . '/upload/images/kits';

            $file->move($path, $filename);
            $kit->image = $filename;
        }

        $kit->kindergarten = $request->has('kindergarten');
        $kit->elementary_school = $request->has('elementary_school');
        $kit->high_school = $request->has('high_school');
        $kit->all_type = $request->has('all_type');
        $kit->save();

        $kit->products()->attach($request->get('products'));

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('kits.index');
    }

    public function update(Request $request, Kit $kit)
    {

        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'description' => 'required',
                'price' => 'required',
                'display_order' => 'required|numeric',
                'image' => 'image',
                'discount' => 'required',
            ], [
                'name.required' => 'Por favor, preencha o nome',
                'description.required' => 'Por favor, preencha a descrição',
                'price.required' => 'Por favor, preencha o preço',
                'display_order.required' => 'Por favor, preencha a ordem de exibição',
                'display_order.numeric' => 'Por favor, preencha com um número válido',
                'image.required' => 'Por favor, selecione uma imagem',
                'image.image' => 'Somente imagens são permitidas',
                'discount.required' => 'Por favor, preencha o preço',
            ]
        );

        $validation->validate();

        $kit = Kit::findOrFail($kit->id);

        if ($request->file('image') && $request->file('image')->isValid()){
            $path = public_path() . '/upload/images/kits/';
            if(!empty($kit->image)){
                $file = $path . $kit->image;

                if (file_exists($file)){
                    unlink($file);
                }
            }
            $file = $request->file('image');
            $extension = $file->extension();
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;

            $file->move($path, $filename);

            $kit->image = $filename;
        }

        $kit->fill([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'old_price' => $request->old_price,
            'discount' => $request->discount,
            'display_order' => $request->display_order,
            'kindergarten' => $request->has('kindergarten'),
            'elementary_school' => $request->has('elementary_school'),
            'high_school' => $request->has('high_school'),
            'all_type' => $request->has('all_type'),
            'status' => $request->status,
            'allow_extra_products' => $request->allow_extra_products,
            'receive_without_commitment' => $request->receive_without_commitment
        ]);

        $kit->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('kits.index');
    }

    public function destroy(Kit $kit)
    {
        $kit = Kit::findOrFail($kit->id);

        if (!empty($kit->image)){

            $path = public_path() . '/upload/images/kits/';
            $file = $path . $kit->image;

            if (file_exists($file)){
                unlink($file);
            }
        }

        $kit->products()->detach();
        $kit->delete();

        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->route('kits.index');
    }

    public function destroyImage(kit $kit)
    {
        $kit = kit::findOrFail($kit->id);
        if(!empty($kit->image)) {
            $path = public_path() . '/upload/images/kits/';
            $file = $path . $kit->image;
            if (file_exists($file)){
                unlink($file);
            }
        }
        $kit->image=null;
        $kit->save();
        return response()->json(['success'=>true]);
    }

    public function updateStatus(Kit $kit)
    {
        $kit = Kit::findOrFail($kit->id);

        $kit->status = !$kit->status;
        $kit->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Status Atualizado!']);
        return redirect()->route('kits.index');
    }

}
