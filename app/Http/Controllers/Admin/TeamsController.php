<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Team;
use Illuminate\Support\Facades\Validator;

class TeamsController extends Controller
{
    public function index()
    {
        $teams = Team::all();
        return view('admin.teams.index', compact('teams'));
    }

    public function create()
    {
        return view('admin.teams.create');
    }

    public function edit(Team $team)
    {
        $team = Team::findOrFail($team->id);
        return view('admin.teams.edit', compact('team'));
    }

    public function store(Request $request)
    {

        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'school' => 'required',
                'image' => 'required|image',
            ], [
                'name.required' => 'Por favor, preencha o nome',
                'school.required' => 'Por favor, preencha o nome da escola',
                'image.required' => 'Por favor, selecione uma imagem',
                'image.image' => 'Somente imagens são permitidas,'
            ]
        );


        $validation->validate();

        $team = Team::create($request->all());

        $file = $request->file('image');
        $extension = $file->extension();
        $filename = md5($file->getClientOriginalName() . microtime()) . '.' .$extension;
        $path = public_path() . '/upload/images/teams';

        $file->move($path, $filename);

        $team->image = $filename;
        $team->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('teams.index');
    }

    public function update(Request $request, Team $team)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'school' => 'required',
                'image' => 'image',
            ], [
                'name.required' => 'Por favor, preencha o nome',
                'school.required' => 'Por favor, preencha o nome da escola',
                'image.image' => 'Somente imagens são permitidas,'
            ]
        );

        $validation->validate();

        $team = Team::findOrFail($team->id);

        $team->fill($request->all());

        if ($request->file('image')){
            $path = public_path() . '/upload/images/teams/';
            $file = $path . $team->image;
            if (file_exists($file)){
                unlink($file);
            }
            $file = $request->file('image');
            $extension = $file->extension();
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;

            $file->move($path, $filename);

            $team->image = $filename;
        }
        $team->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('teams.index');
    }

    public function destroy(Team $team)
    {
        $team = Team::findOrFail($team->id);

        if ($team->students->count()>0){
            session()->flash('alert', ['type' => 'error', 'message' => 'Não foi possível excluir! Existem alunos associados a essa turma.']);
            return redirect()->route('teams.index');
        }

        if(!empty($team->image)){
            $path = public_path() . '/upload/images/teams/';
            $file = $path . $team->image;
            if (file_exists($file)){
                unlink($file);
            }
        }

        $team->delete();

        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->route('teams.index');
    }

    public function updateStatus(Team $team)
    {
        $team = Team::findOrFail($team->id);

        $team->status = !$team->status;
        $team->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Status Atualizado!']);
        return redirect()->route('teams.index');
    }

    public function destroyImage(Team $team)
    {
        $team = Team::findOrFail($team->id);
        if(!empty($team->image)) {
            $path = public_path() . '/upload/images/teams/';
            $file = $path . $team->image;
            if (file_exists($file)){
                unlink($file);
            }
        }
        $team->image=null;
        $team->save();
        return response()->json(['success'=>true]);
    }
}
