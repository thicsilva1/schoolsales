<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function edit(Product $product)
    {
        $product = Product::findOrFail($product->id);
        return view('admin.products.edit', compact('product'));
    }

    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'price' => 'required',
                'display_order' => 'required|numeric',
                'photo' => 'required|image',
            ], [
                'name.required' => 'Por favor, preencha o nome',
                'price.required' => 'Por favor, preencha o preço',
                'display_order.required' => 'Por favor, preencha a ordem de exibição',
                'display_order.numeric' => 'Por favor, preencha com um número válido',
                'photo.required' => 'Por favor, selecione uma imagem',
                'photo.image' => 'Somente imagens são permitidas,'
            ]
        );

        $validation->validate();

        $product = Product::create($request->all());

        if ($request->file('photo') && $request->file('photo')->isValid()){
            $file = $request->file('photo');
            $extension = $file->extension();
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' .$extension;
            $path = public_path() . '/upload/images/products';
            $file->move($path, $filename);
            $product->photo = $filename;
        }
        $product->kindergarten = $request->has('kindergarten');
        $product->elementary_school = $request->has('elementary_school');
        $product->high_school = $request->has('high_school');
        $product->all_type = $request->has('all_type');

        $product->save();
        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('products.index');
    }

    public function update(Request $request, Product $product)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'price' => 'required',
                'display_order' => 'required|numeric',
                'photo' => 'image',
            ], [
                'name.required' => 'Por favor, preencha o nome',
                'price.required' => 'Por favor, preencha o preço',
                'display_order.required' => 'Por favor, preencha a ordem de exibição',
                'display_order.numeric' => 'Por favor, preencha com um número válido',
                'photo.required' => 'Por favor, selecione uma imagem',
                'photo.image' => 'Somente imagens são permitidas,'
            ]
        );

        $validation->validate();

        $product = Product::findOrFail($product->id);
        $product->fill($request->all());

        if ($request->file('photo') && $request->file('photo')->isValid()){

            $path = public_path() . '/upload/images/products/';
            if (!empty($product->photo)){
                $file = $path . $product->photo;
                if (file_exists($file)){
                    unlink($file);
                }
            }

            $file = $request->file('photo');
            $extension = $file->extension();
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;
            $file->move($path, $filename);
            $product->photo = $filename;
        }

        $product->kindergarten = $request->has('kindergarten');
        $product->elementary_school = $request->has('elementary_school');
        $product->high_school = $request->has('high_school');
        $product->all_type = $request->has('all_type');

        $product->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('products.index');
    }

    public function destroy(Product $product)
    {
        $product = Product::findOrFail($product->id);

        if(!empty($product->photo)) {
            $path = public_path() . '/upload/images/products/';
            $file = $path . $product->photo;
            if (file_exists($file)){
                unlink($file);
            }
        }

        $product->delete();

        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->route('products.index');
    }

    public function updateStatus(Product $product)
    {
        $product = Product::findOrFail($product->id);

        $product->status = !$product->status;
        $product->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Status Atualizado!']);
        return redirect()->route('products.index');
    }

    public function destroyImage(Product $product)
    {
        $product = Product::findOrFail($product->id);
        if(!empty($product->photo)) {
            $path = public_path() . '/upload/images/products/';
            $file = $path . $product->photo;
            if (file_exists($file)){
                unlink($file);
            }
        }
        $product->photo=null;
        $product->save();
        return response()->json(['success'=>true]);
    }
}
