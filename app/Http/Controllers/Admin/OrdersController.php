<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Kit;
use App\Models\KitOrder;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Relative;
use App\Models\Student;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        return view('admin.orders.index', compact('orders'));
    }

    public function export(Request $request)
    {
        $products = Product::all();
        if ($request->has('ids')) {
            $orders = Order::whereIn('id', $request->ids)->get();
            $kitOrders = KitOrder::whereIn('order_id', $request->ids)->get();
            $spreadsheet = new Spreadsheet();
            $spreadsheet->getProperties()
                ->setCreator("ABDesign - SchoolSales")
                ->setLastModifiedBy("ABDesign - SchoolSales")
                ->setTitle("Exportação de Pedidos")
                ->setSubject("Exportação de Pedidos")
                ->setDescription(
                    "Pedidos exportados através do painel administrativo da ABDesign"
                );
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1', 'Nome Comprador');
            $sheet->setCellValue('B1', 'Email');
            $sheet->setCellValue('C1', 'Telefone');
            $sheet->setCellValue('D1', 'CPF');
            $sheet->setCellValue('E1', 'Nome Aluno');
            $sheet->setCellValue('F1', 'Nome Original');
            $sheet->setCellValue('G1', 'Escola');
            $sheet->setCellValue('H1', 'Turma');
            $sheet->setCellValue('I1', 'Período');
            $sheet->setCellValue('J1', 'Pedido');
            $sheet->setCellValue('K1', 'Data Compra');
            $sheet->setCellValue('L1', 'Status');
            $sheet->setCellValue('M1', 'Valor Total');
            $sheet->setCellValue('N1', 'Cupom Desconto');
            $sheet->setCellValue('O1', 'Kit');
            $sheet->setCellValue('P1', 'Quantidade');
            $sheet->setCellValue('Q1', 'Valor');
            $letter = 'R';
            foreach ($products as $key => $product) {

                $sheet->setCellValue(str_replace($letter, $letter . '1', $letter), $product->name);
                $letter++;
            }

            $highestColumn = $sheet->getHighestColumn();
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => 'FF000000'],
                    ],
                ],
            ];
            $i = 2;

            foreach ($orders as $key => $order) {
                $ini = $i;
                $end = $i;
                $sheet->setCellValue('A' . $i, $order->relative->name);
                $sheet->setCellValue('B' . $i, $order->relative->email);
                $sheet->setCellValue('C' . $i, $order->relative->phone);
                $sheet->setCellValue('D' . $i, $order->relative->cpf);
                $sheet->setCellValue('J' . $i, $order->id);
                $sheet->setCellValue('K' . $i, $order->created_at->format('d/m/Y H:i:s'));
                $sheet->setCellValue('L' . $i, $order->payment_status);
                switch ($order->payment_status) {
                    case 'PAGO':
                        $sheet->getStyle('L'.$i)
                            ->getFont()->getColor()->setARGB('00076307');//verde
                        break;
                    case 'SEM COMPROMISSO':
                        $sheet->getStyle('L'.$i)
                            ->getFont()->getColor()->setARGB('009B59B6');//roxo
                        break;
                    case 'PENDENTE':
                        $sheet->getStyle('L'.$i)
                            ->getFont()->getColor()->setARGB('00E87E04');//laranja
                        break;
                    case 'CARRINHO':
                        $sheet->getStyle('L'.$i)
                            ->getFont()->getColor()->setARGB('004C87B9');//azul
                        break;
                    default:
                        $sheet->getStyle('L'.$i)
                            ->getFont()->getColor()->setARGB('00e7505a');//vermelho
                        break;
                }
                $sheet->setCellValue('M' . $i, 'R$ ' . number_format($order->total, 2, ',', '.'));
                $sheet->setCellValue('N' . $i, optional($order->coupon)->code);
                foreach ($order->kits as $k => $kitOrder) {
                    $sheet->setCellValue('E' . $i, $kitOrder->student->name);
                    $sheet->setCellValue('F' . $i, $kitOrder->student->original_name);
                    $sheet->setCellValue('G' . $i, $kitOrder->student->team->school);
                    $sheet->setCellValue('H' . $i, $kitOrder->student->team->name);
                    $sheet->setCellValue('I' . $i, $kitOrder->student->period);

                    $sheet->setCellValue('O' . $i, optional($kitOrder->kit)->name);
                    $sheet->setCellValue('P' . $i, $kitOrder->quantity);
                    $sheet->setCellValue('Q' . $i, 'R$ ' . number_format($kitOrder->value * $kitOrder->quantity, 2, ',', '.'));
                    $letter = 'R';
                    foreach ($products as $key => $product) {
                        $sheet->setCellValue($letter . $i, 0);
                        foreach ($kitOrder->products as $p) {
                            if ($p->product_id == $product->id) {
                                $sheet->setCellValue($letter . $i, $p->quantity);
                            }
                        }
                        $letter++;
                    }
                    $end = $i;
                    $i++;
                }
                $sheet->getStyle('A' . $ini . ':' . $highestColumn . $end)->applyFromArray($styleArray);
            }
            $sheet->getStyle('A1:D1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('fffff2cc');
            $sheet->getStyle('E1:I1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('fff4cccc');
            $sheet->getStyle('J1:N1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('ff00ffff');
            $sheet->getStyle('O1:Q1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('ffbf9000');

            $sheet->getStyle('R1:' . $highestColumn . '1')->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('ffb6d7a8');
            $sheet->getStyle('A1:' . $highestColumn . '1')->applyFromArray(['font'=>['bold' => true]]);

            $highestColumn++;

            for ($col = 'A'; $col != $highestColumn; $col++) {

                $sheet->getColumnDimension($col)->setAutoSize(true);
            }

            $writer = new Xlsx($spreadsheet);
            $writer->save('pedidos.xlsx');
            return response()->download('pedidos.xlsx')->deleteFileAfterSend();
        }
        session()->flash('alert', ['type' => 'error', 'message' => 'Nenhum registro foi selecionado!']);
        return back();
    }

    public function filter($filter)
    {
        $orders = Order::where('payment_status', $filter)->get();
        return view('admin.orders.index', compact('orders'));
    }

    public function edit(Order $order)
    {
        $order = Order::findOrFail($order->id);
        $relatives = Relative::all();
        $students = $order->relative->students;
        $kits = Kit::all();
        $xml = '';
        if ($order->payment_id) {
            $xml = $this->queryPagSeguro($order->payment_id);
        }
        return view('admin.orders.edit', compact('order', 'relatives', 'kits', 'students', 'xml'));
    }

    public function destroy(Order $order)
    {
        $order = Order::findOrFail($order->id);
        foreach ($order->kits as $kit) {
            $kit->delete();
        }
        $order->delete();
        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->route('orders.index');
    }

    public function pay(Order $order)
    {
        $order = Order::findOrFail($order->id);
        $order->payment_status = 'PAGO';
        $order->save();
        session()->flash('alert', ['type' => 'success', 'message' => 'Status de pagamento alterado']);
        return redirect()->back();
    }

    public function updateStatus(Request $request, Order $order)
    {
        $order = Order::findOrFail($order->id);
        $order->payment_status = $request->status;
        $order->save();
        session()->flash('alert', ['type' => 'success', 'message' => 'Status de pagamento alterado']);
        return redirect()->back();
    }

    public function getKits(Request $request)
    {
        if ($request->has('student')){
            $student = Student::findOrFail($request->student);
            $items = Kit::where('status', true)->where(function ($query) use ($student) {
                if ($student->team->type=='Infantil') {
                    $query->where('kindergarten', true);
                }
                if ($student->team->type=='Fundamental') {
                    $query->where('elementary_school', true);
                }
                if ($student->team->type=='Médio') {
                    $query->where('high_school', true);
                }
                $query->orWhere('all_type', true);
            })->orderBy('display_order')->get();
        } else {
            $items = Kit::all();
        }

        $data = view('admin.orders.select-kit', compact('items'))->render();

        return response()->json(['success' => true, 'options' => $data]);
    }

    public function storeKit(Request $request, Order $order)
    {
        $kit = Kit::findOrFail($request->ak_kit);
        $orderKit = KitOrder::create([
            'order_id' => $order->id,
            'kit_id' => $kit->id,
            'student_id' => $request->ak_student,
            'quantity' => $request->ak_quantity,
            'value' => $kit->price,
        ]);
        if ($orderKit->student->discount){
            $orderKit->value = $kit->price - ($kit->price * ($kit->discount/100));
            $orderKit->save();
        }

        $this->sumTotal($order);

        session()->flash('alert', ['type' => 'success', 'message' => 'Kit incluído!']);
        return redirect()->back();
    }

    public function updateKit(Request $request, Order $order)
    {
        $kit = KitOrder::findOrFail($request->ek_id);
        $k = Kit::findOrFail($request->ek_kit);

        foreach ($kit->products as $prod) {
            if (!$k->products()->find($prod->product_id)) {
                $prod->delete();
            }
        }

        $kit->fill([
            'student_id' => $request->ek_student,
            'kit_id' => $request->ek_kit,
            'quantity' => $request->ek_quantity,
        ]);

        $kit->save();

        $this->sumTotal($order);

        session()->flash('alert', ['type' => 'success', 'message' => 'Kit alterado!']);
        return redirect()->back();

    }

    public function destroyKit(Order $order, KitOrder $kit)
    {

        if ($order->kits->count() <= 1) {
            session()->flash('alert', ['type' => 'error', 'message' => 'Não é possível excluir! Pedido ficará sem valor!']);
            return redirect()->back();
        }
        $kit = KitOrder::findOrFail($kit->id);
        $kit->delete();

        $this->sumTotal($order);

        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->back();
    }

    public function getProducts(Request $request)
    {
        $student = Student::findOrFail($request->id);
        $items = Product::where('status', true)->where(function ($query) use ($student) {
            if ($student->team->type=='Infantil') {
                $query->where('kindergarten', true);
            }
            if ($student->team->type=='Fundamental') {
                $query->where('elementary_school', true);
            }
            if ($student->team->type=='Médio') {
                $query->where('high_school', true);
            }
            $query->orWhere('all_type', true);
        })->orderBy('display_order')->get();
        $data = view('admin.orders.select-kit', compact('items'))->render();

        return response()->json(['success' => true, 'options' => $data]);
    }

    public function storeProduct(Request $request, Order $order)
    {
        $product = Product::findOrFail($request->ap_product);
        $op = OrderProduct::create([
            'kit_order_id' => $request->ap_kit,
            'product_id' => $product->id,
            'quantity' => $request->ap_quantity,
            'value' => $product->price,
        ]);

        $this->sumTotal($order);
        session()->flash('alert', ['type' => 'success', 'message' => 'Produto incluído!']);
        return redirect()->back();
    }

    public function updateProduct(Request $request, Order $order)
    {
        $prod = OrderProduct::findOrFail($request->prod_id);
        $prod->fill([
            'prod_id' => $request->product_id,
            'quantity' => $request->product_quantity,
        ]);
        $prod->save();

        $this->sumTotal($order);

        session()->flash('alert', ['type' => 'success', 'message' => 'Produto alterado!']);
        return redirect()->back();
    }

    public function destroyProduct(Order $order, OrderProduct $product)
    {
        $product = OrderProduct::findOrFail($product->id);
        $product->delete();

        $this->sumTotal($order);

        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->back();
    }

    public function getStudents(Request $request)
    {
        $items = Student::where('status', true)->get();
        $data = view('admin.orders.select-kit', compact('items'))->render();

        return response()->json(['success' => true, 'options' => $data]);
    }

    public function updateRelative(Request $request, Order $order)
    {
        $order = Order::findOrFail($order->id);
        $order->relative_id = $request->relative_id;
        $order->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Responsável alterado!']);
        return redirect()->back();
    }

    protected function sumTotal(Order $order)
    {
        $order = Order::findOrFail($order->id);
        $order->subtotal = $order->kits->sum(function ($kit) {
            return ($kit->value * $kit->quantity) + $kit->products->sum(function ($product) {
                return $product->value * $product->quantity;
            });
        });
        $order->total = $order->subtotal - $order->discount;
        $order->save();
    }

    protected function queryPagSeguro($payment)
    {

        $payment = Payment::findOrFail($payment);
        $ret = cache($payment->transaction_code);
        if (!$ret){
            $data['email'] = env('PAGSEGURO_EMAIL');
            $data['token'] = env('PAGSEGURO_TOKEN');
            $query = http_build_query($data);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => env('PAGSEGURO_URL_TRANSACTION') . '/' . $payment->transaction_code . '?' . $query,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded;',
                    'charset=ISO-8859-1',
                ),
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
            ));
            $ret = curl_exec($curl);
            curl_close($curl);
            cache([$payment->transaction_code => $ret], now()->addHours(2));
        }
        $xml = new \SimpleXMLElement($ret);

        return $xml;
    }
}
