<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Support\Facades\Validator;

class CouponsController extends Controller
{
    public function index()
    {
        $coupons = Coupon::all();
        return view('admin.coupons.index', compact('coupons'));
    }

    public function create()
    {
        return view('admin.coupons.create');
    }

    public function edit(Coupon $coupon)
    {
        $coupon = Coupon::findOrFail($coupon->id);
        return view('admin.coupons.edit', compact('coupon'));
    }

    public function destroy(Coupon $coupon)
    {
        $coupon = Coupon::findOrFail($coupon->id);
        $coupon->delete();

        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->back();
    }

    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'code' => 'required|unique:coupons,code',
                'value' => 'required',
                'max_amount' => 'required|numeric',
                'due_date' => 'required|date|after:today',
            ], [
                'code.required' => 'Por favor, preencha o código',
                'code.unique' => 'Código já foi cadastrado',
                'value.required' => 'Por favor, preencha o valor do desconto',
                'max_amount.required' => 'Por favor, preencha o uso máximo',
                'max_amount.numeric' => 'Por favor, informe um número válido',
                'due_date.required' => 'Por favor, preencha a data de vencimento',
                'due_date.date' => 'Por favor, informe uma data válida',
                'due_date.after' => 'Data de vencimento precisa ser a partir de amanhã',
            ]
        );

        $validation->validate();

        $coupon = Coupon::create($request->all());

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('coupons.index');
    }

    public function update(Request $request, Coupon $coupon)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'code' => 'required|unique:coupons,code,' . $coupon->id,
                'value' => 'required',
                'max_amount' => 'required|numeric',
                'due_date' => 'required|date|after:today,' . $coupon->id,
            ], [
                'code.required' => 'Por favor, preencha o código',
                'code.unique' => 'Código já foi cadastrado',
                'value.required' => 'Por favor, preencha o valor do desconto',
                'max_amount.required' => 'Por favor, preencha o uso máximo',
                'max_amount.numeric' => 'Por favor, informe um número válido',
                'due_date.required' => 'Por favor, preencha a data de vencimento',
                'due_date.date' => 'Por favor, informe uma data válida',
                'due_date.after' => 'Data de vencimento precisa ser a partir de amanhã',
            ]
        );

        $validation->validate();
        $coupon = Coupon::findOrFail($coupon->id);

        $coupon->fill($request->all());
        $coupon->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('coupons.index');
    }

    public function updateStatus(Coupon $coupon)
    {
        $coupon = Coupon::findOrFail($coupon->id);

        $coupon->status = !$coupon->status;
        $coupon->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Status Atualizado!']);
        return redirect()->route('coupons.index');
    }
}
