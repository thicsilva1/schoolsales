<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function edit(User $user)
    {
        $user = User::findOrFail($user->id);
        return view('admin.users.edit', compact('user'));
    }

    public function editProfile()
    {
        $user = auth()->user();
        return view('admin.users.edit', compact('user'));
    }

    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6|confirmed',
                'privilege' => 'boolean',
                'status' => 'required|boolean',
                'profile_image' => 'image'
            ], [
                'name.required' => 'Por favor, preencha o nome',
                'email.required' => 'Por favor, preencha o email',
                'email.email' => 'Por favor, preencha um email válido',
                'email.unique' => 'Esse email já está sendo utilizado',
                'title.required' => 'Por favor, preencha o valor do desconto',
                'password.required' => 'Por favor, preencha a senha',
                'password.min' => 'Senha deve ter no mínimo 6 dígitos',
                'password.confirmed' => 'Senha não confere com a confirmação',
                'privilege.boolean' => 'Apenas Sim/Não são permitidos',
                'status.required' => 'Por favor, preencha o status',
                'status.boolean' => 'Apenas Sim/Não são permitidos',
            ]
        );

        $validation->validate();

        $user = User::create($request->all());

        if ($request->file('profile_image') && $request->file('profile_image')->isValid()){
            $path = public_path() . '/upload/images/users/';
            $file = $request->file('profile_image');
            $extension = $file->extension();
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;

            $file->move($path, $filename);
            $user->profile_image = $filename;
        }

        $user->password = bcrypt($user->password);
        $user->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->back();
    }

    public function update(Request $request, User $user)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,' . $user->id,
                'privilege' => 'boolean',
                'status' => 'required|boolean',
                'profile_image' => 'image'
            ], [
                'name.required' => 'Por favor, preencha o nome',
                'email.required' => 'Por favor, preencha o email',
                'email.email' => 'Por favor, preencha um email válido',
                'email.unique' => 'Esse email já está sendo utilizado',
                'title.required' => 'Por favor, preencha o valor do desconto',
                'privilege.boolean' => 'Apenas Sim/Não são permitidos',
                'status.required' => 'Por favor, preencha o status',
                'status.boolean' => 'Apenas Sim/Não são permitidos',
            ]
        );

        $validation->validate();

        $user = User::findOrFail($user->id);

        if ($request->file('profile_image') && $request->file('profile_image')->isValid()){

            $path = public_path() . '/upload/images/users/';

            if(!empty($user->profile_image)){

                $file = $path . $user->profile_image;
                if (file_exists($file)){
                    unlink($file);
                }
            }

            $file = $request->file('profile_image');
            $extension = $file->extension();
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;

            $file->move($path, $filename);
            $user->profile_image = $filename;
        }

        $user->fill($request->all());

        $user->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->back();
    }

    public function destroy(User $user)
    {
        $user = User::findOrFail($user->id);

        if(!empty($user->profile_image)){
            $path = public_path() . '/upload/images/users/';
            $file = $path . $user->profile_image;
            if (file_exists($file)){
                unlink($file);
            }
        }

        $user->delete();

        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->back();
    }

    public function destroyImage(User $user)
    {
        $user = User::findOrFail($user->id);
        if(!empty($user->profile_image)) {
            $path = public_path() . '/upload/images/users/';
            $file = $path . $user->profile_image;
            if (file_exists($file)){
                unlink($file);
            }
        }
        $user->profile_image=null;
        $user->save();
        return response()->json(['success'=>true]);
    }

    public function updateStatus(User $user)
    {
        $user = User::findOrFail($user->id);
        $user->status = !$user->status;
        $user->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Status Atualizado!']);
        return redirect()->back();
    }

    public function editPassword()
    {
        return view('admin.users.edit-password');
    }

    public function updatePassword(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'password' => 'required|min:6|confirmed',
            ], [
                'password.required' => 'Por favor, preencha a senha',
                'password.min' => 'Senha deve ter no mínimo 6 dígitos',
                'password.confirmed' => 'Senha não confere com a confirmação',
            ]
        );

        $validation->validate();

        $user = auth()->user();
        $user->password = bcrypt($request->password);
        $user->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Senha Atualizada!']);
        return redirect()->back();

    }

}
