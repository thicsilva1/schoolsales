<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NotReceive;
use App\Models\Student;
use App\Models\Relative;

class NotReceivesController extends Controller
{
    public function index()
    {
        $items = NotReceive::all();
        return view('admin.notreceive.index', compact('items'));
    }

    public function create()
    {
        $students = Student::all();
        $relatives = Relative::all();
        return view('admin.notreceive.create', compact('students', 'relatives'));
    }

    public function edit(NotReceive $nr)
    {
        $students = Student::all();
        $relatives = Relative::all();
        $nr = NotReceive::findOrFail($nr->id);
        return view('admin.notreceive.edit', compact('students', 'relatives', 'nr'));
    }

    public function store(Request $request)
    {
        $nr = NotReceive::where('relative_id', $request->relative_id)
            ->where('student_id', $request->student_id)->first();
        if ($nr){
            session()->flash('alert', ['type' => 'error', 'message' => 'Cadastro já existe!']);
            return redirect()->back();
        }
        $nr = NotReceive::create($request->all());
        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('notreceive.index');
    }

    public function update(Request $request, NotReceive $nr)
    {
        $nr = NotReceive::findOrFail($nr->id);
        $nr->fill($request->all());
        $nr->save();
        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('notreceive.index');
    }

    public function destroy(NotReceive $nr)
    {
        $nr = NotReceive::findOrFail($nr->id);
        $nr->delete();

        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->route('notreceive.index');
    }

}
