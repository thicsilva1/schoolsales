<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DatabaseLog;
use App\Models\Kit;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Student;
use App\Models\Team;
use Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Storage;

class SettingsController extends Controller
{
    public function index()
    {
        $setting = Setting::first();
        return view('admin.settings.index', compact('setting'));
    }

    public function update(Request $request)
    {
        $setting = Setting::first();
        $setting->fill($request->all());
        $setting->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->back();
    }

    public function backup()
    {
        $exitCode = \Artisan::call('db:backup');
        if (!$exitCode) {
            session()->flash('alert', ['type' => 'success', 'message' => 'Backup realizado com sucesso!']);
        } else {
            session()->flash('alert', ['type' => 'error', 'message' => 'Erro ao fazer backup!']);
        }
        $files = Storage::disk('local')->files('backups');
        return view('admin.settings.backup', compact('files'));
    }

    public function downloadBackup(Request $request)
    {
        if (!$request->has('filename')){
            return redirect()->back();
        }

        if (!Storage::disk('local')->exists($request->filename)){
            return redirect()->back();
        }

        return Storage::download($request->filename);
    }

    public function showRestore()
    {
        $files = Storage::disk('local')->files('backups');
        return view('admin.settings.restore', compact('files'));
    }

    public function restore(Request $request)
    {
        $exitCode = \Artisan::call('db:restore', ['file' => $request->filename]);

        if (!$exitCode) {
            session()->flash('alert', ['type' => 'success', 'message' => 'Backup restaurado com sucesso!']);
        } else {
            session()->flash('alert', ['type' => 'error', 'message' => 'Erro ao restaurar backup!']);
        }

        return redirect()->back();
    }

    public function showEmpty()
    {
        $fake = \Faker\Factory::create('pt_BR');
        $keyword = $fake->domainWord;
        return view('admin.settings.empty', compact('keyword'));
    }

    public function emptyDatabase(Request $request)
    {

        $validation = Validator::make(
            $request->all(),
            [
                'keyword' => 'required|confirmed',
            ], [
                'keyword.confirmed' => 'Palavra está incorreta.',
            ]
        );

        $validation->validate();

        $files = Storage::disk('local')->files('backups');
        $allow = false;
        foreach ($files as $file) {
            $modified = Storage::disk('local')->lastModified($file);
            $date = Carbon\Carbon::createFromTimestampUTC($modified);
            if ($date > Carbon\Carbon::now()->subMinute(10)) {
                $allow = true;
            }
        }

        if (!$allow) {
            session()->flash('alert', ['type' => 'error', 'message' => 'Você precisa ter pelo menos um backup de 10 minutos atras para realizar essa operação!']);
            return redirect()->back();
        }

        $exitCode = \Artisan::call('db:empty');

        if (!$exitCode) {
            session()->flash('alert', ['type' => 'success', 'message' => 'Banco de Dados foi limpo! Você poderá começar a cadastrar novamente.']);
        } else {
            session()->flash('alert', ['type' => 'error', 'message' => 'Erro ao limpar o banco de dados!']);
        }

        return redirect()->back();
    }

    public function log()
    {
        $dbLogs = DatabaseLog::all();
        return view('admin.settings.databaselog', compact('dbLogs'));
    }

    public function syncPhotos()
    {
        $this->synchKits();
        $this->synchProducts();
        $this->synchStudents();
        $this->synchTeams();
        session()->flash('alert', ['type' => 'success', 'message' => 'Fotos sincronizadas.']);
        return redirect()->back();
    }

    protected function synchKits()
    {
        $kits = Kit::all();
        $path = public_path() . '/upload/images/kits/';
        foreach ($kits as $kit) {
            if (empty($kit->image) && file_exists($path . $kit->name . '.jpg')) {
                $kit->image = $kit->name . '.jpg';
                $kit->save();
            }
        }
        return true;
    }

    protected function synchProducts()
    {
        $products = Product::all();
        $path = public_path() . '/upload/images/products/';
        foreach ($products as $product) {
            if (empty($product->photo) && file_exists($path . $product->name . '.jpg')) {
                $product->photo = $product->name . '.jpg';
                $product->save();
            }
        }
        return true;
    }

    protected function synchTeams()
    {
        $teams = Team::all();
        $path = public_path() . '/upload/images/teams/';
        foreach ($teams as $team) {
            if (empty($team->image) && file_exists($path . $team->name . '.jpg')) {
                $team->image = $team->name . '.jpg';
                $team->save();
            }
        }
        return true;
    }

    protected function synchStudents()
    {
        $students = Student::all();
        $path = public_path() . '/upload/images/students/';
        foreach ($students as $student) {
            if (empty($student->photo) && file_exists($path . $student->name . '.jpg')) {
                $student->photo = $student->name . '.jpg';
                $student->save();
            }
        }
        return true;
    }
}
