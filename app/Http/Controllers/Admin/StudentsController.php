<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentsController extends Controller
{

    public function index()
    {
        $students = Student::all();
        return view('admin.students.index', compact('students'));
    }

    public function create()
    {
        $teams = Team::where('status', true)->get();
        return view('admin.students.create', compact('teams'));
    }

    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'team_id' => 'required|numeric',
                'period' => 'required',
                'photo' => 'required|image',
                'access_key' => 'required|unique:students,access_key',
                'discount' => 'required',
                'receive_without_commitment' => 'required',
            ], [
                'name.required' => 'Por favor, preencha o nome',
                'photo.required' => 'Por favor, selecione uma imagem',
                'access_key.unique' => 'Chave já utilizada em outro aluno',
                'access_key.required' => 'Por favor, preencha a chave de acesso',
                'photo.image' => 'Somente imagens são permitidas,',
            ]
        );

        $validation->validate();

        $filename = '';

        if ($request->file('photo')->isValid()) {
            $file = $request->file('photo');
            $extension = $file->extension();
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;
            $path = public_path() . '/upload/images/students';

            $file->move($path, $filename);
        }

        $student = Student::create([
            'name' => $request->name,
            'original_name' => $request->original_name,
            'team_id' => $request->team_id,
            'access_key' => $request->access_key,
            'period' => $request->period,
            'receive_without_commitment' => $request->receive_without_commitment,
            'discount' => $request->discount,
            'photo' => $filename,
        ]);

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('students.index');
    }

    public function edit(Student $student)
    {
        $student = Student::findOrFail($student->id);
        $teams = Team::all();
        return view('admin.students.edit', compact('student', 'teams'));
    }

    public function update(Request $request, Student $student)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'team_id' => 'required|numeric',
                'period' => 'required',
                'photo' => 'image',
                'access_key' => 'required|unique:students,access_key,' . $student->id,
                'discount' => 'required',
                'receive_without_commitment' => 'required',
            ], [
                'name.required' => 'Por favor, preencha o nome',
                'photo.required' => 'Por favor, selecione uma imagem',
                'access_key.unique' => 'Chave já utilizada em outro aluno',
                'access_key.required' => 'Por favor, preencha a chave de acesso',
                'photo.image' => 'Somente imagens são permitidas',
            ]
        );

        $validation->validate();

        $student = Student::findOrFail($student->id);

        if ($request->file('photo') && $request->file('photo')->isValid()) {

            $path = public_path() . '/upload/images/students/';

            if (!empty($student->photo)) {
                $file = $path . $student->photo;
                if (file_exists($file)) {
                    unlink($file);
                }
            }

            $file = $request->file('photo');
            $extension = $file->extension();
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;

            $file->move($path, $filename);
            $student->photo = $filename;
        }

        $student->fill([
            'name' => $request->name,
            'original_name' => $request->original_name,
            'team_id' => $request->team_id,
            'access_key' => $request->access_key,
            'period' => $request->period,
            'receive_without_commitment' => $request->receive_without_commitment,
            'discount' => $request->discount,
        ]);
        

        $student->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->back();
    }

    public function destroy(Student $student)
    {
        $student = Student::findOrFail($student->id);

        if (!empty($student->photo)) {
            $path = public_path() . '/upload/images/students/';
            $file = $path . $student->photo;
            if (file_exists($file)) {
                unlink($file);
            }
        }

        $student->delete();
        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->back();
    }

    public function updateStatus(Student $student)
    {
        $student = Student::findOrFail($student->id);

        $student->status = !$student->status;
        $student->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Status atualizado!']);
        return redirect()->back();
    }

    public function destroyImage(Student $student)
    {
        $student = Student::findOrFail($student->id);
        if (!empty($student->photo)) {
            $path = public_path() . '/upload/images/students/';
            $file = $path . $student->photo;
            if (file_exists($file)) {
                unlink($file);
            }
        }
        $student->photo = null;
        $student->save();
        return response()->json(['success' => true]);
    }

    public function import(Request $request)
    {

        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($request->file('file'));
        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();
        $count = 0;
        $data = array();
        for ($row = 2; $row <= $highestRow; ++$row) {
            if (!empty($worksheet->getCell('A' . $row)->getValue()) && !empty($worksheet->getCell('B' . $row))) {
                $team = Team::where('name', $worksheet->getCell('A' . $row)->getValue())
                    ->where('type', $worksheet->getCell('B' . $row)->getValue())
                    ->first();
                if (!$team) {
                    $team = Team::create([
                        'name' => $worksheet->getCell('A' . $row)->getValue(),
                        'type' => $worksheet->getCell('B' . $row)->getValue(),
                        'school' => '',
                        'period' => $worksheet->getCell('C' . $row)->getValue(),
                    ]);
                }
            }

            if (!empty($worksheet->getCell('D' . $row)->getValue()) && !empty($worksheet->getCell('E' . $row)->getValue())) {

                $student = Student::where('name', $worksheet->getCell('D' . $row)->getValue())
                    ->orWhere('access_key', $worksheet->getCell('E' . $row)->getValue())
                    ->first();
                if (!$student) {
                    $student = Student::create([
                        'name' => $worksheet->getCell('D' . $row)->getValue(),
                        'team_id' => $team->id,
                        'period' => $worksheet->getCell('C' . $row)->getValue(),
                        'access_key' => $worksheet->getCell('E' . $row)->getValue(),
                        'discount' => $worksheet->getCell('F' . $row)->getValue(),
                        'photo' => $worksheet->getCell('G' . $row)->getValue(),
                        'original_name' => $worksheet->getCell('H' . $row)->getValue(),
                        'receive_without_commitment' => $worksheet->getCell('I' . $row)->getValue(),
                    ]);
                    $count++;
                }
            }

        }

        session()->flash('alert', ['type' => 'success', 'message' => $count . ' Alunos Importados ']);

        return redirect()->back();
    }

}
