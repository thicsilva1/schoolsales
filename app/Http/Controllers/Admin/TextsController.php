<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Text;

class TextsController extends Controller
{
    public function index()
    {
        $texts = Text::all();
        return view('admin.texts.index', compact('texts'));
    }

    public function create()
    {
        return view('admin.texts.create');
    }

    public function edit(Text $text)
    {
        $text = Text::findOrFail($text->id);
        return view('admin.texts.edit', compact('text'));
    }

    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'reference' => 'required',
                'title' => 'required',
                'description' => 'required',
            ], [
                'reference.required' => 'Por favor, preencha o código',
                'title.required' => 'Por favor, preencha o valor do desconto',
                'description.required' => 'Por favor, preencha o uso máximo',
            ]
        );

        $validation->validate();

        $text = Text::create($request->all());

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('texts.index');
    }

    public function update(Request $request, Text $text)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'reference' => 'required',
                'title' => 'required',
                'description' => 'required',
            ], [
                'reference.required' => 'Por favor, preencha o código',
                'title.required' => 'Por favor, preencha o valor do desconto',
                'description.required' => 'Por favor, preencha o uso máximo',
            ]
        );

        $validation->validate();

        $text = Text::findOrFail($text->id);

        $text->fill($request->all());
        $text->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Salvo com sucesso!']);
        return redirect()->route('texts.index');
    }

    public function destroy(Text $text)
    {
        $test = Text::findOrFail($text->id);

        $text->delete();

        session()->flash('alert', ['type' => 'success', 'message' => 'Excluído com sucesso!']);
        return redirect()->route('texts.index');
    }

    public function updateStatus(Text $text)
    {
        $text = Text::findOrFail($text->id);

        $text->status = !$text->status;
        $text->save();

        session()->flash('alert', ['type' => 'success', 'message' => 'Status Atualizado!']);
        return redirect()->route('texts.index');
    }
}
