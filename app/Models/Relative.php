<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Relative extends Model
{
    protected $fillable = ['name', 'email', 'cpf', 'phone', 'status'];
    protected $dates = ['created_at', 'updated_at'];    

    public function students()
    {
        return $this->belongsToMany(Student::class);
    }

    public function logs()
    {
        return $this->hasMany(Log::class);
    }
}
