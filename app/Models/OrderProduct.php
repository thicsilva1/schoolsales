<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $fillable = [
        'kit_order_id', 'product_id', 'quantity', 'value'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function kit()
    {
        return $this->belongsTo(KitOrder::class, 'kit_order_id');
    }
}
