<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotReceive extends Model
{
    protected $fillable = [
        'relative_id', 'student_id'
    ];

    public function relative()
    {
        return $this->belongsTo(Relative::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
