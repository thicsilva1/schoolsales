<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'code', 'type', 'value', 'max_amount', 'due_date', 'status'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
