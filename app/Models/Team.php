<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name', 'school', 'period', 'image', 'type', 'status'];

    public function students()
    {
        return $this->hasMany(Student::class);
    }
}
