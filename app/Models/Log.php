<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $dates = ['created_at', 'updated_at'];

    public function relative()
    {
        return $this->belongsTo(Relative::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
