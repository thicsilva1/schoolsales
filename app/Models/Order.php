<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'relative_id', 'type', 'payment_status', 'coupon_id', 'subtotal', 'discount', 'total'
    ];

    public function kits(){
        return $this->hasMany(KitOrder::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function products(){
        return $this->kits()->hasMany(OrderProduct::class);
    }

    public function relative()
    {
        return $this->belongsTo(Relative::class);
    }

    public function delete(){
        $this->kits()->delete();
        parent::delete();
    }
}
