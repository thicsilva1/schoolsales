<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name', 'school_name', 'team_id', 'period', 'access_key',
        'discount', 'photo', 'status', 'original_name',
        'receive_without_commitment',
    ];

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function relatives()
    {
        return $this->belongsToMany(Relative::class);
    }

    public function kitOrders()
    {
        return $this->hasMany(KitOrder::class);
    }

    public function delete()
    {
        $this->relatives()->detach();
        $this->kitOrders()->delete();
        $this->logs()->delete();
        parent::delete();
    }

    public function logs()
    {
        return $this->hasMany(Log::class);
    }


}
