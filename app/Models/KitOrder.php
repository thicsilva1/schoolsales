<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KitOrder extends Model
{
    protected $fillable = [
        'order_id', 'kit_id', 'student_id', 'quantity', 'value',
    ];

    public function kit()
    {
        return $this->belongsTo(Kit::class);
    }

    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function delete()
    {
        $this->products()->delete();
        parent::delete();
    }
}
