<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'description', 'photo', 'price', 'display_order', 'status', 'kindergarten', 'elementary_school',
        'high_school', 'all_type',
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function kits()
    {
        return $this->belongsToMany(Kit::class);
    }

    public function delete()
    {
        $this->kits()->detach();
        parent::delete();
    }
}
