<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DatabaseLog extends Model
{
    protected $fillable = ['type', 'status', 'description'];
    protected $dates = ['created_at', 'updated_at'];
}
