<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kit extends Model
{
    protected $fillable = [
        'name', 'description', 'price', 'old_price', 'discount',
        'image', 'display_order', 'kindergarten', 'elementary_school',
        'high_school', 'all_type', 'status', 'allow_extra_products', 'receive_without_commitment'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

}
