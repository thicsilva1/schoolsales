@extends('admin.layouts.app')
@section('styles')
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
@stop
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('panel.index')}}">Painel</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                  <a href="{{route('orders.index')}}">Pedidos</a>
                  <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Editar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Pedidos

        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('alert'))
          <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            {{ session('alert.message')}}
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-basket font-dark"></i>
                        <span class="caption-subject bold uppercase">Edição de Pedido #{{$order->id}}</span>
                    </div>

                    <div class="actions">

                      <div class="btn-group">
                        @if($order->type=='Venda' && $order->payment_status!='PAGO')
                        <form method="post" action="{{ route('orders.pay', $order->id)}}" id="set-paid" style="display:none">
                          @csrf
                          @method('PATCH')
                        </form>
                        <a href="#" class="btn btn-transparent green-dark btn-outline btn-sm" onclick="event.preventDefault();document.getElementById('set-paid').submit();"><i class="icon-wallet"></i> Confirmar pagamento</a>
                        @endif
                        <form method="post" action="{{ route('orders.export')}}" id="export" style="display:none">
                          @csrf
                          <input type="hidden" name="ids[]" value="{{$order->id}}">
                        </form>
                        <button class="btn btn-transparent blue btn-outline btn-sm" onclick="event.preventDefault();document.getElementById('export').submit();">Exportar</button>
                      </div>
                    </div>
                </div>
                <div class="portlet-body">
                  <div class="row">
                    <div class="col-md-6 col-sm-12">
                      <div class="portlet blue-hoki box">
                        <div class="portlet-title">
                          <div class="caption">
                            <i class="fa fa-shopping-cart"></i>Detalhes do Pedido
                          </div>
                        </div>
                        <div class="portlet-body">
                          <div class="row static-info">
                            <div class="col-md-5 name">Pedido #</div>
                            <div class="col-md-7 value">{{$order->id}}</div>
                          </div>
                          <div class="row static-info">
                            <div class="col-md-5 name">Emitido em</div>
                            <div class="col-md-7 value">{{$order->created_at->format('d/m/Y \à\s H:i')}}</div>
                          </div>
                          <div class="row static-info">
                            <div class="col-md-5 name">Valor Total</div>
                            <div class="col-md-7 value">R$ {{ number_format($order->total, 2, ',', '.') }}</div>
                          </div>
                          <div class="row static-info" id="show-status">
                            <div class="col-md-5 name">Status</div>
                            <div class="col-md-7 value">
                              {{$order->payment_status}}
                              <button class="btn btn-circle btn-icon-only blue" id="edit-status"><i class="fa fa-edit"></i></button>
                            </div>
                          </div>
                          <div class="row static-info" id="alter-status" style="display:none">
                            <form action="{{route('orders.update.status', $order->id)}}" method="post">
                              @csrf
                              @method('PATCH')
                              <div class="col-md-6">
                                <div class="form-group">
                                  <select name="status" id="status" class="form-control">
                                    <option value="PENDENTE" {{$order->payment_status=='PENDENTE'?'selected':''}} >Pendente</option>
                                    <option value="PAGO" {{$order->payment_status=='PAGO'?'selected':''}}>Pago</option>
                                    <option value="CARRINHO" {{$order->payment_status=='CARRINHO'?'selected':''}}>Carrinho</option>
                                    <option value="SEM COMPROMISSO" {{$order->payment_status=='SEM COMPROMISSO'}}>Sem Compromisso</option>
                                    <option value="CANCELADO" {{$order->payment_status=='CANCELADO'?'selected':''}}>Cancelado</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <button type="button" class="btn btn-default" id="cancel-status">Cancelar</button>
                                <button type="submit" class="btn btn-success">Salvar</button>
                              </div>
                            </form>
                          </div>
                          <div class="row static-info">
                            @if(!empty($xml))
                            <div class="col-md-5 name">Tipo de Pagamento</div>
                            <div class="col-md-7 value">
                              @switch($xml->paymentMethod->type)
                                @case(1)
                                  Cartão de crédito
                                  @break
                                @case(2)
                                  Boleto
                                  @break
                                @default
                                  Outros
                              @endswitch
                            </div>
                            @endif
                          </div>
                        </div>
                      </div>

                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="portlet green-meadow box">
                        <div class="portlet-title">
                          <div class="caption">
                            <i class="fa fa-user"></i>Detalhes do Comprador
                          </div>
                          <div class="actions">

                            @if($order->type=='Venda' && $order->payment_status!='PAGO')
                            <a href="#edit-user" data-toggle="modal" data-target="#edit-user" class="btn btn-default"><i class="fa fa-pencil"></i> Editar</a>
                            @endif
                          </div>
                        </div>
                        <div class="portlet-body">
                          <div class="row static-info">
                            <div class="col-md-4 name">Responsável</div>
                            <div class="col-md-8 value">{{ optional($order->relative)->name}}</div>
                          </div>
                          <div class="row static-info">
                            <div class="col-md-4 name">Email </div>
                            <div class="col-md-8 value">{{optional($order->relative)->email}}</div>
                          </div>
                          <div class="row static-info">
                            <div class="col-md-4 name">Fone</div>
                            <div class="col-md-8 value">{{ optional($order->relative)->phone}}</div>
                          </div>
                          <div class="row static-info">
                            <div class="col-md-4 name">CPF</div>
                            <div class="col-md-8 value">{{ optional($order->relative)->cpf}}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="portlet dark box">
                        <div class="portlet-title">
                          <div class="caption">
                            <i class="icon-puzzle"></i>Kits
                          </div>
                          <div class="actions">
                          @if($order->type=='Venda' && $order->payment_status!='PAGO')
                            <a href="#add-kit" class="btn btn-default" data-toggle="modal" data-target="#add-kit" data-relative="{{$order->relative_id}}"><i class="fa fa-plus"></i> Adicionar Kit</a>
                          @endif
                          </div>
                        </div>
                        <div class="portlet-body">
                          <div class="table-responsive">
                            <table class="table table-hover table-bordered table-stripped">
                              <thead>
                                <tr>
                                  <th>Aluno</th>
                                  <th>Kit</th>
                                  <th>Quantidade</th>
                                  <th>Valor</th>
                                  <th>Subtotal</th>
                                  <th>Ações</th>
                                </tr>
                              </thead>
                              <tbody>
                              @foreach($order->kits as $kit)
                                <tr>
                                  <td>{{optional($kit->student)->name}}</td>
                                  <td>{{optional($kit->kit)->name}}</td>
                                  <td>{{$kit->quantity}}</td>
                                  <td class="text-left">R$ {{number_format($kit->value, 2, ',', '.')}}</td>
                                  <td class="text-left">R$ {{number_format($kit->value*$kit->quantity, 2, ',', '.')}}</td>
                                  <td>
                                    @if($order->type=='Venda' && $order->payment_status!='PAGO')
                                    <div class="button-group">
                                      <form action="{{ route('orders.kit.destroy', [$order->id, $kit->id])}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        @if(optional($kit->kit)->allow_extra_products && optional($kit->kit->products)->count()>0)
                                        <a href="#add-prod" class="btn btn-icon-only yellow" data-toggle="modal" data-target="#add-prod" data-id="{{$kit->id}}" data-kit="{{$kit->kit_id}}" data-student="{{$kit->student_id}}" data-relative="{{$order->relative_id}}" title="Adicionar produtos ao kit"><i class="fa fa-plus"></i></a>
                                        @endif
                                        <a href="#edit-kit" class="btn btn-icon-only blue" data-toggle="modal" data-target="#edit-kit" data-relative="{{$order->relative_id}}" data-student="{{$kit->student_id}}" data-kit="{{$kit->kit_id}}" data-id="{{$kit->id}}" data-quantity="{{$kit->quantity}}" title="Editar Kit"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-icon-only red" title="Excluir Kit"><i class="fa fa-trash"></i></button>
                                      </form>
                                    </div>
                                    @endif
                                  </td>
                                </tr>
                                @if(optional($kit->products)->count()>0)
                                <thead class="dark">
                                  <tr>
                                    <th></th>
                                    <th>Produto</th>
                                    <th>Quantidade</th>
                                    <th>Valor</th>
                                    <th>Subtotal</th>
                                    <th></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach($kit->products as $product)
                                  <tr>
                                    <td></td>
                                    <td>{{$product->product->name}}</td>
                                    <td>{{$product->quantity}}</td>
                                    <td class="text-left">R$ {{ number_format($product->value, 2, ',', '.')}}</td>
                                    <td class="text-left">R$ {{ number_format($product->value*$product->quantity, 2, ',', '.')}}</td>
                                    <td>
                                      @if($order->type=='Venda' && $order->payment_status!='PAGO')
                                      <div class="button-group">
                                        <form action="{{ route('orders.product.destroy', [$order->id, $product->id])}}" method="post">
                                          @csrf
                                          @method('DELETE')
                                          <a href="#edit-prod" class="btn btn-icon-only blue" data-toggle="modal" data-id="{{$product->id}}" data-kit="{{$kit->kit_id}}" data-product="{{$product->product_id}}" data-quantity="{{$product->quantity}}"><i class="fa fa-pencil"></i></a>
                                          <button class="btn btn-icon-only red"><i class="fa fa-trash"></i></button>
                                        </form>
                                      </div>
                                      @endif
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                                @endif
                                <tr>
                                  <td colspan="6"></td>
                                </tr>
                              @endforeach
                              </tbody>
                            </table>
                          </div>

                          <div class="modal fade" id="edit-user" role="edit-user" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title">Editar Responsável</h4>
                                  </div>
                                  <form action="{{route('orders.update.relative', $order->id )}}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <label for="relative_id">Responsável</label>
                                        <select name="relative_id" id="relative_id" class="form-control select2" required>
                                        @foreach ($relatives as $relative)
                                          <option value="{{ $relative->id }}" {{$order->relative_id==$relative->id?'selected':''}}>{{$relative->name}}</option>
                                        @endforeach
                                        </select>
                                      </div>
                                    </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                                      <button type="submit" class="btn green">Salvar</button>
                                  </div>
                                  </form>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <div class="modal fade" id="edit-kit" role="edit-kit" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title">Editar Kit</h4>
                                  </div>
                                  <form method="post" action="{{ route('orders.update.kit', $order->id) }}">
                                  @csrf
                                  @method('PATCH')
                                  <input type="hidden" name="ek_id" id="ek_id">
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <label for="ek_student">Aluno</label>
                                        <select name="ek_student" id="ek_student" class="form-control select2" required>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                        <label for="ek_kit">Kit</label>
                                        <select name="ek_kit" id="ek_kit" class="form-control select2" required>

                                        </select>
                                      </div>
                                      <div class="form-group">
                                        <label for="ek_quantity">Quantidade</label>
                                        <input type="number" name="ek_quantity" id="ek_quantity" class="form-control" step="1" min="1" value="1" required>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn green">Salvar</button>
                                    </div>
                                  </form>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <div class="modal fade" id="edit-prod" role="edit-prod" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title">Editar Produto</h4>
                                  </div>
                                  <form method="post" action="{{route('orders.update.product', $order->id)}}">
                                  @csrf
                                  @method('PATCH')
                                    <div class="modal-body">
                                      <input type="hidden" name="ep_id" id="ep_id">
                                      <div class="form-group">
                                        <label for="ep_product">Produto</label>
                                        <select name="ep_product" id="ep_product" class="form-control select2" required>

                                        </select>
                                      </div>
                                      <div class="form-group">
                                        <label for="ep_quantity">Quantidade</label>
                                        <input type="number" name="ep_quantity" id="ep_quantity" class="form-control" step="1" min="1" required>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn green">Salvar</button>
                                    </div>
                                  </form>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <div class="modal fade" id="add-kit" role="add-kit" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title">Adicionar Kit</h4>
                                  </div>
                                  <form method="post" action="{{route('orders.store.kit', $order->id)}}">
                                  @csrf
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <label for="ak_student">Aluno</label>
                                        <select name="ak_student" id="ak_student" class="form-control select2" required>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                        <label for="ak_kit">Kit</label>
                                        <select name="ak_kit" id="ak_kit" class="form-control select2" required>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                        <label for="ak_quantity">Quantidade</label>
                                        <input type="number" name="ak_quantity" id="ak_quantity" class="form-control" step="1" min="1" value="1" required>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn green">Salvar</button>
                                    </div>
                                  </form>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <div class="modal fade" id="add-prod" role="add-prod" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title">Adicionar Produto</h4>
                                  </div>
                                  <form method="post" action="{{route('orders.store.product', $order->id)}}">
                                  @csrf
                                    <input type="hidden" name="ap_kit" id="ap_kit">
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <label for="ap_product">Produto</label>
                                        <select name="ap_product" id="ap_product" class="form-control select2" required>

                                        </select>
                                      </div>
                                      <div class="form-group">
                                        <label for="quantity">Quantidade</label>
                                        <input type="number" name="ap_quantity" id="ap_quantity" class="form-control" step="1" min="1" value="1" required>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn green">Salvar</button>
                                    </div>
                                  </form>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                      <div class="well">
                        <div class="row static-info align-reverse">
                          <div class="col-md-8 name">SubTotal</div>
                          <div class="col-md-3 value">R$ {{ number_format($order->subtotal, 2, ',', '.') }}</div>
                        </div>
                        <div class="row static-info align-reverse">
                          <div class="col-md-8 name">Desconto</div>
                          <div class="col-md-3 value">R$ {{ number_format($order->discount, 2, ',', '.') }}</div>
                        </div>
                        <div class="row static-info align-reverse">
                          <div class="col-md-8 name">Total</div>
                          <div class="col-md-3 value">R$ {{ number_format($order->total, 2, ',', '.') }}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

          </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop
@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('admin/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('admin/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/fileinput.min.js"></script>
<script>

  $("#photo").fileinput({'showUpload':false, 'previewFileType':'any'});

  $('.select2').each(function(){
    $(this).select2({
      theme: 'bootstrap',
      placeholder: 'Selecione...',
      width: null
    });
  })

  $(".btn.btn-icon-only.red").each(function(){
    $(this).on('click', function(e){
      e.preventDefault();
      var form = $(this).parent('form');
      swal({
        title: "Deseja realmente excluir?",
        icon: "warning",
        buttons: ["Cancelar", "Confirmar"],
        dangerMode: true,
      }).then((isConfirmed)=>{
        if (isConfirmed){
          form.submit();
        }
      });
    })
  });

  $('#add-kit').on('show.bs.modal', function(e){
    var button = $(e.relatedTarget);
    var student = $(this).find('#ak_student');
    var kit = $(this).find('#ak_kit');
    $.ajax({
      method: 'GET',
      url: "{{ route('orders.students')}}",
      success: function(response){
        student.html(response.options);
        student.val(button.data('relative')).trigger('change');
      }
    });
    $.ajax({
      method: 'GET',
      url: "{{ route('orders.kits')}}",
      success: function(response){
        kit.html(response.options);
        kit.val(button.data(null)).trigger('change');
      }
    });
  });
  $('#ak_student').on('change', function(e){
    var kit = $('#add-kit').find('#ak_kit');
    var student = $(this).val();
    $.ajax({
      method: 'GET',
      data: {student: student},
      url: "{{ route('orders.kits')}}",
      success: function(response){
        kit.html(response.options);
      }
    });
  })

  $('#add-prod').on('show.bs.modal', function(e){
    var button = $(e.relatedTarget);
    var product = $(this).find('#ap_product');
    var id = $(this).find('#ap_kit');
    id.val(button.data('id'));
    $.ajax({
      method: 'GET',
      data: {id: button.data('student')},
      url: "{{ route('orders.products')}}",
      success: function(response){
        product.html(response.options);
        product.val(null).trigger('change');
      }
    });
  });

  $('#edit-kit').on('show.bs.modal', function(e){
    var button = $(e.relatedTarget);
    var student = $(this).find('#ek_student');
    var kit = $(this).find('#ek_kit');
    var quantity = $(this).find('#ek_quantity');
    var id = $(this).find('#ek_id');
    id.val(button.data('id'));
    quantity.val(button.data('quantity'));
    $.ajax({
      method: 'GET',
      data: {id: button.data('relative')},
      url: "{{ route('orders.students')}}",
      success: function(response){
        student.html(response.options);
        student.val(button.data('student')).trigger('change');
      }
    });
    $.ajax({
      method: 'GET',
      url: "{{ route('orders.kits')}}",
      success: function(response){
        kit.html(response.options);
        kit.val(button.data('kit')).trigger('change');
      }
    });
  });

  $('#edit-prod').on('show.bs.modal', function(e){
    var button = $(e.relatedTarget);
    var product = $(this).find('#ep_product');
    var id = $(this).find('#ep_id');
    var quantity = $(this).find('#ep_quantity');

    id.val(button.data('id'));
    quantity.val(button.data('quantity'));
    $.ajax({
      method: 'GET',
      data: {id: button.data('student')},
      url: "{{ route('orders.products')}}",
      success: function(response){
        product.html(response.options);
        product.val(button.data('product')).trigger('change');
      }
    });
    $.ajax({
      method: 'GET',
      data: {id: button.data('relative')},
      url: "{{ route('orders.students')}}",
      success: function(response){
        student.html(response.options);
        student.val(button.data('relative')).trigger('change');
      }
    });
  });

  $('button#edit-status').on('click', function(){
    console.log('click');
    $('#alter-status').show();
    $('#show-status').hide();
  });
  $('button#cancel-status').on('click', function(){
    $('#alter-status').hide();
    $('#show-status').show();
  })


</script>

@stop
