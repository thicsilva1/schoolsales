@extends('admin.layouts.app')

@section('styles')
  <link href="{{ asset('admin/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Pedidos</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Pedidos

        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('alert'))
          <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            {{ session('alert.message')}}
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-basket font-dark"></i>
                        <span class="caption-subject bold uppercase">Pedidos</span>
                    </div>
                    <div class="actions">
                      <form action="{{route('orders.export')}}" method="post" id="export">
                        @csrf
                        <div id="hiddens">
                        </div>
                      </form>
                      <a href="#" class="btn btn-outline green-dark" onclick="exportExcel();">Exportar Excel</a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                            <tr>
                                <th>
                                  <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes">
                                    <span></span>
                                  </label>
                                </th>
                                <th> # </th>
                                <th> Responsável </th>
                                <th> Alunos </th>
                                <th> Valor </th>
                                <th> Data </th>
                                <th> Status </th>
                                <th> Açoes </th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($orders as $order)
                          <tr>
                            <td>
                              <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="checkboxes" name="selected[]" value="{{$order->id}}">
                                <span></span>
                              </label>
                            </td>
                            <td> {{$order->id}} </td>
                            <td> {{$order->relative->name}} </td>
                            <td>
                              @foreach($order->kits->groupBy('student_id') as $kit)
                                {{$kit[0]->student->name}}
                                @if(!$loop->last)
                                ,
                                @endif
                              @endforeach
                            </td>
                            <td> R$ {{number_format($order->total, 2, ',', '.')}}</td>
                            <td> {{ $order->created_at->format('d/m/Y H:i:s')}}</td>
                            <td>
                            @switch($order->payment_status)
                              @case('PAGO')
                                <span class="font-green-jungle">Pago</span>
                                @break
                              @case('SEM COMPROMISSO')
                                <span class="font-purple-wisteria">Sem Compromisso</span>
                                @break
                              @case('CARRINHO')
                                <span class="font-blue-soft">Carrinho</span>
                                @break
                              @case('CANCELADO')
                                <span class="font-red">Cancelado</span>
                                @break
                              @default
                              <span class="font-yellow-gold">Pendente</span>
                            @endswitch
                            </td>
                            <td>
                              <form method="post" action="{{ route('orders.destroy', $order->id)}}" id="delete-{{$order->id}}" style="display:none">
                                @csrf
                                @method('DELETE')
                              </form>
                              <form method="post" action="{{ route('orders.pay', $order->id)}}" id="pay-{{$order->id}}" style="display:none">
                                @csrf
                                @method('PATCH')
                              </form>

                                <div class="btn-group">
                                  <button class="btn btn-sm default blue-stripe dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                      <i class="fa fa-angle-down"></i>
                                  </button>
                                  <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{route('orders.edit', $order->id)}}">
                                            <i class="icon-pencil"></i> Editar </a>
                                    </li>
                                    <li>
                                      <a href="#" onclick="confirmDelete({{$order->id}})">
                                        <i class="icon-trash"></i> Excluir
                                      </a>
                                    </li>
                                    @if($order->payment_status=='PENDENTE')
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="#" onclick="setPaid({{$order->id}})">
                                            <i class=" icon-wallet"></i> Confirmar Pagamento
                                        </a>
                                    </li>
                                    @endif
                                  </ul>
                                </div>
                              </td>
                          </tr>
                          @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

          </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop

@section('scripts')
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="{{ asset('admin/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/pages/scripts/orders-datatable.js')}}" type="text/javascript"></script>

   <script>

    function confirmDelete(id){
      swal({
        title: "Deseja realmente excluir?",
        text: "Ao excluir, todos os itens relacionados ao pedido serão removidos",
        icon: "warning",
        buttons: ["Cancelar", "Confirmar"],
        dangerMode: true,
      }).then((isConfirmed)=>{
        if (isConfirmed){
          $('#delete-'+id).submit();
        }
      });
    }

    function exportExcel(){
      $('#hiddens').html('');
      var input=''
      $('.checkboxes:checked').each(function(i, v) {
        input += '<input type="hidden" name="ids[]" value="' + $(this).val() + '">';
      });
      $('#hiddens').append(input);
      $('#export').submit();
    }

    function setPaid(id){
      $('#pay-'+id).submit();
    }
  </script>
@stop
