@extends('admin.layouts.app')
@section('styles')
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('admin/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('admin/assets/global/plugins/bootstrap-summernote/summernote.css')}}" rel="stylesheet" type="text/css" />
@stop
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('panel.index')}}">Painel</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('notreceive.index')}}">Não Receber</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Editar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Não Receber

        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('alert'))
          <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            {{ session('alert.message')}}
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-ban font-dark"></i>
                        <span class="caption-subject bold uppercase">Edição de Não Receber</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body form">
                  <form method="post" action="{{route('notreceive.update', $nr->id)}}">
                    @csrf
                    @method('PATCH')
                    <div class="form-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group {{$errors->has('relative_id')?'has-error':''}}">
                            <label for="name">Responsável</label>
                            <select name="relative_id" id="relative_id" class="form-control select2" required>
                              @foreach($relatives as $relative)
                              <option value="{{$relative->id}}" {{$relative->id==old('relative_id', $nr->relative_id)?'selected':''}}>{{$relative->name}} ({{$relative->email}})</option>
                              @endforeach
                            </select>
                            @if($errors->has('relative_id'))
                            <span class="text-danger">{{$errors->first('relative_id') }}</span>
                            @endif
                          </div>
                          <div class="form-group {{$errors->has('student_id')?'has-error':''}}">
                            <label for="name">Aluno</label>
                            <select name="student_id" id="student_id" class="form-control select2" required>
                              @foreach($students as $student)
                              <option value="{{$student->id}}" data-image="{{$student->photo}}" {{$student->id==old('student_id', $nr->student_id)?'selected':''}}>{{$student->name}}</option>
                              @endforeach
                            </select>
                            @if($errors->has('student_id'))
                            <span class="text-danger">{{$errors->first('student_id') }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions right">
                      <a href="{{route('notreceive.index')}}" class="btn default">Cancelar</a>
                      <button class="btn btn-success">Salvar</button>
                    </div>
                  </form>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

          </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop
@section('scripts')
<script src="{{ asset('admin/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>

<script src="{{ asset('admin/assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script>

    $("#description").wysihtml5({stylesheets:["{{ asset('admin/assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css')}}"]});

</script>

@stop
