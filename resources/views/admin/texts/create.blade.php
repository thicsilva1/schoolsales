@extends('admin.layouts.app')
@section('styles')
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('admin/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('admin/assets/global/plugins/bootstrap-summernote/summernote.css')}}" rel="stylesheet" type="text/css" />
@stop
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('panel.index')}}">Painel</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('texts.index')}}">Textos</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Novo</span>
                </li>
            </ul>

        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Textos

        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('alert'))
          <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            {{ session('alert.message')}}
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-doc font-dark"></i>
                        <span class="caption-subject bold uppercase">Inclusão de Texto</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body form">
                  <form method="post" action="{{route('texts.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group {{$errors->has('title')?'has-error':''}}">
                            <label for="name">Titulo</label>
                            <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}">
                            @if($errors->has('title'))
                            <span class="text-danger">{{$errors->first('title') }}</span>
                            @endif
                          </div>
                          <div class="form-group {{$errors->has('reference')?'has-error':''}}">
                            <label for="reference">Referência</label>
                            <select name="reference" id="reference" class="form-control select2">
                              <option value="other" selected>Rascunho</option>
                              <option value="not-receive">Não Receber</option>
                              <option value="payment">Pagamento</option>
                              <option value="finalized">Pedido Finalizado</option>
                              <option value="products">Produtos</option>
                              <option value="login">Sessão</option>
                              <option value="website_inactive">Site Inativo</option>
                            </select>
                            @if($errors->has('reference'))
                            <span class="text-danger">{{$errors->first('reference') }}</span>
                            @endif
                          </div>
                          <div class="form-group">
                            <label for="description">Descrição</label>
                            <textarea name="description" id="description" cols="30" rows="10" class="form-control wysihtml5">{{ old('description')}}</textarea>
                          </div>
                          <div class="form-group">
                            <label for="status">Cadastro Ativo</label>
                            <select name="status" id="status" class="form-control select2">
                              <option value="0" >Não</option>
                              <option value="1" selected>Sim</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions right">
                      <a href="{{route('texts.index')}}" class="btn default">Cancelar</a>
                      <button class="btn btn-success">Salvar</button>
                    </div>
                  </form>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

          </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop
@section('scripts')
<script src="{{ asset('admin/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/global/plugins/bootstrap-wysihtml5/locales/bootstrap-wysihtml5.pt-BR.js') }}" type="text/javascript"></script>

<script src="{{ asset('admin/assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script>

    $("#description").wysihtml5({
      stylesheets:["{{ asset('admin/assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css')}}"],
      link: false,
      image: false,
      locale: "pt-BR"
    })
</script>

@stop
