@extends('admin.layouts.app')
@section('styles')
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
@stop
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('panel.index')}}">Painel</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                  <a href="{{route('products.index')}}">Produtos</a>
                  <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Novo</span>
                </li>
            </ul>

        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Produtos

        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('alert'))
          <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            {{ session('alert.message')}}
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-social-dropbox font-dark"></i>
                        <span class="caption-subject bold uppercase">Inclusão de Produto</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body form">
                  <form method="post" action="{{route('products.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group {{$errors->has('name')?'has-error':''}}">
                            <label for="name">Nome</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
                            @if($errors->has('name'))
                            <span class="text-danger">{{$errors->first('name') }}</span>
                            @endif
                          </div>
                          <div class="form-group {{$errors->has('description')?'has-error':''}}">
                            <label for="description">Descrição</label>
                            <textarea class="form-control" name="description" id="description">{{ old('description') }}</textarea>
                            @if($errors->has('description'))
                            <span class="text-danger">{{$errors->first('description') }}</span>
                            @endif
                          </div>
                          <div class="form-group {{$errors->has('price')?'has-error':''}}">
                            <label for="price">Preço</label>
                            <input type="number" class="form-control" name="price" id="price" value="{{ old('price') }}" step="0.01" min="0">
                            @if($errors->has('price'))
                            <span class="text-danger">{{$errors->first('price') }}</span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group {{$errors->has('display_order')?'has-error':''}}">
                            <label for="display_order">Ordem de Exibição</label>
                            <input type="number" class="form-control" name="display_order" id="display_order" min="0" step="1" value="{{ old('display_order') }}">
                            @if($errors->has('price'))
                            <span class="text-danger">{{$errors->first('display_order') }}</span>
                            @endif
                          </div>
                          <div class="form-group">
                            <label for="name">Cadastro ativo</label>
                            <select name="status" id="status" class="form-control select2">
                              <option value="0" >Não</option>
                              <option value="1" selected>Sim</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label class="col-md-3 control-label">Tipo</label>
                            <div class="col-md-9">
                              <div class="mt-checkbox-inline">
                              <label class="mt-checkbox">
                                      <input type="checkbox" name="kindergarten" id="kindergarten" value="1" {{ old('kindergarten')?'checked':''}}> Infantil
                                      <span></span>
                                  </label>
                                  <label class="mt-checkbox">
                                      <input type="checkbox" name="elementary_school" id="elementary_school" value="1" {{ old('elementary_school')?'checked':''}}> Ensino Fundamental
                                      <span></span>
                                  </label>
                                  <label class="mt-checkbox">
                                      <input type="checkbox" name="high_school" id="high_school" value="1" {{ old('high_school')?'checked':''}}>Ensino Médio
                                      <span></span>
                                  </label>
                                  <label class="mt-checkbox">
                                      <input type="checkbox" name="all_type" id="all_type" value="1" {{ old('all_type')?'checked':''}}>Todos
                                      <span></span>
                                  </label>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group {{$errors->has('photo')?'has-error':''}}">
                            <label for="photo">Foto</label>
                            <input type="file" name="photo" id="photo" data-preview-file-type="text" accept="image/*">
                             @if($errors->has('photo'))
                            <span class="text-danger">{{$errors->first('photo') }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions right">
                      <a href="{{route('products.index')}}" class="btn default">Cancelar</a>
                      <button class="btn btn-success">Salvar</button>
                    </div>
                  </form>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

          </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop
@section('scripts')
<script src="{{ asset('admin/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/fileinput.min.js"></script>
<script>
    $("#photo").fileinput({'showUpload':false, 'previewFileType':'any'});
    $("#all_type").on('click', function(){
      $("#kindergarten").prop('checked', $(this).prop('checked'));
      $("#elementary_school").prop('checked', $(this).prop('checked'));
      $("#high_school").prop('checked', $(this).prop('checked'));
    })
</script>

@stop
