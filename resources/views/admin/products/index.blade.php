@extends('admin.layouts.app')

@section('styles')
  <link href="{{ asset('admin/assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('panel.index')}}">Painel</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Produtos</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Produtos

        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('alert'))
          <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            {{ session('alert.message')}}
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="btn-group">
                       <a href="{{route('products.create')}}" class="btn blue">
                        <i class="icon-plus"></i> Incluir
                      </a>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Nome </th>
                                <th> Preço </th>
                                <th> Imagem </th>
                                <th> Status </th>
                                <th> Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($products as $product)
                          <tr>

                            <td> {{ $product->id }} </td>
                            <td> {{ $product->name}} </td>
                            <td> R$ {{ number_format($product->price, 2, ',', '.')}}</td>
                            <td class="fit"> <img src="{{ asset('upload/images/products/' . $product->photo) }}" alt="{{$product->name}}" class="user-pic" style="max-width:50px;max-height:50px"></td>
                            <td> <span class="font-{{ $product->status?'green-jungle':'red'}}">{{ $product->status?'Ativo':'Inativo'}}</span></td>
                            <td>
                              <form method="post" action="{{ route('products.destroy', $product->id)}}" id="delete-{{$product->id}}" style="display:none">
                                @csrf
                                @method('DELETE')
                              </form>
                              <form method="post" action="{{ route('products.status', $product->id)}}" id="status-{{$product->id}}" style="display:none">
                                @csrf
                                @method('PATCH')
                              </form>
                                <div class="btn-group">
                                  <button class="btn btn-sm default blue-stripe dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Ações
                                      <i class="fa fa-angle-down"></i>
                                  </button>
                                  <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{route('products.edit', $product->id)}}">
                                            <i class="icon-pencil"></i> Editar </a>
                                    </li>
                                    <li>
                                      <a href="javascript:;" onclick="confirmDelete({{$product->id}})">
                                        <i class="icon-trash"></i> Excluir
                                      </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;" onclick="updateStatus({{$product->id}})">
                                            <i class=" icon-close"></i> {{$product->status?'Desativar':'Ativar'}}
                                        </a>
                                    </li>
                                  </ul>
                                </div>
                              </td>
                          </tr>
                          @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

          </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop

@section('scripts')
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="{{ asset('admin/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/pages/scripts/table-datatables-buttons.js')}}" type="text/javascript"></script>
  <script>
    function confirmDelete(id){
      swal({
        title: "Deseja realmente excluir?",
        text: "Ao excluir, todos os itens relacionados ao produto serão removidos",
        icon: "warning",
        buttons: ["Cancelar", "Confirmar"],
        dangerMode: true,
      }).then((isConfirmed)=>{
        if (isConfirmed){
          $('#delete-'+id).submit();
        }
      });
    }

    function updateStatus(id){
      $('#status-'+id).submit();
    }

    $('#file').change(function () {
      $('#save_hidden').css('display','block');
      $('#save_hidden').click();
    });

  </script>
@stop
