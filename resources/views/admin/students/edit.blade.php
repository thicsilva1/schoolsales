@extends('admin.layouts.app')
@section('styles')
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
@stop
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('panel.index')}}">Painel</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                  <a href="{{route('students.index')}}">Alunos</a>
                  <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Editar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Alunos

        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('alert'))
          <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            {{ session('alert.message')}}
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-notebook font-dark"></i>
                        <span class="caption-subject bold uppercase">Edição de Aluno</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body form">
                  <form method="post" action="{{route('students.update', $student->id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="form-body">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group {{$errors->has('name')?'has-error':''}}">
                            <label for="name">Nome</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $student->name) }}">
                            @if($errors->has('name'))
                            <span class="text-danger">{{$errors->first('name') }}</span>
                            @endif
                          </div>

                          <div class="form-group">
                            <label for="team_id">Turma</label>
                            <select name="team_id" id="team_id" class="form-control select2">
                              @foreach($teams->groupBy('school') as $t)
                                <optgroup label="{{$t[0]->school}}">
                                @foreach($teams as $team)
                                @if($t[0]->school==$team->school)
                                <option value="{{$team->id}}" {{ $team->id==old('team_id', $student->team_id)?'selected':'' }} >{{$team->name}}</option>
                                @endif
                                @endforeach
                                </optgroup>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="period">Periodo</label>
                            <select name="period" id="period" class="form-control select2">
                              <option value="M" {{ old('period', $student->period)==='M'?'selected':'' }}>Manhã</option>
                              <option value="T" {{ old('period', $student->period)==='T'?'selected':'' }}>Tarde</option>
                              <option value="N" {{ old('period', $student->period)==='N'?'selected':'' }}>Noite</option>
                            </select>
                          </div>
                          <div class="form-group {{$errors->has('access_key')?'has-error':''}}">
                            <label for="access_key">Chave de Acesso</label>
                            <input type="text" class="form-control" name="access_key" id="access_key" value="{{ old('access_key', $student->access_key) }}">
                            @if($errors->has('access_key'))
                            <span class="text-danger">{{$errors->first('access_key') }}</span>
                            @endif
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="discount">Possui Desconto?</label>
                            <select name="discount" id="discount" class="form-control select2">
                              <option value="0" {{ old('discount', $student->discount)==0?'selected':'' }}>Não</option>
                              <option value="1" {{ old('discount', $student->discount)==1?'selected':'' }}>Sim</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="name">Permite receber sem compromisso?</label>
                            <select name="receive_without_commitment" id="receive_without_commitment" class="form-control select2">
                              <option value="Site" {{ old('receive_without_commitment', $student->receive_without_commitment)=='Site'?'selected':'' }}>Regra do Site</option>
                              <option value="Não" {{ old('receive_without_commitment', $student->receive_without_commitment)=='Não'?'selected':'' }}>Não</option>
                              <option value="Sim" {{ old('receive_without_commitment', $student->receive_without_commitment)=='Sim'?'selected':'' }}>Sim</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="name">Cadastro ativo</label>
                            <select name="status" id="status" class="form-control select2">
                              <option value="1" {{ old('status', $student->status)==1?'selected':'' }}>Sim</option>
                              <option value="0" {{ old('status', $student->status)==0?'selected':'' }}>Não</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="original_name">Nome Original</label>
                            <input type="text" class="form-control" name="original_name" id="original_name" value="{{ $student->original_name }}" readonly="true">
                          </div>

                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group {{$errors->has('photo')?'has-error':''}}">
                            <label for="photo">Foto</label>
                            <input type="file" name="photo" id="photo" data-preview-file-type="text" accept="image/*">
                             @if($errors->has('photo'))
                            <span class="text-danger">{{$errors->first('photo') }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions right">
                      <a href="{{route('students.index')}}" class="btn default">Cancelar</a>
                      <button class="btn btn-success">Salvar</button>
                    </div>
                  </form>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

          </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop
@section('scripts')
<script src="{{ asset('admin/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/fileinput.min.js"></script>
<script>
  @if(!empty($student->photo))
    $("#photo").fileinput({
      showUpload:false,
      showRemove: false,
      initialPreview: [
        "{{ asset('upload/images/students/' . $student->photo )}}"
      ],
      initialPreviewAsData: true,
      initialPreviewConfig: [{
        caption: "{{ $student->name }}",
        key: "{{ $student->id }}",
        url: "{{ url('painel/alunos/image/' . $student->id)}}",
        extra: {
          '_token': "{{csrf_token()}}"
        },
      }],
      overwriteInitial: false,
    });
  @else
    $("#photo").fileinput({'showUpload':false, 'previewFileType':'any'});
  @endif
</script>

@stop
