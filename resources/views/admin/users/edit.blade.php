@extends('admin.layouts.app')
@section('styles')
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
@stop
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('panel.index')}}">Painel</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('users.index')}}">Usuários</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Editar</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Usuários

        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('alert'))
          <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            {{ session('alert.message')}}
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-user font-dark"></i>
                        <span class="caption-subject bold uppercase">Edição de Usuário</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body form">
                  <form method="post" action="{{route('users.update', $user->id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="form-body">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group {{$errors->has('name')?'has-error':''}}">
                            <label for="name">Nome</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $user->name) }}">
                            @if($errors->has('name'))
                            <span class="text-danger">{{$errors->first('name') }}</span>
                            @endif
                          </div>
                          <div class="form-group {{$errors->has('email')?'has-error':''}}">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" value="{{ old('email', $user->email) }}">
                            @if($errors->has('email'))
                            <span class="text-danger">{{$errors->first('email') }}</span>
                            @endif
                          </div>
                          <div class="form-group">
                            <label for="phone">Fone</label>
                            <input type="text" class="form-control" name="phone" id="phone" value="{{ old('phone', $user->phone) }}">
                          </div>
                        </div>
                        <div class="col-md-6">
                          @if(auth()->user()->privilege)
                          <div class="form-group {{$errors->has('privilege')?'has-error':''}}">
                            <label for="privilege">Privilégio</label>
                            <select name="privilege" id="privilege" class="form-control select2">
                              <option value="0" {{ old('privilege', $user->privilege)==0?'selected':''}}>Usuário</option>
                              <option value="1" {{ old('privilege', $user->privilege)==1?'selected':''}}>Administrador</option>
                            </select>
                            @if($errors->has('privilege'))
                            <span class="text-danger">{{$errors->first('privilege') }}</span>
                            @endif
                          </div>
                          @endif
                          <div class="form-group {{$errors->has('status')?'has-error':''}}">
                            <label for="status">Cadastro ativo?</label>
                            <select name="status" id="status" class="form-control select2">
                              <option value="0" {{ old('status', $user->status)==0?'selected':''}}>Não</option>
                              <option value="1" {{ old('status', $user->status)==0?'selected':''}}>Sim</option>
                            </select>
                            @if($errors->has('status'))
                            <span class="text-danger">{{$errors->first('status') }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group {{$errors->has('profile_image')?'has-error':''}}">
                            <label for="profile_image">Foto</label>
                            <input type="file" name="profile_image" id="profile_image" data-preview-file-type="text" accept="image/*">
                             @if($errors->has('profile_image'))
                            <span class="text-danger">{{$errors->first('profile_image') }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions right">
                      <a href="{{route('users.index')}}" class="btn default">Cancelar</a>
                      <button class="btn btn-success">Salvar</button>
                    </div>
                  </form>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

          </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop
@section('scripts')
<script src="{{ asset('admin/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/fileinput.min.js"></script>
<script>
  $("#phone").inputmask({"mask": ['(99) 9999-9999', '(99) 99999-9999']});
  @if(!empty($user->profile_image))
    $("#profile_image").fileinput({
      showUpload:false,
      showRemove: false,
      initialPreview: [
        "{{ asset('upload/images/users/' . $user->profile_image )}}"
      ],
      initialPreviewAsData: true,
      initialPreviewConfig: [{
        caption: "{{ $user->name }}",
        key: "{{ $user->id }}",
        url: "{{ url('painel/usuarios/image/' . $user->id)}}",
        extra: {
          '_token': "{{csrf_token()}}"
        },
      }],
      overwriteInitial: false,
    });
  @else
    $("#profile_image").fileinput({'showUpload':false, 'previewFileType':'any'});
  @endif
</script>

@stop
