@extends('admin.layouts.app')

@section('styles')
  <link href="{{ asset('admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/assets/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/assets/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN PAGE BAR -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li>
              <span>Painel de Controle</span>
          </li>
        </ul>
      </div>
      <!-- END PAGE BAR -->
      <!-- BEGIN PAGE TITLE-->
      <h3 class="page-title"> Painel de Controle

      </h3>
      <!-- END PAGE TITLE-->
      <!-- END PAGE HEADER-->
      @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade in">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if (Session::has('alert'))
        <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          {{ session('alert.message')}}
        </div>
      @endif
      <!-- BEGIN DASHBOARD -->
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2">
            <a href="{{ route('students.index')}}">
              <div class="display">
                <div class="number">
                  <h3 class="font-blue-steel">
                    <span data-counter="counterup" data-value="{{$students}}">0</span>
                  </h3>
                  <small>Alunos</small>
                </div>
                <div class="icon">
                  <i class="icon-notebook"></i>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2">
            <a href="{{route('products.index')}}">
              <div class="display">
                <div class="number">
                  <h3 class="font-green-sharp">
                    <span data-counter="counterup" data-value="{{$products}}">0</span>
                  </h3>
                  <small>Produtos</small>
                </div>
                <div class="icon">
                  <i class="icon-social-dropbox"></i>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2">
            <a href="{{route('kits.index')}}">
              <div class="display">
                <div class="number">
                  <h3 class="font-blue-hoki">
                    <span data-counter="counterup" data-value="{{$kits}}">0</span>
                  </h3>
                  <small>Kits</small>
                </div>
                <div class="icon">
                  <i class="icon-puzzle"></i>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2">
            <a href="{{route('orders.filter', 'PAGO')}}">
              <div class="display">
                <div class="number">
                  <h3 class="font-green-jungle">
                    <span data-counter="counterup" data-value="{{$ordersPaid}}">0</span>
                  </h3>
                  <small>Pedidos Pagos</small>
                </div>
                <div class="icon">
                  <i class="icon-credit-card"></i>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2">
            <a href="{{route('orders.filter', 'PENDENTE')}}">
              <div class="display">
                <div class="number">
                  <h3 class="font-yellow-gold">
                    <span data-counter="counterup" data-value="{{$ordersPending}}">0</span>
                  </h3>
                  <small>Pedidos em Aberto</small>
                </div>
                <div class="icon">
                  <i class="icon-wallet"></i>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2">
            <a href="{{route('orders.filter', 'SEM COMPROMISSO')}}">
              <div class="display">
                <div class="number">
                  <h3 class="font-purple-wisteria">
                    <span data-counter="counterup" data-value="{{$ordersWithout}}">0</span>
                  </h3>
                  <small>Sem compromisso</small>
                </div>
                <div class="icon">
                  <i class="icon-present"></i>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="dashboard-stat2">
            <a href="{{route('notreceive.index')}}">
              <div class="display">
                <div class="number">
                  <h3 class="font-red-mint">
                    <span data-counter="counterup" data-value="{{$notReceive}}">0</span>
                  </h3>
                  <small>Não Receber</small>
                </div>
                <div class="icon">
                  <i class="icon-ban"></i>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>

      <!-- END DASHBOARD  -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop

@section('scripts')
  <script src="{{ asset('admin/assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/amcharts/amcharts/pie.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/amcharts/amcharts/radar.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/amcharts/amcharts/themes/patterns.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/amcharts/amcharts/themes/chalk.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/amcharts/ammap/ammap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/amcharts/amstockcharts/amstock.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/horizontal-timeline/horozontal-timeline.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}" type="text/javascript"></script>
  <script src="{{ asset('admin/assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
@stop
