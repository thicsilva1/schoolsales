@extends('admin.layouts.app')
@section('styles')
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
@stop
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('panel.index')}}">Painel</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Configurações</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Configurações

        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('alert'))
          <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            {{ session('alert.message')}}
          </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Alterar Configurações</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body form">
                  <form method="post" action="{{route('settings.update', $setting->id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="form-body">
                      <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                          <div class="form-group">
                            <label for="site_enabled">Site Habilitado?</label>
                            <select name="site_enabled" id="site_enabled" class="form-control select2">
                              <option value="0" {{ old('site_enabled', $setting->site_enabled)==0?'selected':'' }}>Não</option>
                              <option value="1" {{ old('site_enabled', $setting->site_enabled)==1?'selected':'' }}>Sim</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="name">Permite receber sem compromisso?</label>
                            <select name="allow_receive_without_commitment" id="receive_without_commitment" class="form-control select2">
                              <option value="0" {{ old('allow_receive_without_commitment', $setting->allow_receive_without_commitment)==0?'selected':'' }}>Não</option>
                              <option value="1" {{ old('allow_receive_without_commitment', $setting->allow_receive_without_commitment)==1?'selected':'' }}>Sim</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-actions right">
                      <a href="{{route('panel.index')}}" class="btn default">Cancelar</a>
                      <button class="btn btn-success">Salvar</button>
                    </div>
                  </form>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

          </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop
@section('scripts')
<script src="{{ asset('admin/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

@stop
