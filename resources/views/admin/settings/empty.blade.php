@extends('admin.layouts.app')
@section('styles')
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
@stop
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('panel.index')}}">Painel</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                  <a href="{{route('settings.index')}}">Configurações</a>
                  <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Zerar Site</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h3 class="page-title"> Zerar Site

        </h3>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('alert'))
          <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            {{ session('alert.message')}}
          </div>
        @endif
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-refresh font-dark"></i>
                        <span class="caption-subject bold uppercase">Zerar Site</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body form">
                  <form method="post" action="{{route('settings.empty.exec')}}" id="empty">
                    @csrf
                    <div class="form-body">
                      <input type="hidden" name="keyword" value="{{$keyword}}">
                      <label for="filename">Essa ação é irreversível! Digite a palavra <span class="label label-danger">{{$keyword}}</span> logo abaixo e clique em Confirmar para executar.</label>
                      <input type="text" class="form-control" name="keyword_confirmation" id="keyword_confirmation" autocomplete="off">
                    </div>
                    <div class="form-actions right">
                      <a href="{{route('panel.index')}}" class="btn default">Cancelar</a>
                      <button type="submit" class="btn btn-danger" id="confirm">Confirmar</button>
                    </div>
                  </form>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

          </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@stop
@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('admin/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script>
  $('#confirm').on('click', function(e){
    e.preventDefault();
    swal({
      title: "Deseja realmente zerar os dados do site?",
      text: "Ao zerar os dados, você perderá seus cadastros, exceto usuários do sistema e logs do banco de dados. Deseja continuar?",
      icon: 'warning',
      buttons: ["Cancelar", "Confirmar"],
      dangerMode: true,
    }).then((isConfirmed)=>{
      if (isConfirmed){
        $('#empty').submit();
      }
    })
  })
</script>
@stop
