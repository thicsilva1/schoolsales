<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
      <!-- BEGIN HEADER INNER -->
      <div class="page-header-inner ">
          <!-- BEGIN LOGO -->
          <div class="page-logo">
              <a href="{{route('panel.index')}}">
                  <img src="{{ asset('admin/logo.png')}}" style="max-width:120px;" alt="logo" class="logo-default" /> </a>
              <div class="menu-toggler sidebar-toggler">
                  <span></span>
              </div>
          </div>
          <!-- END LOGO -->
          <!-- BEGIN RESPONSIVE MENU TOGGLER -->
          <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
              <span></span>
          </a>
          <!-- END RESPONSIVE MENU TOGGLER -->
          <!-- BEGIN TOP NAVIGATION MENU -->
          <div class="top-menu">
              <ul class="nav navbar-nav pull-right">
                  <!-- BEGIN USER LOGIN DROPDOWN -->
                  <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                  <li class="dropdown dropdown-user">
                      <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                          <img alt="" class="img-circle" src="{{ auth()->user()->profile_image?asset('upload/images/users/' . auth()->user()->profile_image):asset('upload/images/avatar.png')}}" />
                          <span class="username username-hide-on-mobile"> {{auth()->user()->name}} </span>
                          <i class="fa fa-angle-down"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-default">
                          <li>
                            <a href="{{ route('users.edit.profile') }}"><i class="icon-user"></i>Editar Perfil</a>
                          </li>
                          <li>
                            <a href="{{ route('users.edit.password') }}"><i class="icon-shield"></i>Alterar Senha</a>
                          </li>
                          <li class="divider"></li>
                          <form action="{{ route('logout') }}" method="post" id="logout">
                            @csrf
                          </form>
                          <li>
                              <a href="#" onclick="event.preventDefault();document.getElementById('logout').submit()">
                              <i class="icon-key"></i> Sair </a>
                          </li>
                      </ul>
                  </li>
                  <!-- END USER LOGIN DROPDOWN -->
              </ul>
          </div>
          <!-- END TOP NAVIGATION MENU -->
      </div>
      <!-- END HEADER INNER -->
  </div>
  <!-- END HEADER -->
