<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                    <span></span>
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
          <li class="nav-item start {{request()->is('painel')?'active':''}}">
            <a href="{{route('panel.index')}}" class="nav-link">
              <i class="icon-home"></i>
              <span class="title">Painel de Controle</span>
              <span class="selected"></span>
            </a>
          </li>
          <li class="heading">
            <h3 class="uppercase">Administração</h3>
          </li>
          <li class="nav-item start {{request()->is('painel/nao-receber*')?'active':''}}">
              <a href="{{route('notreceive.index')}}" class="nav-link">
                  <i class="icon-ban"></i>
                  <span class="title">Não Receber</span>
                  <span class="selected"></span>
              </a>
          </li>
          <li class="nav-item start {{request()->is('painel/pedidos*')?'active':''}}">
              <a href="{{route('orders.index')}}" class="nav-link">
                  <i class="icon-basket"></i>
                  <span class="title">Pedidos</span>
                  <span class="selected"></span>
              </a>
          </li>
          <li class="nav-item start {{request()->is('painel/usuarios*')?'active':''}}">
            <a href="{{route('users.index')}}" class="nav-link">
              <i class="icon-user"></i>
              <span class="title">Usuários</span>
              <span class="selected"></span>
            </a>
          </li>
          <li class="heading">
            <h3 class="uppercase">Cadastros</h3>
          </li>
          <li class="nav-item start {{request()->is('painel/alunos*')?'active':''}}">
              <a href="{{route('students.index')}}" class="nav-link">
                  <i class="icon-notebook"></i>
                  <span class="title">Alunos </span>
                  <span class="selected"></span>
              </a>
          </li>
          <li class="nav-item start {{request()->is('painel/cupons*')?'active':''}}">
            <a href="{{route('coupons.index')}}" class="nav-link">
                <i class="icon-tag"></i>
                <span class="title">Cupons de Desconto</span>
                <span class="selected"></span>
            </a>
          </li>
          <li class="nav-item start {{request()->is('painel/kits*')?'active':''}}">
              <a href="{{route('kits.index')}}" class="nav-link">
                  <i class="icon-puzzle"></i>
                  <span class="title">Kits</span>
                  <span class="selected"></span>
              </a>
          </li>

          <li class="nav-item start {{request()->is('painel/produtos*')?'active':''}}">
              <a href="{{route('products.index')}}" class="nav-link">
                  <i class="icon-social-dropbox"></i>
                  <span class="title">Produtos</span>
                  <span class="selected"></span>
              </a>
          </li>
          <li class="nav-item start {{request()->is('painel/textos*')?'active':''}}">
            <a href="{{route('texts.index')}}" class="nav-link">
              <i class="icon-doc"></i>
              <span class="title">Textos</span>
              <span class="selected"></span>
            </a>
          </li>
          <li class="nav-item start {{request()->is('painel/turmas*')?'active':''}}">
              <a href="{{route('teams.index')}}" class="nav-link">
                  <i class="icon-graduation"></i>
                  <span class="title">Turmas</span>
                  <span class="selected"></span>
              </a>
          </li>
          <li class="heading">
            <h3 class="uppercase">Configurações</h3>
          </li>
          <li class="nav-item start {{request()->is('painel/configuracoes*')?'active':''}}">
              <a href="javascript:;" class="nav-link nav-toggle">
                  <i class="icon-settings"></i>
                  <span class="title">Configuração</span>
                  <span class="selected"></span>
                  <span class="arrow"></span>
              </a>
              <ul class="sub-menu">
                <li class="nav-item start">
                  <a href="{{route('settings.index')}}" class="nav-link">
                    <span class="title">Configurações do Site</span>
                  </a>
                </li>
                <li class="nav-item start {{request()->is('painel/configuracoes/*')?'active':''}}">
                  <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-folder"></i>
                    <span class="title">Backup e Restauração</span>
                    <span class="arrow"></span>
                  </a>
                  <ul class="sub-menu">
                    <li class="nav-item {{request()->is('painel/configuracoes/backup')?'active':''}}">
                      <a href="{{route('settings.backup')}}" class="nav-link">
                        <span class="title">Backup</span>
                      </a>
                    </li>
                    <li class="nav-item {{request()->is('painel/configuracoes/restaurar')?'active':''}}">
                      <a href="{{route('settings.restore')}}" class="nav-link">
                        <span class="title">Restauração</span>
                      </a>
                    </li>
                    <li class="nav-item {{request()->is('painel/configuracoes/zerar')?'active':''}}">
                      <a href="{{route('settings.empty')}}" class="nav-link">Zerar site</a>
                    </li>
                    <li class="nav-item {{request()->is('painel/configuracoes/eventos')?'active':''}}">
                      <a href="{{route('settings.log')}}" class="nav-link">Eventos</a>
                    </li>
                  </ul>

                </li>
              </ul>
          </li>
          <li class="nav-item start {{request()->is('painel/log*')?'active':''}}">
              <a href="{{route('logs.index')}}" class="nav-link">
                  <i class="icon-disc"></i>
                  <span class="title">Log dos Registros</span>
                  <span class="selected"></span>
              </a>
          </li>
          <li class="nav-item start {{request()->is('painel/sincronizar-fotos*')?'active':''}}">
              <a href="{{route('settings.sync.photos')}}" class="nav-link">
                  <i class="icon-refresh"></i>
                  <span class="title">Sincronizar Fotos</span>
                  <span class="selected"></span>
              </a>
          </li>
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
