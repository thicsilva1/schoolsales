@extends('site.layouts.app')
@section('content')

  <!-- content -->

    <div class="row justify-content-md-center">
      <div class="col-lg-9 col-sm-11 text-center">
        <h3>Por favor, confirme se você é responsável pelo(a) aluno(a):</h3>
        <h1>{{ $student->name }}</h1>
        <hr>
        <a href="{{ route('student') }}" class="btn btn-success">Sim</a>
        <form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">@csrf</form>
        <a href="#" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Não</a>
      </div>
    </div>


  <!-- end content-->
@stop
