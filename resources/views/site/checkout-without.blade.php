@extends('site.layouts.app')

@section('content')

  <div class="row mt-3">
    <div class="col-md-12 text-center">
      <h3>{{ $text->title }}</h3>
      {!! $text->description !!}
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-12 col-sm-12 col-md-12 text-center">
      <h3>Carrinho de Compras</h3>
      <div class="table-responsive mt-3">
        <table class="table table-sm">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Aluno</th>
              <th scope="col">Nome do Kit</th>
              <th scope="col">Quantidade</th>
              <th scope="col" class="text-left">Valor</th>
              <th scope="col" class="text-left">Subtotal</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @php($total=0)


            @foreach($order->kits as $detail)
            <tr>
              <td scope="row" >{{$detail->student->name}}</td>
              <td scope="row" >{{$detail->kit->name}}</td>
              <td scope="row">
                {{ $detail->quantity }}
              </td>
              <td scope="row" class="text-left">R$ {{number_format($detail->value, 2, ',', '.') }}</td>
              <td scope="row" class="text-left">R$ {{number_format($detail->value*$detail->quantity, 2, ',', '.') }}</td>
              <td scope="row">
                <form action="{{route('cart.deletekit', $detail->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-dark btn-sm"><i class="fa fa-trash"></i></button>
                </form>
              </td>
              @if($detail->products->count()>0)
              <tr class="table-secondary">
                <td scope="col" colspan="6" class="text-center">Produtos Adicionados ao Kit</td>
              </tr>
              @endif
              @foreach($detail->products as $prod)
              <tr>
                <td></td>
                <td scope="row">{{ $prod->product->name }}</td>
                <td scope="row" class="text-left">
                  <form action="{{route('cart.updateproduct', $prod->id)}}" method="post">
                    @csrf
                    @method('PATCH')
                    <input type="number" name="product_quantity" class="form-control quantity" value="{{ $prod->quantity }}" step="1" min="1">
                  </form>
                </td>
                <td scope="row" class="text-left">R$ {{ number_format($prod->value, 2, ',', '.') }}</td>
                <td scope="row" class="text-left">R$ {{ number_format($prod->value*$prod->quantity, 2, ',', '.') }}</td>
                <td>
                  <form action="{{route('cart.deleteproduct', $prod->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-secondary btn-sm"><i class="fa fa-trash"></i></button>
                  </form>
                </td>
              </tr>
              @endforeach
              <tr>
                <td colspan="6"></td>
              </tr>
            </tr>
            @endforeach
            @if(isset($order->coupon_id))
            <tr>
              <td colspan="5" scope=row class="text-right"><strong>SubTotal</strong></td>
              <td scope="row" class="text-left"><strong>R$ {{ number_format($order->subtotal, 2, ',', '.') }}</strong></td>
            </tr>
            <tr>
              <td colspan="5" scope=row class="text-right">
                Cupom:<strong> {{$order->coupon->code}}</strong>
                <br>
                @if($order->coupon->type=='Valor')
                <small>Desconto de R$ {{number_format($order->coupon->value, 2, ',', '.')}}</small>
                @else
                <small>Desconto de {{number_format($order->coupon->value, 0, ',', '.')}}%</small>
                @endif
              </td>
              <td scope="row" class="text-left text-danger"><strong>- R$ {{ number_format($order->discount, 2, ',', '.') }}</strong></td>
            </tr>
            @endif
            <tr>
              <td colspan="5" scope=row class="text-right"><strong>Total Geral</strong></td>
              <td scope="row" class="text-left"><strong>R$ {{ number_format($order->total, 2, ',', '.') }}</strong></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>


  <div class="row d-print-none">
    <div class="col-md-12 text-center">
      <a href="{{route('home')}}" class="btn btn-outline-primary btn-sm">Voltar</a>
    </div>
  </div>

  <div class="row mt-5"></div>

@stop
