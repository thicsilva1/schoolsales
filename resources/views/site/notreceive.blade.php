@extends('site.layouts.app')

@section('content')

  <div class="row">
    <div class="col text-center">
      <p>Você optou por não receber nosso kit. </p>
    </div>
  </div>

  <div class="row mt-3">
    <div class="col-12 col-sm-12 col-md-12 text-center">
      <h3>Carrinho de Compras</h3>
      <div class="table-responsive-md mt-3">
        <table class="table table-sm">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Responsável</th>
              <th scope="col">Nome do Aluno</th>
              <th scope="col" class="text-left">Valor</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td scope="row" >{{$nr->relative->name}}</td>
              <td scope="row" >{{$nr->student->name}}</td>
              <td scope="row"> R$ 0,00</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="row mt-5">
    <div class="col-12 text-center">
      <form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">@csrf</form>
      <a class="btn btn-outline-danger btn-sm" href="#" id="logout"><i class="fa fa-sign-out"></i> Sair
      </a>
    </div>
  </div>


@stop

@section('scripts')

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
     (function($){
      'use strict';
      $(function(){

        sessionStorage.clear();
        $('a#logout').on('click', function(e){
          e.preventDefault();
          swal({
            title: "Deseja realmente sair?",
            icon: "warning",
            buttons: ["Cancelar", "Confirmar"],
            dangerMode: true,
          }).then((isConfirmed)=>{
            if (isConfirmed){
              document.getElementById('logout-form').submit();
            }
          });
        });

      })
     })(jQuery);
  </script>
@stop
