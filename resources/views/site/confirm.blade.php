@extends('site.layouts.app')

@section('content')
<!-- content -->

  @if ($errors->any())
  <div class="alert alert-danger alert-dismissible fade show">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      <span class="sr-only">Close</span>
    </button>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  <div class="row justify-content-md-center">
    <div class="col-lg-7 col-sm-11">
      <h3>Por favor, confirme seus dados para continuar:</h3>
      <form method="post" action="{{route('create.relative')}}">
        {{ csrf_field() }}
        <div class="form-group { {$errors->has('name')?'has-danger':''}}">
            <label for="name">Nome</label>
            <input type="text" class="form-control" name="name" id="name" value="{{old('name', optional($relative)->name) }}">
        </div>
        <div class="form-group {{$errors->has('email')?'has-danger':''}}">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" name="email" id="email" value="{{ old('email', optional($relative)->email) }}">
        </div>

        <div class="form-group {{$errors->has('cpf')?'has-danger':''}}">
            <label for="cpf">CPF</label>
            <input type="text" class="form-control" name="cpf" id="cpf" value="{{ old('cpf', optional($relative)->cpf) }}">
        </div>

        <div class="form-group {{$errors->has('phone')?'has-danger':''}}">
            <label for="Telefone">Telefone</label>
            <input type="text" class="form-control" name="phone" id="phone" value="{{ old('phone', optional($relative)->phone) }}">
        </div>

        <button type="submit" class="btn btn-success">Confirmar</button>
      </form>
    </div>
  </div>

<!-- end content-->

@stop

@section('scripts')
	<script src="{{ asset('site/js/jquery.mask.js') }}"></script>
	<script>
    (function($){
      'use strict';
      $(function(){

        $('#cpf').mask('999.999.999-99', {reverse:false});
        var MaskBehavior = function (val) {
          return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        maskOpt = {
          onKeyPress: function(val, e, field, options) {
              field.mask(MaskBehavior.apply({}, arguments), options);
            }
        };
        $('#phone').mask(MaskBehavior, maskOpt);
      })
    })(jQuery);
	</script>
@stop
