@extends('site.layouts.app')

@section('content')

  <div class="row mt-3">
    <div class="col-md-12 text-center">
      <h3>Obrigado!</h3>
      Seu pedido foi confirmado e será entregue na escola.
      <hr>
      <h3></h3>
      <div class="table-responsive mt-3">
        <table class="table table-sm">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Aluno</th>
              <th scope="col">Nome do Kit</th>
              <th scope="col">Quantidade</th>
              <th scope="col" class="text-left">Valor</th>
              <th scope="col" class="text-left">Subtotal</th>
            </tr>
          </thead>
          <tbody>
            @foreach($order->kits as $detail)

            <tr>
              <td scope="row" >{{$detail->student->name}}</td>
              <td scope="row" >{{$detail->kit->name}}</td>
              <td scope="row">{{ $detail->quantity }}</td>
              <td scope="row" class="text-left">R$ {{number_format($detail->value, 2, ',', '.') }}</td>
              <td scope="row" class="text-left">R$ {{number_format($detail->value*$detail->quantity, 2, ',', '.') }}</td>
              @if($detail->products->count()>0)
              <tr class="table-secondary">
                <td scope="col" colspan="6" class="text-center">Produtos Adicionados ao Kit</td>
              </tr>
              @endif
              @foreach($detail->products as $prod)
              <tr>
                <td></td>
                <td scope="row">{{ $prod->product->name }}</td>
                <td scope="row" class="text-left">{{$prod->quantity}}</td>
                <td scope="row" class="text-left">R$ {{ number_format($prod->value, 2, ',', '.') }}</td>
                <td scope="row" class="text-left">R$ {{ number_format($prod->value*$prod->quantity, 2, ',', '.') }}</td>
              </tr>
              @endforeach
              <tr>
                <td colspan="5"></td>
              </tr>
            </tr>
            @endforeach
            @if(isset($order->coupon_id))
            <tr>
              <td colspan="4" scope=row class="text-right"><strong>SubTotal</strong></td>
              <td scope="row" class="text-left"><strong>R$ {{ number_format($order->subtotal, 2, ',', '.') }}</strong></td>
            </tr>
            <tr>
              <td colspan="4" scope=row class="text-right">
                Cupom:<strong> {{$order->coupon->code}}</strong>
                <br>
                @if($order->coupon->type=='Valor')
                <small>Desconto de R$ {{number_format($order->coupon->value, 2, ',', '.')}}</small>
                @else
                <small>Desconto de {{number_format($order->coupon->value, 0, ',', '.')}}%</small>
                @endif
              </td>
              <td scope="row" class="text-left text-danger"><strong>- R$ {{ number_format($order->discount, 2, ',', '.') }}</strong></td>
            </tr>
            @endif
            <tr>
              <td colspan="4" scope=row class="text-right"><strong>Total Geral</strong></td>
              <td scope="row" class="text-left"><strong>R$ {{ number_format($order->total, 2, ',', '.') }}</strong></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="row mt-5 mb-4">
    <div class="col-12 col-sm-12 col-md-12 text-center">
      <form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">@csrf</form>
        <a class="btn btn-outline-danger btn-sm" href="#" id="logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Sair</a>
    </div>
  </div>

@stop

@section('scripts')
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
  (function($){
      'use strict';
      $(function(){
        $('a#logout').on('click', function(e){
          e.preventDefault();
          swal({
            title: "Deseja realmente sair?",
            icon: "warning",
            buttons: ["Cancelar", "Confirmar"],
            dangerMode: true,
          }).then((isConfirmed)=>{
            if (isConfirmed){
              document.getElementById('logout-form').submit();
            }
          });
        });
      });
  })(jQuery);
  </script>
@stop
