@extends('site.layouts.app')

@section('content')

  <div class="row mt-3">
    <div class="col-md-12 text-center">
      <h3>{{ $text->title }}</h3>
      {!! $text->description !!}
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-12 col-sm-12 col-md-12 text-center">
      <h3>Carrinho de Compras</h3>
      <div class="table-responsive mt-3">
        <table class="table table-sm">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Aluno</th>
              <th scope="col">Nome do Kit</th>
              <th scope="col">Quantidade</th>
              <th scope="col" class="text-left">Valor</th>
              <th scope="col" class="text-left">Subtotal</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @php($total=0)


            @foreach($order->kits as $detail)

            <tr>
              <td scope="row" >{{$detail->student->name}}</td>
              <td scope="row" >{{$detail->kit->name}}</td>
              <td scope="row">
                <form action="{{route('cart.updatekit', $detail->id)}}" method="post">
                  @csrf
                  @method('PATCH')
                  @if(!$order->payment_id)
                  <input type="number" name="kit_quantity" class="form-control quantity" value="{{ $detail->quantity }}" step="1" min="1">
                  @else
                  {{ $detail->quantity }}
                  @endif
                </form>
              </td>
              <td scope="row" class="text-left">R$ {{number_format($detail->value, 2, ',', '.') }}</td>
              <td scope="row" class="text-left">R$ {{number_format($detail->value*$detail->quantity, 2, ',', '.') }}</td>
              <td scope="row">
              @if(!$order->payment_id)
                <form action="{{route('cart.deletekit', $detail->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-dark btn-sm"><i class="fa fa-trash"></i></button>
                </form>
              @endif
              </td>
              @if($detail->products->count()>0)
              <tr class="table-secondary">
                <td scope="col" colspan="6" class="text-center">Produtos Adicionados ao Kit</td>
              </tr>
              @endif
              @foreach($detail->products as $prod)
              <tr>
                <td></td>
                <td scope="row">{{ $prod->product->name }}</td>
                <td scope="row" class="text-left">
                  <form action="{{route('cart.updateproduct', $prod->id)}}" method="post">
                    @csrf
                    @method('PATCH')
                    @if(!$order->payment_id)
                    <input type="number" name="product_quantity" class="form-control quantity" value="{{ $prod->quantity }}" step="1" min="1">
                    @else
                     {{$prod->quantity}}
                    @endif
                  </form>
                </td>
                <td scope="row" class="text-left">R$ {{ number_format($prod->value, 2, ',', '.') }}</td>
                <td scope="row" class="text-left">R$ {{ number_format($prod->value*$prod->quantity, 2, ',', '.') }}</td>
                <td>
                @if(!$order->payment_id)
                  <form action="{{route('cart.deleteproduct', $prod->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-secondary btn-sm"><i class="fa fa-trash"></i></button>
                  </form>
                @endif
                </td>
              </tr>
              @endforeach
              <tr>
                <td colspan="6"></td>
              </tr>
            </tr>
            @endforeach
            @if(isset($order->coupon_id))
            <tr>
              <td colspan="5" scope=row class="text-right"><strong>SubTotal</strong></td>
              <td scope="row" class="text-left"><strong>R$ {{ number_format($order->subtotal, 2, ',', '.') }}</strong></td>
            </tr>
            <tr>
              <td colspan="5" scope=row class="text-right">
                Cupom:<strong> {{$order->coupon->code}}</strong>
                <br>
                @if($order->coupon->type=='Valor')
                <small>Desconto de R$ {{number_format($order->coupon->value, 2, ',', '.')}}</small>
                @else
                <small>Desconto de {{number_format($order->coupon->value, 0, ',', '.')}}%</small>
                @endif
              </td>
              <td scope="row" class="text-left text-danger"><strong>- R$ {{ number_format($order->discount, 2, ',', '.') }}</strong></td>
            </tr>
            @endif
            <tr>
              <td colspan="5" scope=row class="text-right"><strong>Total Geral</strong></td>
              <td scope="row" class="text-left"><strong>R$ {{ number_format($order->total, 2, ',', '.') }}</strong></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @if(!$order->payment_id)
  <div class="row mt-3 d-print-none">
    <div class="col-md-6">
    </div>
    <div class="col-md-6">
      @if($order->type=='Venda')
      <form id="apply-coupon">
        @csrf
        <input type="hidden" name="order" value="{{$order->id}}">
        <div class="form-row  has-feedback">
          <div class="col-md-8">
            <input type="text" class="form-control" id="coupon" name="coupon" placeholder="Cupom de Desconto" autocomplete="off" required>
            <span class="text-danger">
            <p id="not-found"></p>
            </span>
          </div>
          <div class="col-md-4">
            <button class="btn btn-primary">Aplicar</button>
          </div>
        </div>
      </form>
      @endif
    </div>
  </div>

  <div class="row d-print-none">
    <div class="col-md-6 text-right">
      <!-- INICIO FORMULARIO BOTAO PAGSEGURO -->
      <form action="https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html" method="post" id="form-pagseguro">
        <input type="image" id="pay-button" src="https://stc.pagseguro.uol.com.br/public/img/botoes/pagamentos/209x48-pagar-azul-assina.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!" />
      </form>
      <!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
    </div>
    <div class="col-md-6">
      <a href="{{route('home')}}" class="btn btn-outline-primary btn-sm">Continuar Comprando</a>
    </div>
  </div>

  <div class="row mb-8 d-print-none">
    <div class="offset-3 col-6">
      <form id="continue-buy">
        <p>Você pode continuar comprando para outro aluno, basta preencher a chave de acesso abaixo.</p>
        @csrf
        <div class="form-row has-feedback">
          <div class="col-8">
            <input type="hidden" name="order" id="order" value="{{$order->id}}">
            <input type="text" class="form-control" name="key" id="key" placeholder="Chave de Acesso">
            <span class="text-danger">
              <p id="key-error"></p>
            </span>
          </div>
          <div class="col-4">
            <input type="submit" value="Continuar" class="btn btn-danger">
          </div>
        </div>
      </form>
    </div>
  </div>
@else

  <div class="row d-print-none">
    <div class="col-md-6 text-right">
    </div>
    <div class="col-md-6">
      <a href="{{route('home')}}" class="btn btn-outline-primary btn-sm">Voltar</a>
    </div>
  </div>
  <div class="row"></div>
@endif

@stop

@section('scripts')
<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>
     (function($){
      'use strict';
      $(function(){

        sessionStorage.clear();

        $("#apply-coupon").on('submit', function(e){
          e.preventDefault();
          var form = $('#apply-coupon');
          var data = form.serialize();
          $.ajax({
            type: "POST",
            url: "{{ route('applyCoupon') }}",
            data: data,
            success: function(response){
              console.log(response);
              if (response.error){
                $('#not-found').html(response.msg);
              }
              if (response.success){
                location.reload();
              }
            },
          })
        });

        $("#continue-buy").on('submit', function(e){
          e.preventDefault();
          var form = $('#continue-buy');
          var data = form.serialize();
          $.ajax({
            type: "POST",
            url: "{{ route('continueBuy') }}",
            data: data,
            success: function(response){
              console.log(response);
              if (response.error){
                $('#key-error').html(response.msg)
              }
              if (response.success){
                location.href="{{ route('home') }}";
              }
            }
          })
        });
        $('.form-control.quantity').each(function(){
          $(this).on('blur', function(){
            var form = $(this).parent('form');
            form.submit();
          })
        })

        $('#pay-button').on('click', function(e){
          e.preventDefault();
          $.ajax({
            method: 'POST',
            url: "{{ route('createPagSeguro')}}",
            data: {
              "_token": "{{ csrf_token() }}",
              "order": {{$order->id}}
            },
            success: function(response){
              console.log(response);
              if (response.error){
                swal('Algo deu errado na sua compra. Tente efetuar novamente');
              }
              if (response.exists){
                location.href="{{route('pagseguroConfirmed', $order->id)}}";
              }
              if(response.success){
                PagSeguroLightbox({
                  'code': response.msg.code
                }, {
                  'success': function(code){
                    $.ajax({
                      method: 'POST',
                      url: "{{route('createPayment')}}",
                      data: {
                        "_token" : "{{ csrf_token() }}",
                        "order" : {{$order->id}},
                        "transaction": code,
                      },
                      success: function(response){
                        if (response.success){
                          location.href="{{route('pagseguroConfirmed', $order->id)}}";
                        }
                      }
                    })
                  },
                  'abort': function(){
                    swal('Algo deu errado na sua compra. Tente efetuar novamente');
                  }
                });
              }
            }
          })
        })

      })
     })(jQuery);
  </script>
@stop
