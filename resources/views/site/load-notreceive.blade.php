@extends('site.layouts.app')

@section('content')

  <div class="row">
    <div class="offset-2 col-8">
      <h1>{{$text->title}}</h1>
      <p>{!!$text->description!!}</p>
    </div>
  </div>

  <div class="row mt-5">
    <div class="col-12 text-center">
      <form id="logout-form" action="{{ route('signout') }}" method="POST">
      @csrf
        <a href="{{route('home')}}" class="btn btn-success"><i class="fa fa-check"></i> Sim</a>
        <button type="submit" class="btn btn-danger" href="#" id="logout">
          <i class="fa fa-close"></i> Não
        </button>
      </form>
    </div>
  </div>


@stop

