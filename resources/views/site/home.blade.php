@extends('site.layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('/site/fancybox/jquery.fancybox.min.css') }}" type="text/css" media="screen" />
@stop

@section('content')
<!-- content -->
  @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
      </button>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  @if(session()->has('alert'))
  <div class="alert {{session('alert.type')==='success'?'alert-success':'alert-danger'}} alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      <span class="sr-only">Close</span>
    </button>
    {{ session('alert.message')}}
  </div>
  @endif


  <div class="row">
    <div class="col-12 col-sm-12 col-md-12 text-center">
      <div class="display-name">
        <h1>{{ $student->name }}</h1>
      </div>
      <div class="edit-name mb-3">
        <form class="form form-inline justify-content-center" name="edit-name" action="{{ route('update.student', $student->id)}}" method="post">
          @csrf
          <div class="form-group mr-3">
            <input type="hidden" name="original_name" value="{{ $student->name }}">
            <input type="text" id="name" name="name" class="form-control" value="{{ $student->name }}">
          </div>
          <div class="button-group">
            <button class="btn btn-danger" type="submit" id="submit"><i class="fa fa-save"></i> Salvar</button>
            <button class="btn btn-danger" type="button" id="cancel"><i class="fa fa-close"></i> Cancelar</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 text-center">
      <button type="button" id="editar" class="btn btn-outline-danger btn-sm"><i class="fa fa-edit"></i> Editar</button>
    </div>
  </div>

  <div class="row">
    <div class="col-12 col-sm-12 col-md-6 text-center">
      <a href="{{ $student->photo==''? asset('upload/images/250.png') : asset('upload/images/students/' . $student->photo) }}" data-fancybox>
        <img src="{{ $student->photo==''? asset('upload/images/250.png') : asset('upload/images/students/' . $student->photo) }}" class="img-fluid center-cropped">
      </a>
    </div>
    <div class="col-12 col-sm-12 col-md-6 text-center">
      <a href="{{ $student->team->image == '' ? asset('upload/images/800x250.png'): asset('upload/images/teams/' . $student->team->image) }}" data-fancybox>
        <img src="{{ $student->team->image == '' ? asset('upload/images/800x250.png'): asset('upload/images/teams/' . $student->team->image) }}" class="img-fluid center-portrait">
      </a>
    </div>
  </div>

  <div class="row mt-4">
    <div class="col text-center">
      <h2>{{$text->title}}</h2>
      {!!$text->description!!}
    </div>
  </div>

@if(!session()->get('not_receive'))
<!-- Lista de Kits-->
@foreach($kits as $key=>$kit)
  <div class="row m-4 border p-3">
    <div class="col-12 col-sm-12 col-md-6 col-lg-2">
      @if(!empty($kit->image))
      <a href="{{ asset('upload/images/kits/'. $kit->image)}}" data-fancybox data-caption="{{ $kit->description }}">
        <img src="{{ asset('upload/images/kits/'. $kit->image)}}" alt="Foto do Kit" class="img-fluid img-kit">
      @else
      <a href="{{ asset('upload/images/kits/'. $kit->name . '.jpg')}}" data-fancybox data-caption="{{ $kit->description }}">
        <img src="{{ asset('upload/images/kits/'. $kit->name . '.jpg')}}" alt="Foto do Kit" class="img-fluid img-kit">
      @endif
      </a>
    </div>
    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
      <h3>{{ $key+1 }} - {{ $kit->name }}
        @if($student->discount && $kit->discount>0)
        @php
        $price = $kit->price-($kit->price * ($kit->discount/100));
        @endphp
          de <del>R${{number_format($kit->old_price, 2, ',', '.')}}</del> por R${{number_format($price, 2, ',', '.')}}
        @else
          @php
          $price = $kit->price;
          @endphp
          @if(isset($kit->old_price) && $kit->old_price>$kit->price)
            de <del>R${{number_format($kit->old_price, 2, ',', '.')}}</del> por R${{number_format($price, 2, ',', '.')}}
          @else
            por R${{number_format($price, 2, ',', '.')}}
          @endif
        @endif
      </h3>
      <p>{{ $kit->description }}</p>
      @if($student->discount && number_format($kit->discount)>0)
      <p class="text-danger font-weight-bold">Você tem um desconto especial para este kit</p>
      @endif
    </div>
    <div class="col-6 col-xm-6 col-sm-6 col-md-6 col-lg-2">
      <div class="kit-quantity">
        <input type="number" class="form-control form-control-sm" value="0" min="0" data-id="{{$kit->id}}" data-student="{{$student->id}}">
      </div>
    </div>
    <div class="col-6 col-xm-6 col-sm-6 col-md-6 col-lg-2">
      <button class="btn btn-sm btn-primary select" href="#" type="button" role="button" data-id="{{$kit->id}}" data-student="{{$student->id}}" data-quantity="0" data-value="{{$price}}" data-selected="false" data-receive="{{$settings->allow_receive_without_commitment && $kit->receive_without_commitment=='Site'?'Sim':$kit->receive_without_commitment}}">Selecionar kit</button>
    </div>
  @if($kit->allow_extra_products)
    @foreach($products as $product)
    <div class="col-12 col-xm-12 col-sm-12 col-md-6 col-lg-4">
      <div class="product hide">
        <div class="product-image">
          <a href="{{ asset('upload/images/products/' . $product->photo) }}" data-fancybox data-caption="{{ $product->description }}">
            <img src="{{asset('upload/images/products/' . $product->photo) }}" alt="Foto do Produto" class="img-fluid">
          </a>
        </div>
        <div class="product-content">
          <h5>{{$product->name}} @if(!empty($product->description)) <small><i class="fa fa-info-circle text-danger" data-toggle="tooltip" data-html="true" title="{!! $product->description !!}"></i></small>@endif</h5>
          <h5 class="text-success">R${{ number_format($product->price, 2, ',', '.') }}</h5>
        </div>
        <div class="product-footer">
          <div class="row justify-content-center">
            <button class="btn fa fa-plus-square text-success" disabled></button>
            <span class="p-10" data-product="{{ $product->id }}" data-kit="{{ $kit->id }}" data-student="{{$student->id}}" data-value="{{ $product->price }}">0</span>
            <button class="btn fa fa-minus-square text-danger" disabled></button>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  @endif
  </div>
@endforeach
<!-- end of Lista de kits -->
  <div class="row">
    <div class="col-12 col-sm-12 col-md-12 text-center">
      Valor Total: <strong>R$<span id="total">0</strong></span>
    </div>
  </div>
  <div class="row justify-content-center">
    {{ $kits->links() }}
  </div>
  <form id="cart">
    <div class="row text-center">
      @csrf
      @if(session()->get('order_id'))
      <input type="hidden" name="order" id="order" value="{{ session()->get('order_id') }}">
      @endif
      <div id="input-hiddens"></div>
      <div class="col-12 col-sm-12 col-md-4">
        @if($settings->allow_receive_without_commitment && ($student->receive_without_commitment==='Sim'||$student->receive_without_commitment==='Site'))
        <button type="button" class="btn btn-primary" id="receive-without-commitment">Receber sem compromisso</button>
        @endif
      </div>
      <div class="col-12 col-sm-12 col-md-4">
        <button type="submit" class="btn btn-success" id="buy-now">Comprar Agora</button>
      </div>
      <div class="col-12 col-sm-12 col-md-4">
        <button type="button" class="btn btn-danger" id="not-receive">Não desejo receber</button>
      </div>
    </div>
  </form>
  <form id="notreceive" method="post" action="{{route('notReceive')}}">
    @csrf
    <input type="hidden" name="relative_id" id="relative_id" value="{{session()->get('relative_id')}}">
    <input type="hidden" name="student_id" id="student_id" value="{{session()->get('student_id')}}">
  </form>
@endif
  <div class="row mt-5 mb-4">
    <div class="col-12 col-sm-12 col-md-12 text-center">
      <form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">@csrf</form>
        <a class="btn btn-outline-danger btn-sm" href="#" id="logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Sair</a>
    </div>
  </div>

<!-- end content-->

@stop

@section('scripts')
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript" src="{{ asset('site/fancybox/jquery.fancybox.min.js')}}"></script>
  <script>
    (function($){
      'use strict';
      $(function(){
        @if(session()->has('cart'))
        swal({
            text: "Você possui itens deixados no carrinho na sua última compra. Deseja recuperar ou iniciar uma nova compra?",
            icon: "warning",
            buttons: ["Iniciar Nova Compra", "Recuperar"],
            dangerMode: false,
          }).then((isConfirmed)=>{
            if (isConfirmed){
              <?php
              session()->put('order_id', session()->get('cart'));
              ?>
              location.href = "{{route('view-order', session()->get('cart'))}}";
            } else {
              $.ajax({
                method: "DELETE",
                url: "{{ route('cancelOrder')}}",
                data: {"_token": "{{ csrf_token() }}",
                  "order": {{session()->get('cart')}}},
                success: function(response){
                  if (response.success){
                    console.log('sucesso');
                  }
                }
              })
            }
          });
        @endif

        carregarKits();

        function carregarKits(){
          $('span[data-student="{{$student->id}}"]').each(function(){
            $(this).html('0');
          });
          @if(session()->has('order_id'))
          var naoPermitir = true;
          @else
          var naoPermitir = false;
          @endif
          if (sessionStorage.getItem('kitCart')){
            var student = $('.btn.select').first().data('student');
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i=0; i<items.length; i++){
              for (var j in items[i].products){
                $('span[data-product="'+ items[i].products[j].id +'"][data-kit="'+ items[i].products[j].kit+'"][data-student="'+ items[i].products[j].student+'"]').html(items[i].products[j].quantity);
              }
              $('input[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').val(items[i].quantity);
              $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').data('selected', true);
              $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').data('quantity', items[i].quantity);
              $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').addClass('btn-success');
              $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]').html('<i class="fa fa-check" aria-hidden="true"></i>Selecionado');
              var button = $('.btn.select[data-id="'+ items[i].id +'"][data-student="'+ items[i].student+'"]');
              var prodAdd = button.closest('.row').find('button.fa-plus-square');
              var prodDel = button.closest('.row').find('button.fa-minus-square');
              var prod = button.parents().closest('.row').find('.product');
              prodAdd.each(function(){
                $(this).attr('disabled', false);
              });
              prodDel.each(function(){
                $(this).attr('disabled', false);
              });
              prod.each(function(){
                $(this).removeClass('hide');
              });
              if (items[i].receive=='Não' || (items[i].receive=='Sim' &&  (items[i].quantity>1 || items[i].products.length>0))){
                naoPermitir = true;
              }
            }
            if (items.length>1){
              naoPermitir = true;
            }
          }
          $('#receive-without-commitment').attr('disabled', naoPermitir);
          carregarTotal();
        }

        function carregarTotal(){
          var total=0;
          var inputs='';
          var student = $('.btn.select').first().data('student');
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i in items){
              if(items[i].student==student){
                for (var j in items[i].products){
                  total += parseFloat(items[i].products[j].value  * items[i].products[j].quantity);
                  inputs += '<input type="hidden" name="prod_ids['+items[i].id+'][]" value="'+items[i].products[j].id+'">';
                  inputs += '<input type="hidden" name="prod_quantity['+items[i].id+'][]" value="'+items[i].products[j].quantity+'">';
                  inputs += '<input type="hidden" name="prod_value['+items[i].id+'][]" value="'+items[i].products[j].value+'">';
                }
                total += parseFloat(items[i].value  * items[i].quantity);
                inputs += '<input type="hidden" name="kit_ids[]" value="'+items[i].id+'">';
                inputs += '<input type="hidden" name="kit_quantity[]" value="'+items[i].quantity+'">';
                inputs += '<input type="hidden" name="kit_student[]" value="'+items[i].student+'">';
                inputs += '<input type="hidden" name="kit_value[]" value="'+items[i].value+'">';
              }
            }
          }
          if (sessionStorage.getItem('produtoCart')){
            var items = JSON.parse(sessionStorage.getItem('produtoCart'));
            for (var i=0; i<items.length; i++){
              if(items[i].student==student ){
                total += parseFloat(items[i].value  * items[i].quantity);
                inputs += '<input type="hidden" name="prod_ids[]" value="'+items[i].id+'">';
                inputs += '<input type="hidden" name="prod_quantity[]" value="'+items[i].quantity+'">';
                inputs += '<input type="hidden" name="prod_value[]" value="'+items[i].value+'">';
              }
            }
          }
          inputs += '<input type="hidden" name="total" value="'+total+'">';
          $('#total').html(realFormat(total.toFixed(2)));
          $('#input-hiddens').html(inputs);
        }

        function countItems(){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            return items.length;
          }
          return 0;
        }

        function adicionarKit(kit){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i in items){
              if (items[i].id==kit.id && items[i].student==kit.student){
                return;
              }
            }
            items.push(kit);
            sessionStorage.setItem('kitCart', JSON.stringify(items));
          } else {
            sessionStorage.setItem('kitCart', JSON.stringify([kit]));
          }
          carregarKits();
        }

        function atualizarKit(kit){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i in items){
              if (items[i].id==kit.id && items[i].student==kit.student){
                items[i].quantity=kit.quantity;
                sessionStorage.setItem('kitCart', JSON.stringify(items));
                carregarKits();
                return;
              }
            }
            sessionStorage.setItem('kitCart', JSON.stringify(items));
            carregarKits();
          }
        }

        function removerKit(kit){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for(var i in items){
              if (items[i].id==kit.id && items[i].student==kit.student){
                items.splice(i,1);
                sessionStorage.setItem('kitCart', JSON.stringify(items));
                carregarKits();
                return;
              }
            }
            sessionStorage.setItem('kitCart', JSON.stringify(items));
            carregarKits();
          }
        }

        function adicionarProduto(produto){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i in items){
              if (items[i].id==produto.kit && items[i].student==produto.student){
                for (var j in items[i].products) {
                  if (items[i].products[j].id==produto.id){
                    console.log(items[i].products[j]);
                    items[i].products[j].quantity++;
                    sessionStorage.setItem('kitCart', JSON.stringify(items));
                    carregarKits();
                    return ;
                  }
                }
                items[i].products.push(produto);
              }
            }
            sessionStorage.setItem('kitCart', JSON.stringify(items));
          }
          carregarKits();
        }

        function subtrairProduto(produto){
          if (sessionStorage.getItem('kitCart')){
            var items = JSON.parse(sessionStorage.getItem('kitCart'));
            for (var i in items){
              if (items[i].id==produto.kit && items[i].student==produto.student){
                for (var j in items[i].products) {
                  if (items[i].products[j].id==produto.id){
                    if (items[i].products[j].quantity>1){
                      items[i].products[j].quantity--;
                    } else {
                      items[i].products.splice(j,1);
                    }
                    sessionStorage.setItem('kitCart', JSON.stringify(items));
                    carregarKits();
                    return ;
                  }
                }

              }
            }
          }
          carregarKits();
        }

        function realFormat(numero){
          var numero = numero.split('.');
          numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');
          return numero.join(',');
        }

        $('.product-footer button.fa-plus-square').each(function(){
          $(this).on('click', function(){
            var plus = $(this).closest('.row').find('span');
            var id= plus.data('product');
            var kit = plus.data('kit');
            var value = plus.data('value');
            var student = plus.data('student');
            adicionarProduto({id: id, kit: kit, student: student, value: value, quantity: 1})
          });
        });

        $('.product-footer button.fa-minus-square').each(function(){
          $(this).on('click', function(){
            var plus = $(this).closest('.row').find('span');
            var id= plus.data('product');
            var kit = plus.data('kit');
            var value = plus.data('value');
            var student = plus.data('student');
            subtrairProduto({id: id, kit: kit, student: student, value: value, quantity: 1})
          });
        })

        $('#editar').on('click', function(){
          $('.display-name').toggle();
          $('.edit-name').toggle();
          $('#editar').toggle();
        });

        $('#cancel').on('click', function(){
          $('.display-name').toggle();
          $('.edit-name').toggle();
          $('#editar').toggle();
        });

        $('a#logout').on('click', function(e){
          e.preventDefault();
          swal({
            title: "Deseja realmente sair?",
            text: "Ao sair, todos os itens colocados no carrinho serão removidos",
            icon: "warning",
            buttons: ["Cancelar", "Confirmar"],
            dangerMode: true,
          }).then((isConfirmed)=>{
            if (isConfirmed){
              document.getElementById('logout-form').submit();
            }
          });
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('input[type="number"]').each(function(){
          $(this).on('change', function(){
            var value = $(this).val();
            var button = $(this).closest('.row').find('.btn.select');
            button.data('quantity', value);
            if (value>0){
              button.addClass('btn-primary');
            } else {
              if (button.data('selected')){
                button.addClass('btn-primary');
                button.removeClass('btn-success');
                removerKit({id: button.data('id'), student: button.data('student')});
              }
              button.html('Selecionar kit');
              button.data('selected', false);
              var prod = button.parents().closest('.row').find('.product');
              var prodAdd = button.closest('.row').find('button.fa-plus-square');
              var prodDel = button.closest('.row').find('button.fa-minus-square');
              prodAdd.each(function(){
                var prod = $(this).closest('.row').find('span');
                prod.html('0');
                $(this).attr('disabled', true);
              });
              prodDel.each(function(){
                $(this).attr('disabled', true);
              });
              prod.each(function(){
                $(this).addClass('hide');
              })
            }
            if (button.data('selected')){
              atualizarKit({id: button.data('id'), quantity: button.data('quantity'), student: button.data('student')});
            }
          })
        });

        $('.btn.select').each(function(){
          $(this).on('click', function(){
            var button = $(this);
            var prod = button.parents().closest('.row').find('.product');
            var prodAdd = button.closest('.row').find('button.fa-plus-square');
            var prodDel = button.closest('.row').find('button.fa-minus-square');
            var input = button.parents().closest('.row').find('input');
            if (!button.data('selected')){
              if (button.data('quantity')==0){
                input.val(1);
                button.data('quantity', 1);
              } else {
                input.val(button.data('quantity'));
              }
              adicionarKit({id: button.data('id'), student: button.data('student'), quantity: button.data('quantity'), value: button.data('value'), receive: button.data('receive'), products: [] })
              button.html('<i class="fa fa-check" aria-hidden="true"></i>Selecionado');
              button.removeClass('btn-primary');
              button.addClass('btn-success');
              button.data('selected', true);
              prodAdd.each(function(){
                $(this).attr('disabled', false);
              });
              prodDel.each(function(){
                $(this).attr('disabled', false);
              });
              prod.each(function(){
                $(this).removeClass('hide');
              })
            } else {
              removerKit({id: button.data('id'), student: button.data('student')});
              button.html('Selecionar kit');
              button.removeClass('btn-success');
              button.addClass('btn-primary');
              button.data('selected', false);
              prodAdd.each(function(){
                var prod = $(this).closest('.row').find('span');
                prod.html('0');
                $(this).attr('disabled', true);
              });
              prodDel.each(function(){
                $(this).attr('disabled', true);
              });
              prod.each(function(){
                $(this).addClass('hide');
              });
              input.val(0);
              button.data('quantity', 0);
            }
          })
        });

        $('#receive-without-commitment').on('click', function(e){
          if (countItems()==0){
            swal({
              icon: 'warning',
              text: 'Nenhum kit foi selecionado. Por favor selecione pelo menos um kit para permitir essa opção.'
            });
          } else {
            e.preventDefault();
            var form = $('#cart');
            var data = form.serialize();
            $.ajax({
              method: "POST",
              url: "{{ route('checkout-without')}}",
              data: data,
              success: function(response){
                console.log(response);
                if (response.success){
                  location.href="{{url('view-order')}}/"+response.msg.id;
                }
              }
            })
          }
        });

        $('#buy-now').on('click', function(e){
          if (countItems()==0){
            swal({
              icon: 'warning',
              text: 'Nenhum kit foi selecionado. Por favor selecione pelo menos um kit para permitir essa opção.'
            });
          } else {
            e.preventDefault();
            var form = $('#cart');
            var data = form.serialize();
            $.ajax({
              method: "POST",
              url: "{{ route('checkout')}}",
              data: data,
              success: function(response){
                console.log(response);
                if (response.success){
                  location.href="{{url('view-order')}}/"+response.msg.id;
                }
              },
              error: function(response){
                console.log(response);
              }
            })
          }
        });

        $('#not-receive').on('click', function(){
          swal({
            title: "Tem certeza?",
            text: "Ao clicar em \"Não desejo receber\", você deixará de receber essa maravilhosa recordação escolar.",
            icon: "warning",
            buttons: ["Não, voltar à Seleção de kits", "Sim, eu tenho certeza"],
            dangerMode: true,
          }).then((isConfirmed)=>{
            if (isConfirmed){
              $('#notreceive').submit();
            }
          });
        });

      });

    })(jQuery);

  </script>
@stop
