<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

  <!-- Styles -->
  <link href="{{ asset('site/css/app.css') }}" rel="stylesheet">

  <!--Style CSS -->
  <link rel="stylesheet" href="{{ asset('site/css/layout.css') }}">
  @yield('styles')
</head>
<body>
  <div id="app">
    <div class="background">
      <div class="background-left"></div>
      <div class="background-right"></div>
    </div>
    <div class="container conteudo">
      <!-- Logo -->
      <div class="row">
        <div class="col text-center"><img src="{{ asset('site/img/logo.png') }}" alt="Abdesign" ></div>
      </div>
      <!-- end logo -->
      @yield('content')
    </div>
  </div>
  <!-- App Scripts -->
  <script src="{{ asset('site/js/app.js') }}"></script>
  <!-- Page Scripts -->
  @yield('scripts')
</body>

</html>
