@extends('site.layouts.app')

@section('content')

  @if($settings->site_enabled)
  <!-- content -->
    <div class="row justify-content-md-center">
      <div class="col-lg-9 col-sm-11 text-center">
        {!! $text->description !!}
      </div>
    </div>

    <form id="login">
      <div class="form-row has-feedback justify-content-center">
        <div class="col-xs-2">
          <input type="text" class="form-control" id="key" placeholder="Chave de Acesso">
          <span class="text-danger">
            <p id="key-error"></p>
          </span>
        </div>
        <div class="col-xs-3">
          <button type="submit" class="btn btn-danger" id="check-key">Entrar</button>
        </div>
      </div>
    </form>
    <div class="row">
      <div class="col-12 text-muted text-center">
        <p>Esqueceu sua chave de acesso? <a href="#" data-toggle="modal" data-target="#esqueci">Clique aqui!</a></p>
      </div>
    </div>
  @else
  <!-- content -->
  <div class="row justify-content-md-center">
    <div class="col-lg-9 col-sm-11 text-center">
      {!! $textInactive->description !!}
    </div>
  </div>
  @endif
  </div>
  <!-- end content -->

  <!-- modal -->
  <div class="modal fade" id="esqueci" tabindex="-1" role="dialog" aria-labelledby="esqueciLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="esqueciLabel">Chave de Acesso</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="success-msg" class="hide">
            <div class="alert alert-info alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Sucesso!</strong> Logo enviaremos por email seus dados para acesso ao site!!
            </div>
          </div>
          <div id="error-msg" class="hide">
            <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <strong>Erro!</strong> <span id="error"></span>
            </div>
          </div>
          <div class="col-12">
            <p>A chave de acesso está impressa no comunicado que você recebeu através do
                material escolar do seu filho. Caso não tenha mais esse documento solicite uma nova chave através do formulário abaixo:
            </p>

            <form id="register">
                  @csrf
              <div class="form-group has-feedback">
                  <label for="nome">Nome do Responsável</label>
                  <input type="text" class="form-control" name="resp_name" id="resp_name" value="{{ old('resp_name') }}" required>
                  <span class="text-danger">
                      <strong id="resp_name-error"></strong>
                  </span>
              </div>
              <div class="form-group has-feedback">
                  <label for="email">E-mail do Responsável</label>
                  <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" required>
                  <span class="text-danger">
                      <strong id="email-error"></strong>
                  </span>
              </div>
              <div class="form-group has-feedback">
                  <label for="Telefone">Telefone</label>
                  <input type="text" id="phone" name="phone" class="form-control" value="{{ old('phone') }}" required>
                  <span class="text-danger">
                      <strong id="phone-error"></strong>
                  </span>
              </div>
              <div class="form-group has-feedback">
                  <label for="Nome do Aluno">Nome do Aluno</label>
                  <input type="text" class="form-control" name="name" id="name" value="{{ old('name')}}" required>
                  <span class="text-danger">
                      <strong id="name-error"></strong>
                  </span>
              </div>
              <button type="submit" class="btn btn-danger" id="submitRegister">Solicitar nova chave</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  <!-- end modal -->
@stop

@section('scripts')
  <script src="{{ asset('site/js/jquery.mask.js') }}"></script>
	<script>
    (function($){
      'use strict';
      $(function(){
        sessionStorage.clear();
        $("#phone").mask("(99) 99999-9999");

        $("#login").on('submit', function(e){
          e.preventDefault();
          $('#key-not-found').html('');
          var key = $('#key').val();
          $('#check-key').html('<i class="fa fa-spinner fa-spin"></i> Entrar');
          $.ajax({
            type: "get",
            url: "{{ route('checkKey') }}",
            data: {key: key},
            success: function(response){
              $('.invalid-feedback').hide();
              location.href="{{ url('confirm-student')}}/" + response.message.id;
            },
            error: function(data){
              $('#check-key').html('Entrar');
              $('#key-error').html('Chave de acesso não encontrada!');
            }
          })
        });

        $('body').on('click', '#submitRegister', function(e){
          e.preventDefault();
          var registerForm = $('#register');
          var formData = registerForm.serialize();
          $('#resp_name-error').html('');
          $('#email-error').html('');
          $('#phone-error').html('');
          $('#name-error').html('');
          $('#error').html('');
          $.ajax({
            url: "{{route('recovery.key')}}",
            type: "POST",
            data: formData,
            success: function(data){
              console.log(data);
              if (data.errors){
                $('#error-msg').removeClass('hide');
                setInterval(() => {
                  $('#error-msg').addClass('hide');
                }, 3000);
                $('#error').html(data.errors.msg);
                if (data.errors[0].resp_name){
                  $('#resp_name-error').html(data.errors[0].resp_name[0]);
                }
                if (data.errors[0].email){
                  $('#email-error').html(data.errors[0].email[0]);
                }
                if (data.errors[0].phone){
                  $('#phone-error').html(data.errors[0].phone[0]);
                }
                if (data.errors[0].name){
                  $('#name-error').html(data.errors[0].name[0]);
                }
              }
              if (data.success){
                $('#success-msg').removeClass('hide');
                setInterval(() => {
                  $('#esqueci').modal('hide');
                  $('#success-msg').addClass('hide');
                }, 3000);
              }
            }

          });
        })
      })
    })(jQuery);
		</script>
@stop

