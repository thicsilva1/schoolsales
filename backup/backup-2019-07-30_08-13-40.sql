-- mysqldump-php https://github.com/ifsnop/mysqldump-php
--
-- Host: 127.0.0.1	Database: abdfotosabdesign
-- ------------------------------------------------------
-- Server version 	5.6.41
-- Date: Tue, 30 Jul 2019 08:13:40 -0300

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `coupons`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Porcentagem','Valor') COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(8,2) NOT NULL,
  `max_amount` int(11) NOT NULL DEFAULT '1',
  `due_date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `coupons` VALUES (4,'10purssa','Porcentagem',10.00,1,'2019-05-17',1,'2019-05-16 19:44:32','2019-05-16 19:44:32');
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `coupons` with 1 row(s)
--

--
-- Table structure for table `database_logs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `database_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `database_logs`
--

LOCK TABLES `database_logs` WRITE;
/*!40000 ALTER TABLE `database_logs` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `database_logs` VALUES (1,'Backup','Completo',NULL,'2019-02-04 15:47:50','2019-02-04 15:47:50'),(2,'Backup','Completo',NULL,'2019-07-26 11:49:09','2019-07-26 11:49:09'),(3,'Backup','Completo',NULL,'2019-07-26 14:14:30','2019-07-26 14:14:30');
/*!40000 ALTER TABLE `database_logs` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `database_logs` with 3 row(s)
--

--
-- Table structure for table `kit_orders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kit_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `kit_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `value` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kit_orders`
--

LOCK TABLES `kit_orders` WRITE;
/*!40000 ALTER TABLE `kit_orders` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `kit_orders` VALUES (88,8,1,244,5,140.00,'2019-07-26 12:51:34','2019-07-30 00:58:46'),(2,2,1,1,1,140.00,'2019-02-12 15:12:37','2019-02-12 15:12:37'),(3,3,1,1,1,0.00,'2019-02-12 15:14:41','2019-02-12 15:14:41'),(4,4,1,1,1,140.00,'2019-02-12 15:56:41','2019-02-12 15:56:41'),(5,5,1,1,5,0.00,'2019-02-12 15:59:04','2019-07-26 01:17:05'),(15,10,6,1,18,0.00,'2019-02-12 17:11:36','2019-02-12 17:13:12'),(89,9,1,56,1,140.00,'2019-07-29 08:46:29','2019-07-29 08:46:29'),(12,7,4,1,2,140.00,'2019-02-12 16:48:45','2019-07-26 01:52:34'),(11,7,1,2,1,140.00,'2019-02-12 16:47:10','2019-02-12 16:47:10'),(16,11,1,1,1,0.00,'2019-02-12 17:13:43','2019-02-12 17:13:43'),(19,13,4,3,9,138.60,'2019-02-12 17:29:06','2019-02-12 17:31:51'),(18,12,5,1,1,56.00,'2019-02-12 17:27:39','2019-02-12 17:27:39'),(20,14,4,3,12,138.60,'2019-02-12 17:32:34','2019-02-12 17:39:21'),(21,15,5,3,1,56.00,'2019-02-12 17:33:01','2019-02-12 17:33:01'),(22,15,2,4,1,122.75,'2019-02-12 17:37:00','2019-02-12 17:37:00'),(23,14,1,3,1,126.00,'2019-02-12 17:37:17','2019-02-12 17:37:17'),(26,16,6,3,22,0.00,'2019-02-12 17:46:31','2019-02-12 17:50:37'),(27,17,5,2,1,56.00,'2019-02-12 17:49:45','2019-02-12 17:49:45'),(63,47,6,30,1,0.00,'2019-02-28 16:02:55','2019-02-28 16:02:55'),(29,19,6,3,5,0.00,'2019-02-12 17:51:55','2019-02-12 17:53:21'),(30,19,1,3,1,126.00,'2019-02-12 17:52:38','2019-02-12 17:52:38'),(31,20,4,3,13,138.60,'2019-02-12 17:55:04','2019-02-12 17:56:55'),(32,20,6,3,1,0.00,'2019-02-12 17:55:16','2019-02-12 17:55:16'),(33,21,4,3,6,138.60,'2019-02-12 17:57:27','2019-02-12 17:57:47'),(34,22,4,1,4,140.00,'2019-02-12 18:00:21','2019-02-12 18:00:40'),(35,23,4,2,1,140.00,'2019-02-12 18:04:27','2019-02-12 18:04:27'),(36,23,6,2,1,0.00,'2019-02-12 18:04:56','2019-02-12 18:04:56'),(37,24,1,1,1,140.00,'2019-02-12 18:07:39','2019-02-12 18:07:39'),(38,25,4,1,5,140.00,'2019-02-12 18:08:25','2019-02-12 18:11:05'),(39,26,4,3,6,112.00,'2019-02-12 18:25:18','2019-02-12 18:25:39'),(40,27,6,1,3,0.00,'2019-02-12 18:34:19','2019-02-12 18:36:23'),(64,48,1,32,2,0.00,'2019-02-28 16:07:03','2019-02-28 16:07:30'),(42,29,1,3,2,0.00,'2019-02-12 18:36:44','2019-02-12 18:37:02'),(43,30,4,3,1,112.00,'2019-02-12 18:38:41','2019-02-12 18:38:41'),(44,31,1,2,1,140.00,'2019-02-12 18:40:29','2019-02-12 18:40:29'),(45,32,6,1,1,0.00,'2019-02-12 18:42:00','2019-02-12 18:42:00'),(48,34,1,1,1,140.00,'2019-02-13 11:08:22','2019-02-13 11:08:22'),(47,33,4,3,1,112.00,'2019-02-12 18:44:16','2019-02-12 18:44:16'),(49,35,1,3,1,112.00,'2019-02-13 11:09:00','2019-02-13 11:09:00'),(50,36,4,2,1,140.00,'2019-02-13 13:32:00','2019-02-13 13:32:00'),(51,37,1,1,1,0.00,'2019-02-13 13:35:55','2019-02-13 13:35:55'),(52,38,4,1,3,140.00,'2019-02-14 13:17:45','2019-02-14 13:38:35'),(53,38,6,1,1,0.00,'2019-02-14 13:17:45','2019-02-14 13:17:45'),(54,39,4,3,1,112.00,'2019-02-14 13:39:49','2019-02-14 13:39:49'),(55,40,4,2,1,140.00,'2019-02-14 13:49:38','2019-02-14 13:49:38'),(77,59,4,1,1,140.00,'2019-03-01 21:56:15','2019-03-01 21:56:15'),(78,60,4,1,1,140.00,'2019-04-19 14:04:36','2019-04-19 14:04:36'),(62,46,1,30,1,0.00,'2019-02-28 16:01:53','2019-02-28 16:01:53'),(65,49,1,32,5,0.00,'2019-02-28 16:08:05','2019-02-28 16:13:13'),(66,49,4,32,1,140.00,'2019-02-28 16:13:13','2019-02-28 16:13:13'),(67,50,6,34,1,0.00,'2019-02-28 16:13:48','2019-02-28 16:13:48'),(68,51,1,34,2,0.00,'2019-02-28 16:17:41','2019-02-28 16:17:54'),(79,61,4,1,1,140.00,'2019-04-19 14:04:49','2019-04-19 14:04:49'),(81,2,1,1,1,0.00,'2019-05-16 19:51:42','2019-05-16 19:51:42'),(82,3,6,3,1,0.00,'2019-05-20 11:47:08','2019-05-20 11:47:08'),(83,4,6,3,1,0.00,'2019-05-20 11:47:12','2019-05-20 11:47:12'),(84,4,4,3,13,140.00,'2019-05-20 11:47:31','2019-05-20 11:48:17'),(85,4,1,3,1,140.00,'2019-05-20 11:48:23','2019-05-20 11:48:23'),(86,6,1,1,4,112.00,'2019-07-26 01:26:55','2019-07-26 01:26:55'),(87,7,1,1,2,112.00,'2019-07-26 01:30:43','2019-07-26 01:49:53');
/*!40000 ALTER TABLE `kit_orders` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `kit_orders` with 60 row(s)
--

--
-- Table structure for table `kit_product`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kit_product` (
  `kit_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`kit_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kit_product`
--

LOCK TABLES `kit_product` WRITE;
/*!40000 ALTER TABLE `kit_product` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `kit_product` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `kit_product` with 0 row(s)
--

--
-- Table structure for table `kits`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `old_price` decimal(8,2) NOT NULL,
  `discount` decimal(5,2) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_order` int(11) NOT NULL DEFAULT '0',
  `kindergarten` tinyint(1) NOT NULL DEFAULT '0',
  `elementary_school` tinyint(1) NOT NULL DEFAULT '0',
  `high_school` tinyint(1) NOT NULL DEFAULT '0',
  `all_type` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `receive_without_commitment` enum('Sim','Não','Site') COLLATE utf8mb4_unicode_ci NOT NULL,
  `allow_extra_products` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kits`
--

LOCK TABLES `kits` WRITE;
/*!40000 ALTER TABLE `kits` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `kits` VALUES (1,'KIT Completo Infantil','Livreto INFANTIL (com foto individual e turma), Kit com 9 fotos 3x4cm, Cartela de Adesivos, 2 Fotos Coloridas 13x18cm, Tag de Mochila Infantil.',140.00,152.00,20.00,'a7d271e5bad3516249c6ac6a8f873e40.jpeg',1,1,0,0,0,1,'Sim',0,'2019-02-08 16:40:00','2019-02-12 18:07:55'),(2,'Kit Completo Fundamental','Kit com 9 fotos 3x4cm, Marcador de Livros (4x20cm), 2 Fotos Coloridas 13x18cm, Livreto FUNDAMENTAL (com foto da turma oficial e divertida)',123.00,129.00,0.20,'c44a0adc3e5afba385639357c43385f4.jpeg',2,0,1,0,0,1,'Site',1,'2019-02-08 16:42:01','2019-02-08 16:42:01'),(3,'Kit Completo Ensino Médio','Kit com 9 fotos 3x4cm, 2 Fotos Coloridas 13x18cm, Livreto MÉDIO (com foto da turma)',110.00,116.00,0.20,'56780391ef0fb528e90426b3868c8fda.jpeg',3,0,0,1,0,1,'Site',1,'2019-02-08 16:43:30','2019-02-08 16:43:30'),(4,'Kit Completo Infantil + Itens Extras','Opção de comprar o kit completo + Itens adicionais avulsos.\r\n\r\nKit completo composto de: Livreto INFANTIL (com foto individual e turma), Kit com 9 fotos 3x4cm, Cartela de Adesivos, 2 Fotos Coloridas 13x18cm, Tag de Mochila Infantil.',140.00,152.00,20.00,'74e2970263416cd9180ed89625375c2c.jpeg',2,1,0,0,0,1,'Site',1,'2019-02-12 15:36:53','2019-02-12 18:07:04'),(5,'Livreto + Extras','Opção para comprar apenas o Livreto com foto de turma, ou o livreto + itens avulsos.',56.00,56.00,0.00,'e384e14603a8c78d37d388f5f1423d1c.jpeg',3,1,0,0,0,0,'Site',1,'2019-02-12 15:39:05','2019-02-12 18:10:51'),(6,'Itens Extras','Com essa opção, você pode escolher itens individuais para comprar.',0.00,0.00,0.00,'7abb0536a37fc051e2156a532f8199e7.jpeg',6,1,0,0,0,1,'Site',1,'2019-02-12 15:42:02','2019-02-12 15:42:02');
/*!40000 ALTER TABLE `kits` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `kits` with 6 row(s)
--

--
-- Table structure for table `logs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `relative_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `logs` VALUES (1,1,1,'teste foi adicionado como responsável ao aluno Alicia Carvalho Corrêia',NULL,'2019-02-12 11:19:15','2019-02-12 11:19:15'),(2,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 11:19:15','2019-02-12 11:19:15'),(3,1,1,'Gerado novo pedido de compra: #1',NULL,'2019-02-12 11:20:27','2019-02-12 11:20:27'),(4,1,2,'teste foi adicionado como responsável ao aluno Beatriz Sepulcre Gil',NULL,'2019-02-12 11:31:44','2019-02-12 11:31:44'),(5,1,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-12 11:31:44','2019-02-12 11:31:44'),(6,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 13:37:17','2019-02-12 13:37:17'),(7,2,1,'testecris foi adicionado como responsável ao aluno Alicia Carvalho Corrêia',NULL,'2019-02-12 15:09:55','2019-02-12 15:09:55'),(8,2,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 15:09:55','2019-02-12 15:09:55'),(9,2,1,'Gerado novo pedido de compra: #2',NULL,'2019-02-12 15:12:37','2019-02-12 15:12:37'),(10,3,1,'testecris2 foi adicionado como responsável ao aluno Alicia Carvalho Corrêia',NULL,'2019-02-12 15:14:31','2019-02-12 15:14:31'),(11,3,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 15:14:31','2019-02-12 15:14:31'),(12,3,1,'Gerado novo pedido SEM COMPROMISSO: #3',NULL,'2019-02-12 15:14:41','2019-02-12 15:14:41'),(13,3,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 15:16:50','2019-02-12 15:16:50'),(14,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 15:47:15','2019-02-12 15:47:15'),(15,1,1,'Gerado novo pedido de compra: #4',NULL,'2019-02-12 15:56:41','2019-02-12 15:56:41'),(16,1,1,'Gerado novo pedido SEM COMPROMISSO: #5',NULL,'2019-02-12 15:59:04','2019-02-12 15:59:04'),(17,1,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-12 16:12:19','2019-02-12 16:12:19'),(18,4,2,'testecri foi adicionado como responsável ao aluno Beatriz Sepulcre Gil',NULL,'2019-02-12 16:31:27','2019-02-12 16:31:27'),(19,4,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-12 16:31:27','2019-02-12 16:31:27'),(20,4,2,'Gerado novo pedido de compra: #6',NULL,'2019-02-12 16:38:31','2019-02-12 16:38:31'),(21,4,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-12 16:45:35','2019-02-12 16:45:35'),(22,4,2,'Gerado novo pedido de compra: #7',NULL,'2019-02-12 16:47:10','2019-02-12 16:47:10'),(23,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 16:55:23','2019-02-12 16:55:23'),(24,1,1,'Gerado novo pedido de compra: #8',NULL,'2019-02-12 16:55:34','2019-02-12 16:55:34'),(25,1,2,'Gerado novo pedido SEM COMPROMISSO: #9',NULL,'2019-02-12 17:02:09','2019-02-12 17:02:09'),(26,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 17:10:54','2019-02-12 17:10:54'),(27,1,1,'Gerado novo pedido de compra: #10',NULL,'2019-02-12 17:11:36','2019-02-12 17:11:36'),(28,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 17:13:31','2019-02-12 17:13:31'),(29,5,1,'teste1 foi adicionado como responsável ao aluno Alicia Carvalho Corrêia',NULL,'2019-02-12 17:13:37','2019-02-12 17:13:37'),(30,5,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 17:13:37','2019-02-12 17:13:37'),(31,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 17:13:42','2019-02-12 17:13:42'),(32,5,1,'Gerado novo pedido SEM COMPROMISSO: #11',NULL,'2019-02-12 17:13:43','2019-02-12 17:13:43'),(33,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 17:14:47','2019-02-12 17:14:47'),(34,1,1,'Gerado novo pedido de compra: #12',NULL,'2019-02-12 17:14:59','2019-02-12 17:14:59'),(35,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 17:18:37','2019-02-12 17:18:37'),(36,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 17:19:49','2019-02-12 17:19:49'),(37,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 17:20:51','2019-02-12 17:20:51'),(38,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 17:21:37','2019-02-12 17:21:37'),(39,6,3,'11111111 foi adicionado como responsável ao aluno Benício Perazza Furtado',NULL,'2019-02-12 17:24:11','2019-02-12 17:24:11'),(40,6,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 17:24:11','2019-02-12 17:24:11'),(41,7,3,'ccccccccc foi adicionado como responsável ao aluno Benício Perazza Furtado',NULL,'2019-02-12 17:28:37','2019-02-12 17:28:37'),(42,7,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 17:28:37','2019-02-12 17:28:37'),(43,7,3,'Gerado novo pedido de compra: #13',NULL,'2019-02-12 17:29:06','2019-02-12 17:29:06'),(44,8,3,'ccccccccc foi adicionado como responsável ao aluno Benício Perazza Furtado',NULL,'2019-02-12 17:32:22','2019-02-12 17:32:22'),(45,8,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 17:32:22','2019-02-12 17:32:22'),(46,8,3,'Gerado novo pedido de compra: #14',NULL,'2019-02-12 17:32:34','2019-02-12 17:32:34'),(47,6,3,'Gerado novo pedido de compra: #15',NULL,'2019-02-12 17:33:01','2019-02-12 17:33:01'),(48,9,3,'cris5 foi adicionado como responsável ao aluno Benício Perazza Furtado',NULL,'2019-02-12 17:40:36','2019-02-12 17:40:36'),(49,9,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 17:40:36','2019-02-12 17:40:36'),(50,10,9,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno Matteo Catelli Neis',NULL,'2019-02-12 17:40:39','2019-02-12 17:40:39'),(51,10,9,'Acesso com a chave: acesso3sbm',NULL,'2019-02-12 17:40:39','2019-02-12 17:40:39'),(52,9,3,'Gerado novo pedido de compra: #16',NULL,'2019-02-12 17:41:04','2019-02-12 17:41:04'),(53,1,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-12 17:48:54','2019-02-12 17:48:54'),(54,1,2,'Gerado novo pedido de compra: #17',NULL,'2019-02-12 17:49:45','2019-02-12 17:49:45'),(55,11,3,'66666 foi adicionado como responsável ao aluno Benício Perazza Furtado',NULL,'2019-02-12 17:51:08','2019-02-12 17:51:08'),(56,11,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 17:51:08','2019-02-12 17:51:08'),(57,10,9,'Gerado novo pedido de compra: #18',NULL,'2019-02-12 17:51:32','2019-02-12 17:51:32'),(58,11,3,'Gerado novo pedido de compra: #19',NULL,'2019-02-12 17:51:55','2019-02-12 17:51:55'),(59,12,3,'7777777777777777777 foi adicionado como responsável ao aluno Benício Perazza Furtado',NULL,'2019-02-12 17:54:55','2019-02-12 17:54:55'),(60,12,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 17:54:55','2019-02-12 17:54:55'),(61,12,3,'Gerado novo pedido de compra: #20',NULL,'2019-02-12 17:55:04','2019-02-12 17:55:04'),(62,13,3,'8888 foi adicionado como responsável ao aluno Benício Perazza Furtado',NULL,'2019-02-12 17:57:19','2019-02-12 17:57:19'),(63,13,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 17:57:19','2019-02-12 17:57:19'),(64,13,3,'Gerado novo pedido de compra: #21',NULL,'2019-02-12 17:57:27','2019-02-12 17:57:27'),(65,14,1,'testecris7 foi adicionado como responsável ao aluno Alícia Carvalho Corrêia Cruz',NULL,'2019-02-12 18:00:07','2019-02-12 18:00:07'),(66,14,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 18:00:07','2019-02-12 18:00:07'),(67,14,1,'Gerado novo pedido de compra: #22',NULL,'2019-02-12 18:00:21','2019-02-12 18:00:21'),(68,15,2,'testecris29 foi adicionado como responsável ao aluno Beatriz Sepulcre Gil teste',NULL,'2019-02-12 18:03:35','2019-02-12 18:03:35'),(69,15,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-12 18:03:35','2019-02-12 18:03:35'),(70,15,2,'Gerado novo pedido de compra: #23',NULL,'2019-02-12 18:04:27','2019-02-12 18:04:27'),(71,10,3,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno Benício Perazza Furtado',NULL,'2019-02-12 18:05:11','2019-02-12 18:05:11'),(72,10,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 18:05:11','2019-02-12 18:05:11'),(73,16,1,'testecri3 foi adicionado como responsável ao aluno Alícia Carvalho Corrêia Cruz',NULL,'2019-02-12 18:06:25','2019-02-12 18:06:25'),(74,16,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 18:06:25','2019-02-12 18:06:25'),(75,16,1,'Gerado novo pedido de compra: #24',NULL,'2019-02-12 18:07:39','2019-02-12 18:07:39'),(76,17,1,'testecri4 foi adicionado como responsável ao aluno Alícia Carvalho Corrêia Cruz',NULL,'2019-02-12 18:08:16','2019-02-12 18:08:16'),(77,17,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 18:08:16','2019-02-12 18:08:16'),(78,17,1,'Gerado novo pedido de compra: #25',NULL,'2019-02-12 18:08:25','2019-02-12 18:08:25'),(79,19,2,'testecri5 foi adicionado como responsável ao aluno Beatriz Sepulcre Gil teste',NULL,'2019-02-12 18:11:33','2019-02-12 18:11:33'),(80,18,2,'testecri5 foi adicionado como responsável ao aluno Beatriz Sepulcre Gil teste',NULL,'2019-02-12 18:11:33','2019-02-12 18:11:33'),(81,19,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-12 18:11:33','2019-02-12 18:11:33'),(82,18,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-12 18:11:33','2019-02-12 18:11:33'),(83,6,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 18:17:37','2019-02-12 18:17:37'),(84,6,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 18:23:31','2019-02-12 18:23:31'),(85,6,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 18:24:15','2019-02-12 18:24:15'),(86,6,3,'Gerado novo pedido de compra: #26',NULL,'2019-02-12 18:25:18','2019-02-12 18:25:18'),(87,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 18:27:26','2019-02-12 18:27:26'),(88,1,1,'Gerado novo pedido de compra: #27',NULL,'2019-02-12 18:34:19','2019-02-12 18:34:19'),(89,10,3,'Gerado novo pedido de compra: #28',NULL,'2019-02-12 18:36:24','2019-02-12 18:36:24'),(90,6,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 18:36:30','2019-02-12 18:36:30'),(91,6,3,'Gerado novo pedido SEM COMPROMISSO: #29',NULL,'2019-02-12 18:36:44','2019-02-12 18:36:44'),(92,6,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 18:37:15','2019-02-12 18:37:15'),(93,6,3,'Gerado novo pedido de compra: #30',NULL,'2019-02-12 18:38:41','2019-02-12 18:38:41'),(94,1,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-12 18:39:59','2019-02-12 18:39:59'),(95,1,2,'Gerado novo pedido de compra: #31',NULL,'2019-02-12 18:40:29','2019-02-12 18:40:29'),(96,6,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 18:41:21','2019-02-12 18:41:21'),(97,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-12 18:41:49','2019-02-12 18:41:49'),(98,1,1,'Gerado novo pedido de compra: #32',NULL,'2019-02-12 18:42:00','2019-02-12 18:42:00'),(99,6,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-12 18:43:39','2019-02-12 18:43:39'),(100,6,3,'Gerado novo pedido SEM COMPROMISSO: #33',NULL,'2019-02-12 18:43:46','2019-02-12 18:43:46'),(101,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-13 11:07:40','2019-02-13 11:07:40'),(102,1,1,'Gerado novo pedido de compra: #34',NULL,'2019-02-13 11:08:22','2019-02-13 11:08:22'),(103,6,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-13 11:08:51','2019-02-13 11:08:51'),(104,6,3,'Gerado novo pedido de compra: #35',NULL,'2019-02-13 11:09:00','2019-02-13 11:09:00'),(105,1,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-13 13:31:46','2019-02-13 13:31:46'),(106,1,2,'Gerado novo pedido de compra: #36',NULL,'2019-02-13 13:32:00','2019-02-13 13:32:00'),(107,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-13 13:35:48','2019-02-13 13:35:48'),(108,1,1,'Gerado novo pedido SEM COMPROMISSO: #37',NULL,'2019-02-13 13:35:55','2019-02-13 13:35:55'),(109,10,19,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno Laura de Carvalho Maffei',NULL,'2019-02-13 15:58:42','2019-02-13 15:58:42'),(110,10,19,'Acesso com a chave: acesso6',NULL,'2019-02-13 15:58:42','2019-02-13 15:58:42'),(111,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-13 16:52:56','2019-02-13 16:52:56'),(112,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-14 12:51:17','2019-02-14 12:51:17'),(113,1,1,'Gerado novo pedido de compra: #38',NULL,'2019-02-14 13:17:45','2019-02-14 13:17:45'),(114,6,3,'Acesso com a chave: acessog5bm',NULL,'2019-02-14 13:39:04','2019-02-14 13:39:04'),(115,6,3,'Gerado novo pedido de compra: #39',NULL,'2019-02-14 13:39:49','2019-02-14 13:39:49'),(116,1,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-14 13:49:27','2019-02-14 13:49:27'),(117,1,2,'Gerado novo pedido de compra: #40',NULL,'2019-02-14 13:49:38','2019-02-14 13:49:38'),(118,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-20 13:25:30','2019-02-20 13:25:30'),(119,1,1,'Gerado novo pedido de compra: #41',NULL,'2019-02-20 13:28:56','2019-02-20 13:28:56'),(120,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-20 14:09:05','2019-02-20 14:09:05'),(121,1,1,'Gerado novo pedido de compra: #42',NULL,'2019-02-20 14:10:19','2019-02-20 14:10:19'),(122,1,2,'Acesso com a chave: acessog3bm',NULL,'2019-02-20 14:11:52','2019-02-20 14:11:52'),(123,1,1,'Acesso com a chave: acessob1',NULL,'2019-02-25 11:20:08','2019-02-25 11:20:08'),(124,10,8,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno Claudia Jemima Malara de Andrade',NULL,'2019-02-28 15:08:15','2019-02-28 15:08:15'),(125,10,8,'Acesso com a chave: acesso1sbm',NULL,'2019-02-28 15:08:15','2019-02-28 15:08:15'),(126,10,4,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno Augusto Fiuza Funicello de Souza Ferreira',NULL,'2019-02-28 15:13:20','2019-02-28 15:13:20'),(127,10,4,'Acesso com a chave: acesso9am',NULL,'2019-02-28 15:13:20','2019-02-28 15:13:20'),(128,10,4,'Acesso com a chave: acesso9am',NULL,'2019-02-28 15:45:50','2019-02-28 15:45:50'),(129,10,4,'Acesso com a chave: acesso9am',NULL,'2019-02-28 15:46:20','2019-02-28 15:46:20'),(130,10,4,'Gerado novo pedido de compra: #43',NULL,'2019-02-28 15:46:30','2019-02-28 15:46:30'),(131,10,4,'Acesso com a chave: acesso9am',NULL,'2019-02-28 15:56:15','2019-02-28 15:56:15'),(132,10,4,'Acesso com a chave: acesso9am',NULL,'2019-02-28 15:56:34','2019-02-28 15:56:34'),(133,10,4,'Gerado novo pedido de compra: #44',NULL,'2019-02-28 15:56:43','2019-02-28 15:56:43'),(134,10,4,'Acesso com a chave: acesso9am',NULL,'2019-02-28 15:57:35','2019-02-28 15:57:35'),(135,10,4,'Acesso com a chave: acesso9am',NULL,'2019-02-28 15:59:14','2019-02-28 15:59:14'),(136,10,4,'Gerado novo pedido de compra: #45',NULL,'2019-02-28 15:59:27','2019-02-28 15:59:27'),(137,10,30,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno 4',NULL,'2019-02-28 16:01:05','2019-02-28 16:01:05'),(138,10,30,'Acesso com a chave: acesso19',NULL,'2019-02-28 16:01:05','2019-02-28 16:01:05'),(139,20,30,'Carolina Vilhena Bittencourt foi adicionado como responsável ao aluno 4',NULL,'2019-02-28 16:01:36','2019-02-28 16:01:36'),(140,20,30,'Acesso com a chave: acesso19',NULL,'2019-02-28 16:01:36','2019-02-28 16:01:36'),(141,20,30,'Gerado novo pedido SEM COMPROMISSO: #46',NULL,'2019-02-28 16:01:53','2019-02-28 16:01:53'),(142,10,30,'Acesso com a chave: acesso19',NULL,'2019-02-28 16:02:14','2019-02-28 16:02:14'),(143,20,30,'Acesso com a chave: acesso19',NULL,'2019-02-28 16:02:44','2019-02-28 16:02:44'),(144,20,30,'Gerado novo pedido de compra: #47',NULL,'2019-02-28 16:02:55','2019-02-28 16:02:55'),(145,10,38,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno 12',NULL,'2019-02-28 16:05:53','2019-02-28 16:05:53'),(146,10,38,'Acesso com a chave: acesso27',NULL,'2019-02-28 16:05:53','2019-02-28 16:05:53'),(147,10,38,'Acesso com a chave: acesso27',NULL,'2019-02-28 16:06:07','2019-02-28 16:06:07'),(148,10,31,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno 5',NULL,'2019-02-28 16:06:26','2019-02-28 16:06:26'),(149,10,31,'Acesso com a chave: acesso20',NULL,'2019-02-28 16:06:26','2019-02-28 16:06:26'),(150,10,32,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno 6',NULL,'2019-02-28 16:06:51','2019-02-28 16:06:51'),(151,10,32,'Acesso com a chave: acesso21',NULL,'2019-02-28 16:06:51','2019-02-28 16:06:51'),(152,10,32,'Gerado novo pedido SEM COMPROMISSO: #48',NULL,'2019-02-28 16:07:03','2019-02-28 16:07:03'),(153,10,32,'Acesso com a chave: acesso21',NULL,'2019-02-28 16:07:59','2019-02-28 16:07:59'),(154,10,32,'Gerado novo pedido SEM COMPROMISSO: #49',NULL,'2019-02-28 16:08:05','2019-02-28 16:08:05'),(155,10,34,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno 8',NULL,'2019-02-28 16:13:36','2019-02-28 16:13:36'),(156,10,34,'Acesso com a chave: acesso23',NULL,'2019-02-28 16:13:36','2019-02-28 16:13:36'),(157,10,34,'Gerado novo pedido de compra: #50',NULL,'2019-02-28 16:13:48','2019-02-28 16:13:48'),(158,10,34,'Acesso com a chave: acesso23',NULL,'2019-02-28 16:17:33','2019-02-28 16:17:33'),(159,10,34,'Gerado novo pedido SEM COMPROMISSO: #51',NULL,'2019-02-28 16:17:41','2019-02-28 16:17:41'),(160,10,35,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno 9',NULL,'2019-02-28 16:19:20','2019-02-28 16:19:20'),(161,10,35,'Acesso com a chave: acesso24',NULL,'2019-02-28 16:19:20','2019-02-28 16:19:20'),(162,10,35,'Gerado novo pedido SEM COMPROMISSO: #52',NULL,'2019-02-28 16:19:49','2019-02-28 16:19:49'),(163,10,35,'Gerado novo pedido de compra: #53',NULL,'2019-02-28 16:21:09','2019-02-28 16:21:09'),(164,10,4,'Acesso com a chave: acesso9am',NULL,'2019-03-01 21:45:04','2019-03-01 21:45:04'),(165,1,1,'Acesso com a chave: acessob1',NULL,'2019-03-01 21:45:26','2019-03-01 21:45:26'),(166,1,1,'Acesso com a chave: acessob1',NULL,'2019-03-01 21:45:40','2019-03-01 21:45:40'),(167,1,1,'Gerado novo pedido SEM COMPROMISSO: #54',NULL,'2019-03-01 21:52:34','2019-03-01 21:52:34'),(168,1,1,'Gerado novo pedido SEM COMPROMISSO: #55',NULL,'2019-03-01 21:53:00','2019-03-01 21:53:00'),(169,1,1,'Gerado novo pedido de compra: #56',NULL,'2019-03-01 21:54:05','2019-03-01 21:54:05'),(170,1,1,'Gerado novo pedido SEM COMPROMISSO: #57',NULL,'2019-03-01 21:54:28','2019-03-01 21:54:28'),(171,1,1,'Gerado novo pedido de compra: #58',NULL,'2019-03-01 21:55:59','2019-03-01 21:55:59'),(172,1,1,'Gerado novo pedido de compra: #59',NULL,'2019-03-01 21:56:15','2019-03-01 21:56:15'),(173,10,4,'Acesso com a chave: acesso9am',NULL,'2019-03-11 17:58:32','2019-03-11 17:58:32'),(174,1,1,'Acesso com a chave: acessob1',NULL,'2019-04-19 14:03:49','2019-04-19 14:03:49'),(175,1,1,'Gerado novo pedido de compra: #60',NULL,'2019-04-19 14:04:36','2019-04-19 14:04:36'),(176,1,1,'Gerado novo pedido de compra: #61',NULL,'2019-04-19 14:04:49','2019-04-19 14:04:49'),(177,1,1,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno _ADRIANO KEISUKE MUNAKATA, PPT_G2AM',NULL,'2019-05-16 19:47:32','2019-05-16 19:47:32'),(178,1,1,'Acesso com a chave: 10000',NULL,'2019-05-16 19:47:32','2019-05-16 19:47:32'),(179,1,1,'Gerado novo pedido de compra: #1',NULL,'2019-05-16 19:49:53','2019-05-16 19:49:53'),(180,1,1,'Gerado novo pedido SEM COMPROMISSO: #2',NULL,'2019-05-16 19:51:42','2019-05-16 19:51:42'),(181,2,3,'Carolina Vilhena Bittencourt foi adicionado como responsável ao aluno _AIKE JEMINEZ EBINA, PPT_G5AT',NULL,'2019-05-20 11:46:59','2019-05-20 11:46:59'),(182,2,3,'Acesso com a chave: 10002',NULL,'2019-05-20 11:46:59','2019-05-20 11:46:59'),(183,2,3,'Gerado novo pedido de compra: #3',NULL,'2019-05-20 11:47:08','2019-05-20 11:47:08'),(184,2,3,'Gerado novo pedido SEM COMPROMISSO: #4',NULL,'2019-05-20 11:47:12','2019-05-20 11:47:12'),(185,1,1,'Acesso com a chave: 10000',NULL,'2019-07-26 01:07:53','2019-07-26 01:07:53'),(186,1,1,'Gerado novo pedido de compra: #5',NULL,'2019-07-26 01:17:05','2019-07-26 01:17:05'),(187,1,1,'Gerado novo pedido de compra: #6',NULL,'2019-07-26 01:26:55','2019-07-26 01:26:55'),(188,1,1,'Acesso com a chave: 10000',NULL,'2019-07-26 01:30:34','2019-07-26 01:30:34'),(189,1,1,'Gerado novo pedido de compra: #7',NULL,'2019-07-26 01:30:43','2019-07-26 01:30:43'),(190,1,1,'Acesso com a chave: 10000',NULL,'2019-07-26 11:09:35','2019-07-26 11:09:35'),(191,1,1,'Gerado novo pedido de compra: #8',NULL,'2019-07-26 11:09:44','2019-07-26 11:09:44'),(192,1,244,'ANDRE MANSO BITTENCOURT foi adicionado como responsável ao aluno _FELIPE DAS VIRGENS DE LIMA, PPT_G3CT',NULL,'2019-07-26 12:43:04','2019-07-26 12:43:04'),(193,1,244,'Acesso com a chave: 10245',NULL,'2019-07-26 12:43:04','2019-07-26 12:43:04'),(194,3,56,'Rafael foi adicionado como responsável ao aluno _ARTHUR BATONI NORMANDA, PPT_G5BM',NULL,'2019-07-29 08:46:13','2019-07-29 08:46:13'),(195,3,56,'Acesso com a chave: 10057',NULL,'2019-07-29 08:46:13','2019-07-29 08:46:13'),(196,3,56,'Gerado novo pedido de compra: #9',NULL,'2019-07-29 08:46:29','2019-07-29 08:46:29'),(197,1,1,'Acesso com a chave: 10000',NULL,'2019-07-29 10:58:23','2019-07-29 10:58:23'),(198,1,1,'Acesso com a chave: 10000',NULL,'2019-07-29 16:17:41','2019-07-29 16:17:41'),(199,3,56,'Acesso com a chave: 10057',NULL,'2019-07-29 20:30:37','2019-07-29 20:30:37'),(200,1,1,'Acesso com a chave: 10000',NULL,'2019-07-30 00:40:34','2019-07-30 00:40:34');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `logs` with 200 row(s)
--

--
-- Table structure for table `migrations`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `migrations` with 0 row(s)
--

--
-- Table structure for table `not_receives`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `not_receives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `relative_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `not_receives`
--

LOCK TABLES `not_receives` WRITE;
/*!40000 ALTER TABLE `not_receives` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `not_receives` VALUES (1,2,1,'2019-02-12 15:13:42','2019-02-12 15:13:42'),(8,10,31,'2019-02-28 16:06:34','2019-02-28 16:06:34'),(7,10,30,'2019-02-28 16:02:32','2019-02-28 16:02:32');
/*!40000 ALTER TABLE `not_receives` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `not_receives` with 3 row(s)
--

--
-- Table structure for table `order_products`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kit_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `value` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_products`
--

LOCK TABLES `order_products` WRITE;
/*!40000 ALTER TABLE `order_products` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `order_products` VALUES (2,84,2,1,20.00,'2019-05-20 11:47:31','2019-05-20 11:47:31'),(3,84,8,1,56.00,'2019-05-20 11:47:31','2019-05-20 11:47:31'),(4,84,21,1,35.00,'2019-05-20 11:47:31','2019-05-20 11:47:31');
/*!40000 ALTER TABLE `order_products` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `order_products` with 3 row(s)
--

--
-- Table structure for table `orders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `relative_id` int(11) NOT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `type` enum('Venda','Receber Sem Compromisso') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `subtotal` decimal(8,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total` decimal(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `orders` VALUES (9,3,NULL,'Venda','CARRINHO',NULL,140.00,0.00,140.00,'2019-07-29 08:46:29','2019-07-29 08:46:58'),(2,1,NULL,'Receber Sem Compromisso','SEM COMPROMISSO',NULL,0.00,0.00,0.00,'2019-05-16 19:51:42','2019-05-16 19:51:42'),(3,2,NULL,'Venda','CARRINHO',NULL,0.00,0.00,0.00,'2019-05-20 11:47:08','2019-05-20 11:47:08'),(4,2,NULL,'Receber Sem Compromisso','SEM COMPROMISSO',NULL,2211.00,0.00,2211.00,'2019-05-20 11:47:12','2019-05-20 11:48:23'),(5,1,NULL,'Venda','CARRINHO',NULL,0.00,0.00,0.00,'2019-07-26 01:17:05','2019-07-26 01:17:05'),(6,1,NULL,'Venda','CARRINHO',NULL,448.00,0.00,448.00,'2019-07-26 01:26:55','2019-07-26 01:26:55'),(7,1,NULL,'Venda','CARRINHO',NULL,504.00,0.00,504.00,'2019-07-26 01:30:43','2019-07-26 01:49:53'),(8,1,NULL,'Venda','CARRINHO',NULL,700.00,0.00,700.00,'2019-07-26 11:09:44','2019-07-30 00:58:46');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `orders` with 8 row(s)
--

--
-- Table structure for table `password_resets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `password_resets` with 0 row(s)
--

--
-- Table structure for table `payments`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `transaction_code` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
SET autocommit=0;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `payments` with 0 row(s)
--

--
-- Table structure for table `products`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `display_order` int(11) NOT NULL DEFAULT '0',
  `kindergarten` tinyint(1) NOT NULL DEFAULT '0',
  `elementary_school` tinyint(1) NOT NULL DEFAULT '0',
  `high_school` tinyint(1) NOT NULL DEFAULT '0',
  `all_type` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `products` VALUES (1,'Kit com 9 fotos 3x4cm','9 fotos 3x4 impressas em tamanho 10x15 cm','fc957b0dc5d43ec7495586055245aa70.jpeg',20.00,2,1,1,1,1,1,'2019-02-08 13:00:25','2019-02-08 13:34:50'),(2,'Foto Colorida 13x18 cm','Impressa em papel fotográfico, tamanho 13x18 cm','4e4232fad5281df9b6283443b0554014.jpeg',20.00,1,1,1,1,1,1,'2019-02-08 13:14:41','2019-02-12 17:17:29'),(3,'Cartela de Adesivos','Cartela 18x13 cm com 8 Adesivos autocolantes','9a863479b99f1886b036829d2d96e55f.jpeg',29.00,3,1,1,1,1,1,'2019-02-08 13:24:39','2019-02-08 13:33:14'),(4,'Marcador de Página','Impresso em papel couchê 300gr, medidas 4x20 cm','1bbb9818c920bb2148f094aae17675f4.jpeg',13.00,4,1,1,1,1,1,'2019-02-08 13:33:04','2019-02-08 13:33:04'),(5,'TAG de Mochila Berçário','TAG de PVC 8,5 x 5,5 cm, com corrente','dcf68a3929022c93b49eba0744999cc8.png',15.00,5,1,0,0,0,1,'2019-02-08 13:44:24','2019-02-08 13:46:07'),(6,'Tag de Mochila Infantil','TAG de PVC 8,5 x 5,5 cm, com corrente','1e3451d580d83de560a090b0d964c4bd.png',15.00,6,1,0,0,0,1,'2019-02-08 13:45:58','2019-02-08 13:46:21'),(7,'Tag de Mochila','TAG de PVC 8,5 x 5,5 cm, com corrente','e5714bb1c3e3b444fa2f55278a7fb0bd.jpeg',15.00,7,0,1,1,0,1,'2019-02-08 13:59:46','2019-02-08 13:59:46'),(8,'Livreto Ensino Infantil','Foto individual e de turma, com relação de nomes dos alunos e professoras. Impresso em papel couchê 300gr, laminado, tamanho fechado 20x20 cm.','a833ffc1b5ea713a2f672166d149090a.jpeg',56.00,1,1,0,0,0,1,'2019-02-08 14:03:42','2019-02-12 18:10:24'),(9,'Livreto Ensino Fundamental','Composto de duas fotos da turma, uma oficial  e outra divertida. Impresso em papel couchê 300gr, laminado, tamanho fechado 20x20 cm.','e2cc9c6534e18a4a222b8da32af598dc.jpeg',56.00,9,0,1,1,0,1,'2019-02-08 14:04:13','2019-02-08 14:05:36'),(10,'Caneca B1 Feminina','Caneca de porcelana branca personalizada com foto individual','3471784a2f15c7e6faa030cd1ec0b2a7.jpeg',50.00,8,1,0,0,0,1,'2019-02-08 14:08:49','2019-02-20 13:28:19'),(11,'Caneca B1 Masculina','Caneca de porcelana branca personalizada com foto individual','8bdcc33be6a1f4dc0d422edd8e15ed14.jpeg',50.00,11,1,0,0,0,1,'2019-02-08 14:09:48','2019-02-08 14:09:48'),(12,'Caneca Ensino Infantil','Caneca de porcelana branca, personalizada com foto individual','8d06720c5b3192091d0fb32c6590bc22.jpeg',50.00,12,1,0,0,0,1,'2019-02-08 14:11:11','2019-02-08 14:11:11'),(13,'Caneca Fund/EM','Caneca de porcelana branca, personalizada com foto individual','531880967cf095bc42e35b8163ad4e96.jpeg',50.00,13,0,1,1,0,1,'2019-02-08 14:12:35','2019-02-08 14:12:36'),(14,'Quebra Cabeça','15 peças','ecd22e849335a1648b30884498a24639.jpeg',35.00,14,1,1,1,1,1,'2019-02-08 15:29:41','2019-02-08 15:31:56'),(15,'Porta Copos','Kit com 8 unidades.\r\nFeito com cortiça emborrachada.\r\nTamanho: 9,5 x 9,5 cm','9c445b31ad23fe5ab586abc815529fc2.jpeg',56.00,15,1,1,1,1,1,'2019-02-08 15:31:33','2019-02-08 15:31:47'),(16,'Jogo Americano','Kit com 4 unidades.\r\nResistente à água.\r\nTamanho: 28,7 x 40 cm.','a555ab0dc1d45eb0dd7d5056918bbb43.jpeg',74.00,16,1,1,1,1,1,'2019-02-08 15:33:31','2019-02-12 17:21:31'),(17,'Bloco de Notas','Capa dura laminada. Miolo com 100 folhas brancas lisas. Tamanho: 19,5 x 15 cm','a70846bbd0ca411cf2e12d9c69653d18.jpeg',50.00,16,1,1,1,1,1,'2019-02-08 15:35:33','2019-02-08 15:35:33'),(18,'Agenda','Capa dura impressa e laminada.\r\n1 dia por página.\r\nTamanho: 20 x 14 cm.','5be64309ce3eb157d05fce01ce2263b7.jpeg',90.00,17,1,1,1,1,1,'2019-02-08 15:37:09','2019-02-12 17:21:44'),(19,'Porta Retrato opção A','Feito em papel couchê 300gr, projetado para fotos 13x18cm.','6bf0ba258daf8caf7d5b0e148667e797.jpeg',20.00,14,1,1,1,1,1,'2019-02-08 15:51:09','2019-02-08 15:51:09'),(20,'Porta Retrato Opção B','Feito em papel couchê 300gr, projetado para fotos 13x18cm.','8c63b356a13c7a008d5c32c2d1c40e02.jpeg',20.00,15,1,1,1,1,1,'2019-02-08 15:51:54','2019-02-08 15:51:54'),(21,'2 Fotos Coloridas 13x18','Impressas em papel fotográfico','b839806f5ce2f41801b5eed36ecfe524.jpeg',35.00,0,1,1,1,1,1,'2019-02-12 17:18:17','2019-02-12 17:19:02');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `products` with 21 row(s)
--

--
-- Table structure for table `relative_student`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relative_student` (
  `relative_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`relative_id`,`student_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relative_student`
--

LOCK TABLES `relative_student` WRITE;
/*!40000 ALTER TABLE `relative_student` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `relative_student` VALUES (1,1),(1,244),(2,3),(3,56);
/*!40000 ALTER TABLE `relative_student` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `relative_student` with 4 row(s)
--

--
-- Table structure for table `relatives`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relatives`
--

LOCK TABLES `relatives` WRITE;
/*!40000 ALTER TABLE `relatives` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `relatives` VALUES (1,'ANDRE MANSO BITTENCOURT','mansobr@gmail.com','123.123.123-12','(11) 96482-3000',1,'2019-05-16 19:47:32','2019-07-26 12:43:04'),(2,'Carolina Vilhena Bittencourt','carolina@bosquedasletras.com.br','123.123.123-12','(11) 4618-5849',1,'2019-05-20 11:46:59','2019-05-20 11:46:59'),(3,'Rafael','rafael@ex2.com.br','123.123.123-33','(11) 22334-4555',1,'2019-07-29 08:46:13','2019-07-29 08:46:13');
/*!40000 ALTER TABLE `relatives` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `relatives` with 3 row(s)
--

--
-- Table structure for table `settings`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `allow_receive_without_commitment` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `settings` VALUES (1,1,1,'2018-12-29 21:33:25','2019-01-15 11:00:48');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `settings` with 1 row(s)
--

--
-- Table structure for table `students`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_id` int(11) NOT NULL,
  `period` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '1',
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `original_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_without_commitment` enum('Sim','Não','Site') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=317 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `students` VALUES (1,'_ADRIANO KEISUKE MUNAKATA, PPT_G2AM',1,'T','10000',1,'_ADRIANO KEISUKE MUNAKATA, PPT_G2AM.jpg',1,NULL,'Site','2019-05-20 11:41:16','2019-05-20 11:41:16'),(2,'_ADRIANO_KEISUKE_MUNAKATA_PPT_G2AM',1,'T','10001',0,'_ADRIANO_KEISUKE_MUNAKATA_PPT_G2AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(3,'_AIKE JEMINEZ EBINA, PPT_G5AT',1,'T','10002',0,'_AIKE JEMINEZ EBINA, PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(4,'_AIKE_JEMINEZ_EBINA_PPT_G5AT',1,'T','10003',1,'_AIKE_JEMINEZ_EBINA_PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(5,'_ALANA ANDREA GOENAGA PINILLA, PPT_G2DT',1,'T','10004',1,'_ALANA ANDREA GOENAGA PINILLA, PPT_G2DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(6,'_ALANA_ANDREA_GOENAGA_PINILLA_PPT_G2DT',1,'T','10005',1,'_ALANA_ANDREA_GOENAGA_PINILLA_PPT_G2DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(7,'_ALEXIA_SOTILLE_FISCHER_PIRES_DE_CAMPOS_PPT_G4BT',1,'T','10007',1,'_ALEXIA_SOTILLE_FISCHER_PIRES_DE_CAMPOS_PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(8,'_ALICE DIAS MONTEIRO, PPT_G3BM',1,'T','10008',1,'_ALICE DIAS MONTEIRO, PPT_G3BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(9,'_ALICE MARQUES ESSIG, PPT_G4BT',1,'T','10009',1,'_ALICE MARQUES ESSIG, PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(10,'_ALICE SOARES TANAKA, PPT_G5CM',1,'T','10010',1,'_ALICE SOARES TANAKA, PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(11,'_ALICE_DIAS_MONTEIRO_PPT_G3BM',1,'T','10011',1,'_ALICE_DIAS_MONTEIRO_PPT_G3BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(12,'_ALICE_MARQUES_ESSIG_PPT_G4BT',1,'T','10012',0,'_ALICE_MARQUES_ESSIG_PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(13,'_ALICE_SOARES_TANAKA_PPT_G5CM',2,'T','10013',0,'_ALICE_SOARES_TANAKA_PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(14,'_ALYSSA_OMAR_AREF_PPT_G3AT',2,'T','10015',0,'_ALYSSA_OMAR_AREF_PPT_G3AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(15,'_AMANDA PANIZZA, PPT_G3BT',2,'T','10016',0,'_AMANDA PANIZZA, PPT_G3BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(16,'_AMANDA_PANIZZA_PPT_G3BT',2,'T','10017',0,'_AMANDA_PANIZZA_PPT_G3BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(17,'_ANA JÚLIA FRANCO DA SILVA XAVIER, PPT_G5DT',2,'T','10018',0,'_ANA JÚLIA FRANCO DA SILVA XAVIER, PPT_G5DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(18,'_ANA LUÍSA FERREIRA DE LEMOS, PPT_G4BT',2,'T','10019',1,'_ANA LUÍSA FERREIRA DE LEMOS, PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(19,'_ANA LUIZA BALAN ABDALLA, PPT_G1AM',2,'T','10020',1,'_ANA LUIZA BALAN ABDALLA, PPT_G1AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(20,'_ANA LUIZA GERBELLI GOMES, PPT_G5AM',2,'T','10021',1,'_ANA LUIZA GERBELLI GOMES, PPT_G5AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(21,'_ANA_JULIA_FRANCO_DA_SILVA_XAVIER_PPT_G5DT',2,'T','10022',1,'_ANA_JULIA_FRANCO_DA_SILVA_XAVIER_PPT_G5DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(22,'_ANA_LUISA_FERREIRA_DE_LEMOS_PPT_G4BT',2,'T','10023',0,'_ANA_LUISA_FERREIRA_DE_LEMOS_PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(23,'_ANA_LUIZA_BALAN_ABDALLA_PPT_G1AM',2,'T','10024',0,'_ANA_LUIZA_BALAN_ABDALLA_PPT_G1AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(24,'_ANA_LUIZA_GERBELLI_GOMES_PPT_G5AM',2,'T','10025',0,'_ANA_LUIZA_GERBELLI_GOMES_PPT_G5AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(25,'_ANDRÉ ALIPERTI ANGULO, PPT_G4DT',2,'T','10026',0,'_ANDRÉ ALIPERTI ANGULO, PPT_G4DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(26,'_ANDRÉ FIGUEIREDO CARREIRA, PPT_G3CM',2,'T','10027',0,'_ANDRÉ FIGUEIREDO CARREIRA, PPT_G3CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(27,'_ANDRÉ PEIXOTO SOARES, PPT_G2BM',2,'T','10028',0,'_ANDRÉ PEIXOTO SOARES, PPT_G2BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(28,'_ANDRÉ VON GAL PESSOA, PPT_G5CM',2,'T','10029',0,'_ANDRÉ VON GAL PESSOA, PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(29,'_ANDRE_ALIPERTI_ANGULO_PPT_G4DT',2,'T','10030',0,'_ANDRE_ALIPERTI_ANGULO_PPT_G4DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(30,'_ANDRE_FIGUEIREDO_CARREIRA_PPT_G3CM',2,'T','10031',0,'_ANDRE_FIGUEIREDO_CARREIRA_PPT_G3CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(31,'_ANDRE_PEIXOTO_SOARES_PPT_G2BM',2,'T','10032',0,'_ANDRE_PEIXOTO_SOARES_PPT_G2BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(32,'_ANDRE_VON_GAL_PESSOA_PPT_G5CM',3,'T','10033',0,'_ANDRE_VON_GAL_PESSOA_PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(33,'_ANE POZZA ZANFORLIN, PPT_G4AM',3,'T','10034',0,'_ANE POZZA ZANFORLIN, PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(34,'_ANE_POZZA_ZANFORLIN_PPT_G4AM',3,'T','10035',0,'_ANE_POZZA_ZANFORLIN_PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(35,'_ÂNGELO DE CAMPOS TONIOLO, PPT_G5AM',3,'T','10036',0,'_ÂNGELO DE CAMPOS TONIOLO, PPT_G5AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(36,'_ANGELO_DE_CAMPOS_TONIOLO_PPT_G5AM',3,'T','10037',0,'_ANGELO_DE_CAMPOS_TONIOLO_PPT_G5AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(37,'_ANTONELA DE OLIVEIRA FAGUNDES, _B1 < PPT',3,'T','10038',0,'_ANTONELA DE OLIVEIRA FAGUNDES, _B1 < PPT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(38,'_ANTONELLA ARMELIN DA CUNHA PEDÓ, PPT_G3DT',3,'T','10039',0,'_ANTONELLA ARMELIN DA CUNHA PEDÓ, PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(39,'_ANTONELLA AZZUZ JANESCH, PPT_G2DM',3,'T','10040',0,'_ANTONELLA AZZUZ JANESCH, PPT_G2DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(40,'_ANTONELLA MONTEIRO CONTE, PPT_G2AT',3,'T','10041',0,'_ANTONELLA MONTEIRO CONTE, PPT_G2AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(41,'_ANTONELLA STRADIOTTO AUDI, PPT_G4AT',3,'T','10042',0,'_ANTONELLA STRADIOTTO AUDI, PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(42,'_ANTONELLA_ARMELIN_DA_CUNHA_PEDO_PPT_G3DT',3,'T','10043',0,'_ANTONELLA_ARMELIN_DA_CUNHA_PEDO_PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(43,'_ANTONELLA_AZZUZ_JANESCH_PPT_G2DM',3,'T','10044',0,'_ANTONELLA_AZZUZ_JANESCH_PPT_G2DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(44,'_ANTONELLA_MONTEIRO_CONTE_PPT_G2AT',3,'T','10045',0,'_ANTONELLA_MONTEIRO_CONTE_PPT_G2AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(45,'_ANTONELLA_STRADIOTTO_AUDI_PPT_G4AT',3,'T','10046',0,'_ANTONELLA_STRADIOTTO_AUDI_PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(46,'_ANTONIO GABURRO MORETTI, PPT_G5CM',3,'T','10047',0,'_ANTONIO GABURRO MORETTI, PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(47,'_ANTÔNIO LAGE DIONIZIO, PPT_G2CT',3,'T','10048',0,'_ANTÔNIO LAGE DIONIZIO, PPT_G2CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(48,'_ANTONIO MIGUEL ZARVOS, PPT_G5AT',3,'T','10049',0,'_ANTONIO MIGUEL ZARVOS, PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(49,'_ANTONIO RIBEIRO MEYER-PFLUG, PPT_G3BM',3,'T','10050',0,'_ANTONIO RIBEIRO MEYER-PFLUG, PPT_G3BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(50,'_ANTONIO SOLIGO MENDES, PPT_G5AM',3,'T','10051',0,'_ANTONIO SOLIGO MENDES, PPT_G5AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(51,'_ANTONIO_GABURRO_MORETTI_PPT_G5CM',3,'T','10052',0,'_ANTONIO_GABURRO_MORETTI_PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(52,'_ANTONIO_LAGE_DIONIZIO_PPT_G2CT',3,'T','10053',0,'_ANTONIO_LAGE_DIONIZIO_PPT_G2CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(53,'_ANTONIO_MIGUEL_ZARVOS_PPT_G5AT',3,'T','10054',0,'_ANTONIO_MIGUEL_ZARVOS_PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(54,'_ANTONIO_RIBEIRO_MEYER-PFLUG_PPT_G3BM',3,'T','10055',0,'_ANTONIO_RIBEIRO_MEYER-PFLUG_PPT_G3BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(55,'_ANTONIO_SOLIGO_MENDES_PPT_G5AM',3,'T','10056',0,'_ANTONIO_SOLIGO_MENDES_PPT_G5AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(56,'_ARTHUR BATONI NORMANDA, PPT_G5BM',3,'T','10057',0,'_ARTHUR BATONI NORMANDA, PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(57,'_ARTHUR FELIPE KUTZE SOMMER, PPT_G3CM',3,'T','10058',0,'_ARTHUR FELIPE KUTZE SOMMER, PPT_G3CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(58,'_ARTHUR HEBEDA MAKHLOUF, PPT_G4AM',3,'T','10059',0,'_ARTHUR HEBEDA MAKHLOUF, PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(59,'_ARTHUR KAISER MACHADO AZEVEDO, PPT_G4AM',3,'T','10060',0,'_ARTHUR KAISER MACHADO AZEVEDO, PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(60,'_ARTHUR MOTA DE ANDRADE, PPT_G5BM',3,'T','10061',0,'_ARTHUR MOTA DE ANDRADE, PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(61,'_ARTHUR TRIBST WHYTE GAILEY, PPT_G2DT',3,'T','10062',0,'_ARTHUR TRIBST WHYTE GAILEY, PPT_G2DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(62,'_ARTHUR_BATONI_NORMANDA_PPT_G5BM',3,'T','10063',0,'_ARTHUR_BATONI_NORMANDA_PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(63,'_ARTHUR_FELIPE_KUTZE_SOMMER_PPT_G3CM',3,'T','10064',0,'_ARTHUR_FELIPE_KUTZE_SOMMER_PPT_G3CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(64,'_ARTHUR_HEBEDA_MAKHLOUF_PPT_G4AM',3,'T','10065',0,'_ARTHUR_HEBEDA_MAKHLOUF_PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(65,'_ARTHUR_KAISER_MACHADO_AZEVEDO_PPT_G4AM',3,'T','10066',0,'_ARTHUR_KAISER_MACHADO_AZEVEDO_PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(66,'_ARTHUR_MOTA_DE_ANDRADE_PPT_G5BM',3,'T','10067',0,'_ARTHUR_MOTA_DE_ANDRADE_PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(67,'_ARTHUR_TRIBST_WHYTE_GAILEY_PPT_G2DT',3,'T','10068',0,'_ARTHUR_TRIBST_WHYTE_GAILEY_PPT_G2DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(68,'_B1 < PPT, _BERNARDO DE FREITAS CONSTANTIN',3,'T','10069',0,'_B1 < PPT, _BERNARDO DE FREITAS CONSTANTIN.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(69,'_B1 < PPT, _DIANA LOCKEMANN DOS REIS',3,'T','10070',0,'_B1 < PPT, _DIANA LOCKEMANN DOS REIS.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(70,'_B1 < PPT, _FELIPE YUKI KOBASHI',3,'T','10071',0,'_B1 < PPT, _FELIPE YUKI KOBASHI.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(71,'_B1 < PPT, _HELENA MARINHO PAVANELLI',3,'T','10072',0,'_B1 < PPT, _HELENA MARINHO PAVANELLI.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(72,'_B1 < PPT, _JÚLIA CEREZER MELGARÉ',3,'T','10073',0,'_B1 < PPT, _JÚLIA CEREZER MELGARÉ.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(73,'_B1 < PPT, _LUCAS PIERRE BAPTISTE DE LAREBERDIERE',3,'T','10074',0,'_B1 < PPT, _LUCAS PIERRE BAPTISTE DE LAREBERDIERE.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(74,'_B1 < PPT, _LUCAS',3,'T','10075',0,'_B1 < PPT, _LUCAS.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(75,'_B1 < PPT, _LUÍS FELIPE DE CAMARGO FILHO',3,'T','10076',0,'_B1 < PPT, _LUÍS FELIPE DE CAMARGO FILHO.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(76,'_B1 < PPT, _MANUELA CARVALHO AVILA',3,'T','10077',0,'_B1 < PPT, _MANUELA CARVALHO AVILA.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(77,'_B1 < PPT, _MARTIN BAZZINI JAEGER',3,'T','10078',0,'_B1 < PPT, _MARTIN BAZZINI JAEGER.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(78,'_B1 < PPT, _MAYA PRATA CANINA',3,'T','10079',0,'_B1 < PPT, _MAYA PRATA CANINA.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(79,'_B1 < PPT, _RICARDO MACEDO DOS SANTOS COSTA',4,'T','10080',0,'_B1 < PPT, _RICARDO MACEDO DOS SANTOS COSTA.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(80,'_B1 < PPT, _RODRIGO YOSHIO SHIN-IKE TATIBANA',4,'T','10081',0,'_B1 < PPT, _RODRIGO YOSHIO SHIN-IKE TATIBANA.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(81,'_B1 < PPT, _THEODORO BEBER ROCHA',4,'T','10082',0,'_B1 < PPT, _THEODORO BEBER ROCHA.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(82,'_BÁRBARA GRANCHI MIRAGLIA, PPT_G5BM',4,'T','10083',0,'_BÁRBARA GRANCHI MIRAGLIA, PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(83,'_BARBARA_GRANCHI_MIRAGLIA_PPT_G5BM',4,'T','10084',0,'_BARBARA_GRANCHI_MIRAGLIA_PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(84,'_BEATRIZ ALIPERTI ANGULO, PPT_G2BT',4,'T','10085',0,'_BEATRIZ ALIPERTI ANGULO, PPT_G2BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(85,'_BEATRIZ BONALDO, PPT_G4BM',4,'T','10086',0,'_BEATRIZ BONALDO, PPT_G4BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(86,'_BEATRIZ CECCATO PALÁCIO, PPT_G5AT',4,'T','10087',0,'_BEATRIZ CECCATO PALÁCIO, PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(87,'_BEATRIZ CRUZ SOUZA, PPT_G3BM',4,'T','10088',0,'_BEATRIZ CRUZ SOUZA, PPT_G3BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(88,'_BEATRIZ DA FONSECA TIMM, PPT_G4CM',4,'T','10089',0,'_BEATRIZ DA FONSECA TIMM, PPT_G4CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(89,'_BEATRIZ DE ABREU ALENCAR FLORENCE, PPT_G4AM',4,'T','10090',0,'_BEATRIZ DE ABREU ALENCAR FLORENCE, PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(90,'_BEATRIZ LILLO BORNIA, PPT_G3AM',4,'T','10091',0,'_BEATRIZ LILLO BORNIA, PPT_G3AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(91,'_BEATRIZ NASTRI CASTRO MELO, PPT_G3AM',4,'T','10092',0,'_BEATRIZ NASTRI CASTRO MELO, PPT_G3AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(92,'_BEATRIZ PEÇANHA MENDONÇA GONÇALVES, PPT_G3DT',4,'T','10093',0,'_BEATRIZ PEÇANHA MENDONÇA GONÇALVES, PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(93,'_BEATRIZ RIBEIRO DE LAVOR, PPT_G3DM',4,'T','10094',0,'_BEATRIZ RIBEIRO DE LAVOR, PPT_G3DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(94,'_BEATRIZ SILICANI CARDOSO CORREA, PPT_G4CT',4,'T','10095',0,'_BEATRIZ SILICANI CARDOSO CORREA, PPT_G4CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(95,'_BEATRIZ_ALIPERTI_ANGULO_PPT_G2BT',4,'T','10096',0,'_BEATRIZ_ALIPERTI_ANGULO_PPT_G2BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(96,'_BEATRIZ_BONALDO_PPT_G4BM',4,'T','10097',0,'_BEATRIZ_BONALDO_PPT_G4BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(97,'_BEATRIZ_CECCATO_PALACIO_PPT_G5AT',4,'T','10098',0,'_BEATRIZ_CECCATO_PALACIO_PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(98,'_BEATRIZ_CRUZ_SOUZA_PPT_G3BM',4,'T','10099',0,'_BEATRIZ_CRUZ_SOUZA_PPT_G3BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(99,'_BEATRIZ_DA_FONSECA_TIMM_PPT_G4CM',4,'T','10100',0,'_BEATRIZ_DA_FONSECA_TIMM_PPT_G4CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(100,'_BEATRIZ_DE_ABREU_ALENCAR_FLORENCE_PPT_G4AM',4,'T','10101',0,'_BEATRIZ_DE_ABREU_ALENCAR_FLORENCE_PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(101,'_BEATRIZ_LILLO_BORNIA_PPT_G3AM',4,'T','10102',0,'_BEATRIZ_LILLO_BORNIA_PPT_G3AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(102,'_BEATRIZ_NASTRI_CASTRO_MELO_PPT_G3AM',4,'T','10103',0,'_BEATRIZ_NASTRI_CASTRO_MELO_PPT_G3AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(103,'_BEATRIZ_PECANHA_MENDONCA_GONCALVES_PPT_G3DT',4,'T','10104',0,'_BEATRIZ_PECANHA_MENDONCA_GONCALVES_PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(104,'_BEATRIZ_RIBEIRO_DE_LAVOR_PPT_G3DM',4,'T','10105',0,'_BEATRIZ_RIBEIRO_DE_LAVOR_PPT_G3DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(105,'_BEATRIZ_SILICANI_CARDOSO_CORREA_PPT_G4CT',4,'T','10106',0,'_BEATRIZ_SILICANI_CARDOSO_CORREA_PPT_G4CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(106,'_BENICIO FERNANDES BRISSOW, PPT_G3AM',4,'T','10107',0,'_BENICIO FERNANDES BRISSOW, PPT_G3AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(107,'_BENÍCIO MATTA HARDER, PPT_G3DT',4,'T','10108',0,'_BENÍCIO MATTA HARDER, PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(108,'_BENÍCIO RAIMONDI BARROS NETTO, PPT_G3CM',4,'T','10109',0,'_BENÍCIO RAIMONDI BARROS NETTO, PPT_G3CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(109,'_BENÍCIO RODRIGUES IMPARATO, PPT_G2CT',4,'T','10110',0,'_BENÍCIO RODRIGUES IMPARATO, PPT_G2CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(110,'_BENICIO_FERNANDES_BRISSOW_PPT_G3AM',4,'T','10111',0,'_BENICIO_FERNANDES_BRISSOW_PPT_G3AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(111,'_BENICIO_MATTA_HARDER_PPT_G3DT',4,'T','10112',0,'_BENICIO_MATTA_HARDER_PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(112,'_BENICIO_RAIMONDI_BARROS_NETTO_PPT_G3CM',4,'T','10113',0,'_BENICIO_RAIMONDI_BARROS_NETTO_PPT_G3CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(113,'_BENICIO_RODRIGUES_IMPARATO_PPT_G2CT',4,'T','10114',0,'_BENICIO_RODRIGUES_IMPARATO_PPT_G2CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(114,'_BENJAMIN JALES BERGER, PPT_G2CT',4,'T','10115',0,'_BENJAMIN JALES BERGER, PPT_G2CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(115,'_BENJAMIN_JALES_BERGER_PPT_G2CT',4,'T','10116',0,'_BENJAMIN_JALES_BERGER_PPT_G2CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(116,'_BENJAMIN_LOPES_GARCIA_CARPINELLI_DOS_SANTOS_PPT_G1AT',4,'T','10117',0,'_BENJAMIN_LOPES_GARCIA_CARPINELLI_DOS_SANTOS_PPT_G1AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(117,'_BENJAMIN_PPT_G1AM',4,'T','10118',0,'_BENJAMIN_PPT_G1AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(118,'_BENJAMIN, PPT_G1AM',4,'T','10119',0,'_BENJAMIN, PPT_G1AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(119,'_BERNARDO ALMEIDA MARCHESAN, PPT_G4CT',4,'T','10120',0,'_BERNARDO ALMEIDA MARCHESAN, PPT_G4CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(120,'_BERNARDO BIANCOLINI ROSA, PPT_G2BT',4,'T','10121',0,'_BERNARDO BIANCOLINI ROSA, PPT_G2BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(121,'_BERNARDO DONHA CARDOSO, PPT_G4AM',4,'T','10122',0,'_BERNARDO DONHA CARDOSO, PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(122,'_BERNARDO DUTRA BROSSI, PPT_G3CT',4,'T','10123',0,'_BERNARDO DUTRA BROSSI, PPT_G3CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(123,'_BERNARDO ZAZULA PARAJO, PPT_G4BT',4,'T','10124',0,'_BERNARDO ZAZULA PARAJO, PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(124,'_BERNARDO_ALMEIDA_MARCHESAN_PPT_G4CT',4,'T','10125',0,'_BERNARDO_ALMEIDA_MARCHESAN_PPT_G4CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(125,'_BERNARDO_BIANCOLINI_ROSA_PPT_G2BT',5,'T','10126',0,'_BERNARDO_BIANCOLINI_ROSA_PPT_G2BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(126,'_BERNARDO_DE_FREITAS_CONSTANTIN_PPT_B1',5,'T','10127',0,'_BERNARDO_DE_FREITAS_CONSTANTIN_PPT_B1.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(127,'_BERNARDO_DONHA_CARDOSO_PPT_G4AM',5,'T','10128',0,'_BERNARDO_DONHA_CARDOSO_PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(128,'_BERNARDO_DUTRA_BROSSI_PPT_G3CT',5,'T','10129',0,'_BERNARDO_DUTRA_BROSSI_PPT_G3CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(129,'_BERNARDO_ZAZULA_PARAJO_PPT_G4BT',5,'T','10130',0,'_BERNARDO_ZAZULA_PARAJO_PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(130,'_BIANCA COSTA GARCIA D\'ANTONA, PPT_G3BT',5,'T','10131',0,'_BIANCA COSTA GARCIA D\'ANTONA, PPT_G3BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(131,'_BIANCA DE SOUZA MORETTI SOARES, PPT_G5DM',5,'T','10132',0,'_BIANCA DE SOUZA MORETTI SOARES, PPT_G5DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(132,'_BIANCA PATRIARCHA MACIAS, PPT_G5AM',5,'T','10133',0,'_BIANCA PATRIARCHA MACIAS, PPT_G5AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(133,'_BIANCA_DE_SOUZA_MORETTI_SOARES_PPT_G5DM',5,'T','10134',0,'_BIANCA_DE_SOUZA_MORETTI_SOARES_PPT_G5DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(134,'_BIANCA_PATRIARCHA_MACIAS_PPT_G5AM',5,'T','10135',0,'_BIANCA_PATRIARCHA_MACIAS_PPT_G5AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(135,'_BRUNO ALONSO SARAIVA, PPT_G4AT',5,'T','10136',0,'_BRUNO ALONSO SARAIVA, PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(136,'_BRUNO BIANCHINI, PPT_G2BM',5,'T','10137',0,'_BRUNO BIANCHINI, PPT_G2BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(137,'_BRUNO BISCÁRO CHICO, PPT_G3DT',5,'T','10138',0,'_BRUNO BISCÁRO CHICO, PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(138,'_BRUNO GALVÃO CASTIGLIA DE OLIVEIRA, PPT_G5CM',5,'T','10139',0,'_BRUNO GALVÃO CASTIGLIA DE OLIVEIRA, PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(139,'_BRUNO_ALONSO_SARAIVA_PPT_G4AT',5,'T','10140',0,'_BRUNO_ALONSO_SARAIVA_PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(140,'_BRUNO_BIANCHINI_PPT_G2BM',5,'T','10141',0,'_BRUNO_BIANCHINI_PPT_G2BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(141,'_BRUNO_BISCARO_CHICO_PPT_G3DT',5,'T','10142',0,'_BRUNO_BISCARO_CHICO_PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(142,'_BRUNO_GALVAO_CASTIGLIA_DE_OLIVEIRA_PPT_G5CM',5,'T','10143',0,'_BRUNO_GALVAO_CASTIGLIA_DE_OLIVEIRA_PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(143,'_CAIO ROLAND AJUDARTE, PPT_G2DM',5,'T','10144',0,'_CAIO ROLAND AJUDARTE, PPT_G2DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(144,'_CAIO TELES LOPES DA SILVA, PPT_G4DT',5,'T','10145',0,'_CAIO TELES LOPES DA SILVA, PPT_G4DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(145,'_CAIO_ROLAND_AJUDARTE_PPT_G2DM',5,'T','10146',0,'_CAIO_ROLAND_AJUDARTE_PPT_G2DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(146,'_CAIO_TELES_LOPES_DA_SILVA_PPT_G4DT',5,'T','10147',0,'_CAIO_TELES_LOPES_DA_SILVA_PPT_G4DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(147,'_CAMILA CAVALCANTI DE CARVALHO REBÊLO, PPT_G4DM',5,'T','10148',0,'_CAMILA CAVALCANTI DE CARVALHO REBÊLO, PPT_G4DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(148,'_CAMILA_CAVALCANTI_DE_CARVALHO_REBELO_PPT_G4DM',5,'T','10149',0,'_CAMILA_CAVALCANTI_DE_CARVALHO_REBELO_PPT_G4DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(149,'_CARLOS AUGUSTO GARCIA SCATIMBURGO, PPT_G2CM',5,'T','10150',0,'_CARLOS AUGUSTO GARCIA SCATIMBURGO, PPT_G2CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(150,'_CARLOS EDUARDO GARCIA SCATIMBURGO, PPT_G5CM',5,'T','10151',0,'_CARLOS EDUARDO GARCIA SCATIMBURGO, PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(151,'_CARLOS_AUGUSTO_GARCIA_SCATIMBURGO_PPT_G2CM',5,'T','10152',0,'_CARLOS_AUGUSTO_GARCIA_SCATIMBURGO_PPT_G2CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(152,'_CARLOS_EDUARDO_GARCIA_SCATIMBURGO_PPT_G5CM',5,'T','10153',0,'_CARLOS_EDUARDO_GARCIA_SCATIMBURGO_PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(153,'_CAROLINA BREVE SCHEMANN, PPT_G3AT',5,'T','10154',0,'_CAROLINA BREVE SCHEMANN, PPT_G3AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(154,'_CAROLINA JORGE FERREIRA, PPT_G2CT',5,'T','10155',0,'_CAROLINA JORGE FERREIRA, PPT_G2CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(155,'_CAROLINA MARTINS ESPERANDIO, PPT_G4DT',5,'T','10156',0,'_CAROLINA MARTINS ESPERANDIO, PPT_G4DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(156,'_CAROLINA MOTA DE ANDRADE, PPT_G3DM',5,'T','10157',0,'_CAROLINA MOTA DE ANDRADE, PPT_G3DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(157,'_CAROLINA PIO GABBAY, PPT_G2BT',5,'T','10158',0,'_CAROLINA PIO GABBAY, PPT_G2BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(158,'_CAROLINA TONIOLO BUENO, PPT_G3DT',5,'T','10159',0,'_CAROLINA TONIOLO BUENO, PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(159,'_CAROLINA_BREVE_SCHEMANN_PPT_G3AT',5,'T','10160',0,'_CAROLINA_BREVE_SCHEMANN_PPT_G3AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(160,'_CAROLINA_JORGE_FERREIRA_PPT_G2CT',5,'T','10161',0,'_CAROLINA_JORGE_FERREIRA_PPT_G2CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(161,'_CAROLINA_MARTINS_ESPERANDIO_PPT_G4DT',5,'T','10162',0,'_CAROLINA_MARTINS_ESPERANDIO_PPT_G4DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(162,'_CAROLINA_MOTA_DE_ANDRADE_PPT_G3DM',5,'T','10163',0,'_CAROLINA_MOTA_DE_ANDRADE_PPT_G3DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(163,'_CAROLINA_PIO_GABBAY_PPT_G2BT',5,'T','10164',0,'_CAROLINA_PIO_GABBAY_PPT_G2BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(164,'_CAROLINA_TONIOLO_BUENO_PPT_G3DT',5,'T','10165',0,'_CAROLINA_TONIOLO_BUENO_PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(165,'_CAROLINE LABREGO BARBERIS, PPT_G5BM',5,'T','10166',0,'_CAROLINE LABREGO BARBERIS, PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(166,'_CAROLINE MORAES CHIORO DOS SANTOS, PPT_G2AM',5,'T','10167',0,'_CAROLINE MORAES CHIORO DOS SANTOS, PPT_G2AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(167,'_CAROLINE_LABREGO_BARBERIS_PPT_G5BM',5,'T','10168',0,'_CAROLINE_LABREGO_BARBERIS_PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(168,'_CAROLINE_MORAES_CHIORO_DOS_SANTOS_PPT_G2AM',5,'T','10169',0,'_CAROLINE_MORAES_CHIORO_DOS_SANTOS_PPT_G2AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(169,'_CATARINA MENDES SEDEH, PPT_G3DT',5,'T','10170',0,'_CATARINA MENDES SEDEH, PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(170,'_CATARINA RIBEIRO DE PADUA SILVA, PPT_G2AT',5,'T','10171',0,'_CATARINA RIBEIRO DE PADUA SILVA, PPT_G2AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(171,'_CATARINA YOSHIDA MEDEIROS DE ALMEIDA, PPT_G5CT',5,'T','10172',0,'_CATARINA YOSHIDA MEDEIROS DE ALMEIDA, PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(172,'_CATARINA_MENDES_SEDEH_PPT_G3DT',5,'T','10173',0,'_CATARINA_MENDES_SEDEH_PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(173,'_CATARINA_RIBEIRO_DE_PADUA_SILVA_PPT_G2AT',5,'T','10174',0,'_CATARINA_RIBEIRO_DE_PADUA_SILVA_PPT_G2AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(174,'_CATARINA_YOSHIDA_MEDEIROS_DE_ALMEIDA_PPT_G5CT',5,'T','10175',0,'_CATARINA_YOSHIDA_MEDEIROS_DE_ALMEIDA_PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(175,'_CATHARINA HIGINO DIAS, PPT_G3AT',5,'T','10176',0,'_CATHARINA HIGINO DIAS, PPT_G3AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(176,'_CATHARINA SOPHIA C. B. B. VON RAUTENFELD, PPT_G5CT',5,'T','10177',0,'_CATHARINA SOPHIA C. B. B. VON RAUTENFELD, PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(177,'_CATHARINA_HIGINO_DIAS_PPT_G3AT',5,'T','10178',0,'_CATHARINA_HIGINO_DIAS_PPT_G3AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(178,'_CATHARINA_SOPHIA_C_B_B_VON_RAUTENFELD_PPT_G5CT',5,'T','10179',0,'_CATHARINA_SOPHIA_C_B_B_VON_RAUTENFELD_PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(179,'_CATHERINE SOTILLE FISCHER PIRES DE CAMPOS, PPT_G4AT',5,'T','10180',0,'_CATHERINE SOTILLE FISCHER PIRES DE CAMPOS, PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(180,'_CATHERINE_SOTILLE_FISCHER_PIRES_DE_CAMPOS_PPT_G4AT',5,'T','10181',0,'_CATHERINE_SOTILLE_FISCHER_PIRES_DE_CAMPOS_PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(181,'_CAUÃ COSTA DE SOUSA RODRIGUES, PPT_G5CT',5,'T','10182',0,'_CAUÃ COSTA DE SOUSA RODRIGUES, PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(182,'_CAUA_COSTA_DE_SOUSA_RODRIGUES_PPT_G5CT',5,'T','10183',0,'_CAUA_COSTA_DE_SOUSA_RODRIGUES_PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(183,'_CECÍLIA PRADO OLIVEIRA, PPT_G2BM',5,'T','10184',0,'_CECÍLIA PRADO OLIVEIRA, PPT_G2BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(184,'_CECILIA_PRADO_OLIVEIRA_PPT_G2BM',5,'T','10185',0,'_CECILIA_PRADO_OLIVEIRA_PPT_G2BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(185,'_CELINE MICHELETTO CARREIRA, PPT_G1CT',5,'T','10186',0,'_CELINE MICHELETTO CARREIRA, PPT_G1CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(186,'_CELINE_MICHELETTO_CARREIRA_PPT_G1CT',5,'T','10187',0,'_CELINE_MICHELETTO_CARREIRA_PPT_G1CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(187,'_CHIARA PIRES LARA, PPT_G5CM',5,'T','10188',0,'_CHIARA PIRES LARA, PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(188,'_CHIARA_PIRES_LARA_PPT_G5CM',5,'T','10189',0,'_CHIARA_PIRES_LARA_PPT_G5CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(189,'_CLARA HARUMI TOYOTA MADURO, PPT_G2DM',5,'T','10190',0,'_CLARA HARUMI TOYOTA MADURO, PPT_G2DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(190,'_CLARA NAVAS OLIVEIRA, PPT_G1BM',5,'T','10191',0,'_CLARA NAVAS OLIVEIRA, PPT_G1BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(191,'_CLARA_HARUMI_TOYOTA_MADURO_PPT_G2DM',5,'T','10192',0,'_CLARA_HARUMI_TOYOTA_MADURO_PPT_G2DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(192,'_CLARA_NAVAS_OLIVEIRA_PPT_G1BM',5,'T','10193',0,'_CLARA_NAVAS_OLIVEIRA_PPT_G1BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(193,'_CLARISSE DE FREITAS BOCCATTO ROSA, PPT_G3DM',5,'T','10194',0,'_CLARISSE DE FREITAS BOCCATTO ROSA, PPT_G3DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(194,'_CLARISSE_DE_FREITAS_BOCCATTO_ROSA_PPT_G3DM',5,'T','10195',0,'_CLARISSE_DE_FREITAS_BOCCATTO_ROSA_PPT_G3DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(195,'_CORA RAIMONDI, PPT_G2BM',5,'T','10196',0,'_CORA RAIMONDI, PPT_G2BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(196,'_CORA_RAIMONDI_PPT_G2BM',1,'T','10197',0,'_CORA_RAIMONDI_PPT_G2BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(197,'_DANIEL CALLIX RUPEREZ, PPT_G5CT',1,'T','10198',0,'_DANIEL CALLIX RUPEREZ, PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(198,'_DANIEL PASTOR SILVA < PPT_G2CM, PPT_G2CM',1,'T','10199',0,'_DANIEL PASTOR SILVA < PPT_G2CM, PPT_G2CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(199,'_DANIEL_CALLIX_RUPEREZ_PPT_G5CT',1,'T','10200',0,'_DANIEL_CALLIX_RUPEREZ_PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(200,'_DANIEL_PASTOR_SILVA_PPT_G2CM_PPT_G2CM',1,'T','10201',0,'_DANIEL_PASTOR_SILVA_PPT_G2CM_PPT_G2CM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(201,'_DANIELA SHIN-IKE TATIBANA, PPT_G5DM',1,'T','10202',0,'_DANIELA SHIN-IKE TATIBANA, PPT_G5DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(202,'_DANIELA_SHIN-IKE_TATIBANA_PPT_G5DM',1,'T','10203',0,'_DANIELA_SHIN-IKE_TATIBANA_PPT_G5DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(203,'_DAVI ALVES DE SOUZA, PPT_G5DT',1,'T','10204',0,'_DAVI ALVES DE SOUZA, PPT_G5DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(204,'_DAVI EXPOSITO, PPT_G4CT',1,'T','10205',0,'_DAVI EXPOSITO, PPT_G4CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(205,'_DAVI GABRIEL FERRANTI TEIXEIRA BOTEGA, PPT_G2AT',1,'T','10206',0,'_DAVI GABRIEL FERRANTI TEIXEIRA BOTEGA, PPT_G2AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(206,'_DAVI KLAUS CAPEL < PPT_G5DT, PPT_G5DT',1,'T','10207',0,'_DAVI KLAUS CAPEL < PPT_G5DT, PPT_G5DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(207,'_DAVI MACHADO NAVARRO, PPT_G3DT',1,'T','10208',0,'_DAVI MACHADO NAVARRO, PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(208,'_DAVI MARTENSEN BOZONI, PPT_G5BT',1,'T','10209',0,'_DAVI MARTENSEN BOZONI, PPT_G5BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(209,'_DAVI PITERI LOBO, PPT_G2CT',1,'T','10210',0,'_DAVI PITERI LOBO, PPT_G2CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(210,'_DAVI_ALVES_DE_SOUZA_PPT_G5DT',1,'T','10211',0,'_DAVI_ALVES_DE_SOUZA_PPT_G5DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(211,'_DAVI_EXPOSITO_PPT_G4CT',1,'T','10212',0,'_DAVI_EXPOSITO_PPT_G4CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(212,'_DAVI_GABRIEL_FERRANTI_TEIXEIRA_BOTEGA_PPT_G2AT',1,'T','10213',0,'_DAVI_GABRIEL_FERRANTI_TEIXEIRA_BOTEGA_PPT_G2AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(213,'_DAVI_KLAUS_CAPEL_PPT_G5DT_PPT_G5DT',1,'T','10214',0,'_DAVI_KLAUS_CAPEL_PPT_G5DT_PPT_G5DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(214,'_DAVI_MACHADO_NAVARRO_PPT_G3DT',1,'T','10215',0,'_DAVI_MACHADO_NAVARRO_PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(215,'_DAVI_MARTENSEN_BOZONI_PPT_G5BT',1,'T','10216',0,'_DAVI_MARTENSEN_BOZONI_PPT_G5BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(216,'_DAVI_PITERI_LOBO_PPT_G2CT',1,'T','10217',0,'_DAVI_PITERI_LOBO_PPT_G2CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(217,'_DAVID FALCÃO GORENSTEIN, PPT_G2BM',1,'T','10218',0,'_DAVID FALCÃO GORENSTEIN, PPT_G2BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(218,'_DAVID_FALCAO_GORENSTEIN_PPT_G2BM',1,'T','10219',0,'_DAVID_FALCAO_GORENSTEIN_PPT_G2BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(219,'_DIANA LOCKEMANN DOS REIS < PPT_G1BM, PPT_G1BM',1,'T','10220',0,'_DIANA LOCKEMANN DOS REIS < PPT_G1BM, PPT_G1BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(220,'_DIANA_LOCKEMANN_DOS_REIS_PPT_B1',1,'T','10221',0,'_DIANA_LOCKEMANN_DOS_REIS_PPT_B1.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(221,'_EDUARDA CARDOSO DE CRESCI NETO, PPT_G1AT',1,'T','10222',0,'_EDUARDA CARDOSO DE CRESCI NETO, PPT_G1AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(222,'_EDUARDA_CARDOSO_DE_CRESCI_NETO_PPT_G1AT',1,'T','10223',0,'_EDUARDA_CARDOSO_DE_CRESCI_NETO_PPT_G1AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(223,'_EDUARDO MARIA PEREIRA DA COSTA SEQUEIRA NUNES, PPT_G5BM',1,'T','10224',0,'_EDUARDO MARIA PEREIRA DA COSTA SEQUEIRA NUNES, PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(224,'_EDUARDO_MARIA_PEREIRA_DA_COSTA_SEQUEIRA_NUNES_PPT_G5BM',1,'T','10225',0,'_EDUARDO_MARIA_PEREIRA_DA_COSTA_SEQUEIRA_NUNES_PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(225,'_ENRICO CORRÊA RAMALHO, PPT_G2DT',1,'T','10226',0,'_ENRICO CORRÊA RAMALHO, PPT_G2DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(226,'_ENRICO POLO PARETO, PPT_G4AT',1,'T','10227',0,'_ENRICO POLO PARETO, PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(227,'_ENRICO RANGEL D\'ANGELO, PPT_G5CT',1,'T','10228',0,'_ENRICO RANGEL D\'ANGELO, PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(228,'_ENRICO RAYMUNDI HAMUCHE, PPT_G5AT',1,'T','10229',0,'_ENRICO RAYMUNDI HAMUCHE, PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(229,'_ENRICO_CORREA_RAMALHO_PPT_G2DT',1,'T','10230',0,'_ENRICO_CORREA_RAMALHO_PPT_G2DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(230,'_ENRICO_POLO_PARETO_PPT_G4AT',1,'T','10231',0,'_ENRICO_POLO_PARETO_PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(231,'_ENRICO_RANGEL_DANGELO_PPT_G5CT',1,'T','10232',0,'_ENRICO_RANGEL_DANGELO_PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(232,'_ENRICO_RAYMUNDI_HAMUCHE_PPT_G5AT',1,'T','10233',0,'_ENRICO_RAYMUNDI_HAMUCHE_PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(233,'_Enzo (descobrir sobrenome), PPT_G2AT',1,'T','10234',0,'_Enzo (descobrir sobrenome), PPT_G2AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(234,'_ENZO FERNANDES DE NICOLA BECHARA, PPT_G2DM',1,'T','10235',0,'_ENZO FERNANDES DE NICOLA BECHARA, PPT_G2DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(235,'_ENZO HARUTO SATO MORI, PPT_G5BM',1,'T','10236',0,'_ENZO HARUTO SATO MORI, PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(236,'_ENZO KOICHI FUKIMOTO, PPT_G4AM',1,'T','10237',0,'_ENZO KOICHI FUKIMOTO, PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(237,'_Enzo_(descobrir_sobrenome)_PPT_G2AT',1,'T','10238',0,'_Enzo_(descobrir_sobrenome)_PPT_G2AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(238,'_ENZO_FERNANDES_DE_NICOLA_BECHARA_PPT_G2DM',1,'T','10239',0,'_ENZO_FERNANDES_DE_NICOLA_BECHARA_PPT_G2DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(239,'_ENZO_HARUTO_SATO_MORI_PPT_G5BM',1,'T','10240',0,'_ENZO_HARUTO_SATO_MORI_PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(240,'_ENZO_KOICHI_FUKIMOTO_PPT_G4AM',1,'T','10241',0,'_ENZO_KOICHI_FUKIMOTO_PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(241,'_FABIO GONÇALVES SALLES, PPT_G4DM',1,'T','10242',0,'_FABIO GONÇALVES SALLES, PPT_G4DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(242,'_FABIO_GONCALVES_SALLES_PPT_G4DM',1,'T','10243',0,'_FABIO_GONCALVES_SALLES_PPT_G4DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(243,'_FELIPE ARAÚJO BUTTI DE FREITAS, PPT_G4DT',1,'T','10244',0,'_FELIPE ARAÚJO BUTTI DE FREITAS, PPT_G4DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(244,'_FELIPE DAS VIRGENS DE LIMA, PPT_G3CT',1,'T','10245',0,'_FELIPE DAS VIRGENS DE LIMA, PPT_G3CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(245,'_FELIPE DAVID RANGEL, PPT_G5DT',1,'T','10246',0,'_FELIPE DAVID RANGEL, PPT_G5DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(246,'_FELIPE MAFFEI BOULOS, PPT_G4AT',2,'T','10247',0,'_FELIPE MAFFEI BOULOS, PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(247,'_FELIPE PASTOR FILHO, PPT_G2AM',2,'T','10248',0,'_FELIPE PASTOR FILHO, PPT_G2AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(248,'_FELIPE PIMENTEL ALGODOAL, PPT_G1CT',2,'T','10249',0,'_FELIPE PIMENTEL ALGODOAL, PPT_G1CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(249,'_FELIPE SARACCHI SERRA, PPT_G2AM',2,'T','10250',0,'_FELIPE SARACCHI SERRA, PPT_G2AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(250,'_FELIPE_ARAUJO_BUTTI_DE_FREITAS_PPT_G4DT',2,'T','10251',0,'_FELIPE_ARAUJO_BUTTI_DE_FREITAS_PPT_G4DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(251,'_FELIPE_DAS_VIRGENS_DE_LIMA_PPT_G3CT',2,'T','10252',0,'_FELIPE_DAS_VIRGENS_DE_LIMA_PPT_G3CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(252,'_FELIPE_DAVID_RANGEL_PPT_G5DT',2,'T','10253',0,'_FELIPE_DAVID_RANGEL_PPT_G5DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(253,'_FELIPE_MAFFEI_BOULOS_PPT_G4AT',2,'T','10254',0,'_FELIPE_MAFFEI_BOULOS_PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(254,'_FELIPE_PASTOR_FILHO_PPT_G2AM',2,'T','10255',0,'_FELIPE_PASTOR_FILHO_PPT_G2AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(255,'_FELIPE_PIMENTEL_ALGODOAL_PPT_G1CT',2,'T','10256',0,'_FELIPE_PIMENTEL_ALGODOAL_PPT_G1CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(256,'_FELIPE_SARACCHI_SERRA_PPT_G2AM',2,'T','10257',0,'_FELIPE_SARACCHI_SERRA_PPT_G2AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(257,'_FELIPE_YUKI_KOBASHI_PPT_B1',2,'T','10258',0,'_FELIPE_YUKI_KOBASHI_PPT_B1.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(258,'_FERNANDA GONZAGA WALTER GUIDI, PPT_G3CT',2,'T','10259',0,'_FERNANDA GONZAGA WALTER GUIDI, PPT_G3CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(259,'_FERNANDA_GONZAGA_WALTER_GUIDI_PPT_G3CT',2,'T','10260',0,'_FERNANDA_GONZAGA_WALTER_GUIDI_PPT_G3CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(260,'_FILIPPO FRANCISCO SANTORO, PPT_G4BT',2,'T','10261',0,'_FILIPPO FRANCISCO SANTORO, PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(261,'_FILIPPO_FRANCISCO_SANTORO_PPT_G4BT',2,'T','10262',0,'_FILIPPO_FRANCISCO_SANTORO_PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(262,'_FRANCESCO CERIONI SPIROPULOS RODRIGUES, PPT_G4AM',2,'T','10263',0,'_FRANCESCO CERIONI SPIROPULOS RODRIGUES, PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(263,'_FRANCESCO_CERIONI_SPIROPULOS_RODRIGUES_PPT_G4AM',2,'T','10264',0,'_FRANCESCO_CERIONI_SPIROPULOS_RODRIGUES_PPT_G4AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(264,'_FRANCISCO BASILIO MARTINS, PPT_G4BT',2,'T','10265',0,'_FRANCISCO BASILIO MARTINS, PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(265,'_FRANCISCO CABRAL MAIA LUZ, PPT_G5BT',2,'T','10266',0,'_FRANCISCO CABRAL MAIA LUZ, PPT_G5BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(266,'_FRANCISCO CARBONE FIORESE, PPT_G4CT',2,'T','10267',0,'_FRANCISCO CARBONE FIORESE, PPT_G4CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(267,'_FRANCISCO_BASILIO_MARTINS_PPT_G4BT',2,'T','10268',0,'_FRANCISCO_BASILIO_MARTINS_PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(268,'_FRANCISCO_CABRAL_MAIA_LUZ_PPT_G5BT',2,'T','10269',0,'_FRANCISCO_CABRAL_MAIA_LUZ_PPT_G5BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(269,'_FRANCISCO_CARBONE_FIORESE_PPT_G4CT',2,'T','10270',0,'_FRANCISCO_CARBONE_FIORESE_PPT_G4CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(270,'_FREDERICO AZZUZ JANESCH, PPT_G3CT',2,'T','10271',0,'_FREDERICO AZZUZ JANESCH, PPT_G3CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(271,'_FREDERICO MACHADO FABICHAK, PPT_G3DT',2,'T','10272',0,'_FREDERICO MACHADO FABICHAK, PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(272,'_FREDERICO MATTA HARDER, PPT_G5AT',2,'T','10273',0,'_FREDERICO MATTA HARDER, PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(273,'_FREDERICO_AZZUZ_JANESCH_PPT_G3CT',2,'T','10274',0,'_FREDERICO_AZZUZ_JANESCH_PPT_G3CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(274,'_FREDERICO_MACHADO_FABICHAK_PPT_G3DT',2,'T','10275',0,'_FREDERICO_MACHADO_FABICHAK_PPT_G3DT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(275,'_FREDERICO_MATTA_HARDER_PPT_G5AT',2,'T','10276',0,'_FREDERICO_MATTA_HARDER_PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(276,'_GABRIEL BRANCO CARNEIRO, PPT_G1AT',2,'T','10277',0,'_GABRIEL BRANCO CARNEIRO, PPT_G1AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(277,'_GABRIEL FELDBERG, PPT_G5AM',2,'T','10278',0,'_GABRIEL FELDBERG, PPT_G5AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(278,'_GABRIEL FELIPE ALVES SILVA, PPT_G5BM',2,'T','10279',0,'_GABRIEL FELIPE ALVES SILVA, PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(279,'_GABRIEL GOMIDE ARIZA, PPT_G2BT',2,'T','10280',0,'_GABRIEL GOMIDE ARIZA, PPT_G2BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(280,'_GABRIEL GUTIERREZ NASCIMBENI, PPT_G3AM',2,'T','10281',0,'_GABRIEL GUTIERREZ NASCIMBENI, PPT_G3AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(281,'_GABRIEL KENJI YAMAJI, PPT_G2DM',2,'T','10282',0,'_GABRIEL KENJI YAMAJI, PPT_G2DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(282,'_GABRIEL MARTINS CANDIDO, PPT_G3DM  d',2,'T','10283',0,'_GABRIEL MARTINS CANDIDO, PPT_G3DM  d.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(283,'_GABRIEL MONTEIRO PEIXE, PPT_G5BT',2,'T','10284',0,'_GABRIEL MONTEIRO PEIXE, PPT_G5BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(284,'_GABRIEL MOREIRA COLELLA, PPT_G5BM',2,'T','10285',0,'_GABRIEL MOREIRA COLELLA, PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(285,'_GABRIEL MUNUERA NERI DOS SANTOS, PPT_G4CT',2,'T','10286',0,'_GABRIEL MUNUERA NERI DOS SANTOS, PPT_G4CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(286,'_GABRIEL PALORO BRISSI, PPT_G5BM',2,'T','10287',0,'_GABRIEL PALORO BRISSI, PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(287,'_GABRIEL PIO GABBAY, PPT_G3BT',2,'T','10288',0,'_GABRIEL PIO GABBAY, PPT_G3BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(288,'_GABRIEL POSTIGO RODRIGUES, PPT_G5BT',2,'T','10289',0,'_GABRIEL POSTIGO RODRIGUES, PPT_G5BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(289,'_GABRIEL_BRANCO_CARNEIRO_PPT_G1AT',2,'T','10290',0,'_GABRIEL_BRANCO_CARNEIRO_PPT_G1AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(290,'_GABRIEL_FELDBERG_PPT_G5AM',2,'T','10291',0,'_GABRIEL_FELDBERG_PPT_G5AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(291,'_GABRIEL_FELIPE_ALVES_SILVA_PPT_G5BM',2,'T','10292',0,'_GABRIEL_FELIPE_ALVES_SILVA_PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(292,'_GABRIEL_GOMIDE_ARIZA_PPT_G2BT',2,'T','10293',0,'_GABRIEL_GOMIDE_ARIZA_PPT_G2BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(293,'_GABRIEL_GUTIERREZ_NASCIMBENI_PPT_G3AM',2,'T','10294',0,'_GABRIEL_GUTIERREZ_NASCIMBENI_PPT_G3AM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(294,'_GABRIEL_KENJI_YAMAJI_PPT_G2DM',2,'T','10295',0,'_GABRIEL_KENJI_YAMAJI_PPT_G2DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(295,'_GABRIEL_MARTINS_CANDIDO_PPT_G3DM_d',2,'T','10296',0,'_GABRIEL_MARTINS_CANDIDO_PPT_G3DM_d.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(296,'_GABRIEL_MONTEIRO_PEIXE_PPT_G5BT',2,'T','10297',0,'_GABRIEL_MONTEIRO_PEIXE_PPT_G5BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(297,'_GABRIEL_MOREIRA_COLELLA_PPT_G5BM',2,'T','10298',0,'_GABRIEL_MOREIRA_COLELLA_PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(298,'_GABRIEL_MUNUERA_NERI_DOS_SANTOS_PPT_G4CT',2,'T','10299',0,'_GABRIEL_MUNUERA_NERI_DOS_SANTOS_PPT_G4CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(299,'_GABRIEL_PALORO_BRISSI_PPT_G5BM',2,'T','10300',0,'_GABRIEL_PALORO_BRISSI_PPT_G5BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(300,'_GABRIEL_PIO_GABBAY_PPT_G3BT',2,'T','10301',0,'_GABRIEL_PIO_GABBAY_PPT_G3BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(301,'_GABRIEL_POSTIGO_RODRIGUES_PPT_G5BT',2,'T','10302',0,'_GABRIEL_POSTIGO_RODRIGUES_PPT_G5BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(302,'_GABRIELA BREVE SCHEMANN, PPT_G4BT',2,'T','10303',0,'_GABRIELA BREVE SCHEMANN, PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(303,'_GABRIELA CARNEIRO CORDIOLI, PPT_G3BM',2,'T','10304',0,'_GABRIELA CARNEIRO CORDIOLI, PPT_G3BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(304,'_GABRIELA DRIGO SARRA FALSI, PPT_G5CT',2,'T','10305',0,'_GABRIELA DRIGO SARRA FALSI, PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(305,'_GABRIELA PRADO LANG, PPT_G4DM',2,'T','10306',0,'_GABRIELA PRADO LANG, PPT_G4DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(306,'_GABRIELA ROZO DUCATTI, PPT_G5BT',2,'T','10307',0,'_GABRIELA ROZO DUCATTI, PPT_G5BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(307,'_GABRIELA SASAZAKI TAKAHASHI, PPT_G4AT',2,'T','10308',0,'_GABRIELA SASAZAKI TAKAHASHI, PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(308,'_GABRIELA SILICANI CARDOSO CORRÊA, PPT_G5CT',2,'T','10309',0,'_GABRIELA SILICANI CARDOSO CORRÊA, PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(309,'_GABRIELA TONIOLO BUENO, PPT_G5AT',2,'T','10310',0,'_GABRIELA TONIOLO BUENO, PPT_G5AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(310,'_GABRIELA ZAMPIERI GALLARDO, PPT_G4DM',2,'T','10311',0,'_GABRIELA ZAMPIERI GALLARDO, PPT_G4DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(311,'_GABRIELA_BREVE_SCHEMANN_PPT_G4BT',2,'T','10312',0,'_GABRIELA_BREVE_SCHEMANN_PPT_G4BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(312,'_GABRIELA_CARNEIRO_CORDIOLI_PPT_G3BM',2,'T','10313',0,'_GABRIELA_CARNEIRO_CORDIOLI_PPT_G3BM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(313,'_GABRIELA_DRIGO_SARRA_FALSI_PPT_G5CT',2,'T','10314',0,'_GABRIELA_DRIGO_SARRA_FALSI_PPT_G5CT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(314,'_GABRIELA_PRADO_LANG_PPT_G4DM',2,'T','10315',0,'_GABRIELA_PRADO_LANG_PPT_G4DM.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(315,'_GABRIELA_ROZO_DUCATTI_PPT_G5BT',2,'T','10316',0,'_GABRIELA_ROZO_DUCATTI_PPT_G5BT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17'),(316,'_GABRIELA_SASAZAKI_TAKAHASHI_PPT_G4AT',2,'T','10317',0,'_GABRIELA_SASAZAKI_TAKAHASHI_PPT_G4AT.jpg',1,NULL,'Site','2019-05-20 11:41:17','2019-05-20 11:41:17');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `students` with 316 row(s)
--

--
-- Table structure for table `teams`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `period` enum('M','T','N') COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Infantil','Fundamental','Médio','') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `teams` VALUES (1,'G1','','T','Infantil',NULL,1,'2019-05-20 11:41:16','2019-05-20 11:41:16'),(2,'G2','','T','Infantil',NULL,1,'2019-05-20 11:41:17','2019-05-20 11:41:17'),(3,'G4','','T','Infantil',NULL,1,'2019-05-20 11:41:17','2019-05-20 11:41:17'),(4,'G3','','T','Infantil',NULL,1,'2019-05-20 11:41:17','2019-05-20 11:41:17'),(5,'B1','','T','Infantil',NULL,1,'2019-05-20 11:41:17','2019-05-20 11:41:17');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `teams` with 5 row(s)
--

--
-- Table structure for table `texts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `texts`
--

LOCK TABLES `texts` WRITE;
/*!40000 ALTER TABLE `texts` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `texts` VALUES (1,'other','Pós Venda Site - Login','Olá, s<span>eja bem vindo ao site de compras de Fotolembranças da ABDesignFotos.<br><br></span>Aqui você poderá pagar o kit recebido e também comprar novos produtos personalizados. <br><br>Entre agora mesmo com a Chave de Acesso que recebeu na agenda do seu filho para visualizar a fotos feitas na escola.<br><span><br>O Prazo para compra já está finalizado, portanto neste momento você poderá comprar o Kit ou qualquer item de Fotolembranças para receber em Novembro na Mochila da Criança, porém não será permitido \"Receber sem Compromisso\".<br><br></span><span>Aproveite!!<br></span><span><br>Qualquer dúvida, favor entrar em contato pelo Whatsapp.<br></span><span><br>PS: Estamos tendo algumas instabilidades com acesso via Smartphone. <br>Peço a gentileza de acessar via Computador.<br><br></span>Grato,<br><u>André Bittencourt - ABDesignFotos<br></u>11 9 6482-3000<br><br>&nbsp;',1,'2018-12-21 07:29:15','2019-02-13 15:56:19'),(2,'products','Padrão Site - Produtos','<span>Para selecionar uma opção abaixo, basta clicar no sinal de \"Check\" no lado direito.<br><br><b>Opção 01</b> é para adquirir somente o kit COMPLETO ganhando um Porta retrato de Brinde. Você pode também optar por adquirir produtos extra para complementar o pacote.<br><b>Opção 02</b> é para adquirir apenas itens avulsos.<br></span><b>Opção 03</b>&nbsp;para receber o kit para apreciação sem compromisso, podendo optar pela compra ou devolução depois de recebê-lo.<br><h5>RECEBER SEM COMPROMISSO</h5>Selecione a Opção 03 e clique no botão abaixo com essa opção. Neste caso você receberá o kit para apreciação e poderá escolher se quer ficar com todos os ítens ou comprar somente alguns itens do pacote. Caso não tenha interesse em ficar com o kit, pode devolvê-lo na escola fechado e lacrado, da mesma forma que o recebeu.<br>Para esta opção, não há entrega do brinde&nbsp; (Porta retrato) e o desconto será menor.',1,'2018-12-29 11:12:19','2019-02-28 15:55:10'),(4,'payment','Carrinho de Compras','Os produtos serão entregues na escola em Novembro. Ocorrendo algum atraso na entrega, entraremos em contato para avisar. <br><br>Caso você não consiga efetuar o pagamento por aqui, favor entrar em contato com André Bittencourt <br>(WhatsApp: 11 96482-3000) <br>',1,'2018-12-21 09:31:16','2019-02-20 13:30:33'),(13,'website_inactive','Padrão Site - Site Inativo','<div>Olá,<br><br></div><div>Infelizmente o prazo de compras foi finalizado!&nbsp;<img alt=\"\"><img alt=\"\"><img alt=\"\"><img alt=\"\"><img alt=\"\"><br>Teste!<a target=\"\" rel=\"\"></a></div>',1,'2019-01-10 06:17:55','2019-01-13 05:32:45'),(14,'not-receive','Não Receber o Kit','<blockquote></blockquote>Você optou previamente por não receber o Kit de Fotolembranças deste ano.<br><blockquote></blockquote>Deseja modificar essa opção?<br><blockquote></blockquote>Se quiser manter essa opção, clique em \"NÃO\".<br>Caso tenha mudado de idéia e queira ver novamente as opções de compra, clique em \"SIM\".',1,'2019-01-18 10:25:18','2019-02-12 17:21:09'),(15,'login','Pré-Venda Site-Login','Olá, s<span>eja bem-vindo ao site de compras de Fotolembranças da ABDesignFotos.<br><br></span>Aqui você terá acesso às fotos (Individual e Turma)&nbsp;feitas na escola este ano.&nbsp;<br><br>Entre agora mesmo com a Chave de Acesso que recebeu na agenda do seu filho pra fazer a sua escolha.<br><span><br>O site ficará disponível para&nbsp;seu pedido até o dia 10 de Agosto de 2019.<br><br></span><span>Lembre-se que somente receberá o kit ou&nbsp; produtos desejados quem acessar aqui e fazer a sua opção.<br><br>Aproveite!!<br></span><span><br>Qualquer dúvida, favor entrar em contato pelo WhatsApp.<br></span><br>Grato,<br><u>André Bittencourt - ABDesignFotos<br></u>11 9 6482-3000',1,'2019-02-13 15:54:24','2019-02-20 13:01:20');
/*!40000 ALTER TABLE `texts` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `texts` with 6 row(s)
--

--
-- Table structure for table `users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `privilege` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
SET autocommit=0;
INSERT INTO `users` VALUES (1,'Administrador','admin@admin.com',NULL,'b790c60865f4823c876702d2b4ecb9df.png',1,1,NULL,'$2a$10$H4c4b7Zjsy6ifwLaivwOjeonICvP5oldDhZTi24eQPcTbBhEBlnfK','CXvk76WBNFQ56eDQBbycnwEYTqUNRiKEYUrRXZOgOI60AbBFzgKMEooIAAO4','2018-12-31 18:44:53','2019-02-01 13:55:33'),(2,'Ana Cristina Sztengrat Machado Cezar','financeiro@abdesignfotos.com',NULL,'c5b5c28502a585e6ee82b266f4b5a0fc.png',0,1,NULL,'$2y$10$CuTcw7JXVR.ol4aJvmX/XeIkYsXydhH/krdLWwmwQgqqWLzAVIpAe',NULL,'2019-02-01 13:53:29','2019-02-12 18:33:32'),(3,'Aline Araújo','a.araujo@abdesignfotos.com',NULL,'091a5882b2d4eee8c9624ba010c934e2.png',0,1,NULL,'$2y$10$8yAerkyEsp/xjCHIABR0fuINtH1GPIyyAKlXaBVo6sVus5PoZ0eOi',NULL,'2019-02-01 13:54:09','2019-02-01 13:56:15'),(4,'Thiago','thicsilva@yahoo.com.br',NULL,NULL,1,1,NULL,'$2y$10$SyRRHLcev0qslPw0aij6..64QwidJnFL1UpK6Jt2JHn49s8OX7gzy',NULL,'2019-05-16 11:43:57','2019-05-16 11:43:59');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
COMMIT;

-- Dumped table `users` with 4 row(s)
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on: Tue, 30 Jul 2019 08:13:40 -0300
