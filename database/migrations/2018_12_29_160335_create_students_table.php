<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('team_id');
            $table->string('period');
            $table->string('access_key');
            $table->decimal('discount', 5, 2)->default(0);
            $table->string('photo');
            $table->boolean('status')->default(true);
            $table->string('original_name')->nullable();
            $table->enum('receive_without_commitment',['Sim', 'Não', 'Site']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
