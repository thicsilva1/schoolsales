<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->decimal('price', 8, 2);
            $table->decimal('old_price', 8, 2);
            $table->decimal('discount', 5, 2);
            $table->string('image');
            $table->integer('display_order')->default(0);
            $table->boolean('kindergarten')->default(false);
            $table->boolean('elementary_school')->default(false);
            $table->boolean('high_school')->default(false);
            $table->boolean('all_type')->default(false);
            $table->boolean('status')->default(true);
            $table->enum('receive_without_commitment',['Sim', 'Não', 'Site']);
            $table->boolean('allow_extra_products')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kits');
    }
}
