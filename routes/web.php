<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Rotas do site
 */

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;


Route::namespace('Site')->group(function(){
    Route::get('/atualizar-pagseguro', 'OrdersController@queryPagSeguro');
    Route::post('/notifica-pagseguro', 'OrdersController@updateWithNotification')->name('pagseguro.notify');

    Route::group(['middleware'=>'checkStudent'], function(){
        Route::get('/', 'HomeController@index')->name('home');
        Route::get('/confirm-student/{student}', 'HomeController@confirmStudent')->name('confirmStudent');
        Route::get('/student', 'HomeController@getStudent')->name('student');
        Route::get('/destroy', 'HomeController@destroy')->name('destroy');
        Route::post('/relative', 'HomeController@createRelative')->name('create.relative');
        Route::post('/update-student/{student}', 'HomeController@updateStudent')->name('update.student');
        Route::post('/signout', 'HomeController@signout')->name('signout');

        Route::post('/create-pagseguro', 'OrdersController@createPagSeguro')->name('createPagSeguro');
        Route::post('/create-payment', 'OrdersController@createPayment')->name('createPayment');
        Route::get('/pagseguro/{order}', 'OrdersController@confirmedPagSeguro')->name('pagseguroConfirmed');
        Route::post('/not-receive', 'OrdersController@notReceive')->name('notReceive');
        Route::post('/checkout', 'OrdersController@checkout')->name('checkout');
        Route::post('/checkout-without', 'OrdersController@checkoutWithout')->name('checkout-without');
        Route::get('/view-order/{order}', 'OrdersController@viewOrder')->name('view-order');
        Route::post('/apply-coupon', 'OrdersController@applyCoupon')->name('applyCoupon');
        Route::post('/continue-buy', 'OrdersController@continueBuy')->name('continueBuy');
        Route::delete('/cancel-order', 'OrdersController@cancelOrder')->name('cancelOrder');
        Route::patch('checkout/update-kit/{kit}', 'OrdersController@updateKit')->name('cart.updatekit');
        Route::delete('checkout/kit/{kit}', 'OrdersController@deleteKit')->name('cart.deletekit');
        Route::patch('checkout/update-product/{product}', 'OrdersController@updateProduct')->name('cart.updateproduct');
        Route::delete('checkout/product/{product}', 'OrdersController@deleteProduct')->name('cart.deleteproduct');

    });
    Route::get('/auth', 'HomeController@auth')->name('auth');
    Route::get('/check-key', 'HomeController@checkKey')->name('checkKey');
    Route::post('/recovery-key', 'HomeController@recoveryKey')->name('recovery.key');
});

/**
 * Rotas do Painel Administrativo
 */

Route::prefix('painel')->namespace('Admin')->middleware('auth')->group(function () {

    Route::get('/', 'HomeController@index')->name('panel.index');
    Route::group(['prefix' => 'alunos'], function(){
        Route::get('/', 'StudentsController@index')->name('students.index');
        Route::get('/novo', 'StudentsController@create')->name('students.create');
        Route::post('/', 'StudentsController@store')->name('students.store');
        Route::get('/editar/{student}', 'StudentsController@edit')->name('students.edit');
        Route::patch('/{student}', 'StudentsController@update')->name('students.update');
        Route::patch('/status/{student}', 'StudentsController@updateStatus')->name('students.status');
        Route::delete('/{student}', 'StudentsController@destroy')->name('students.destroy');
        Route::post('/import', 'StudentsController@import')->name('students.import');
        Route::post('/image/{student}', 'StudentsController@destroyImage')->name('students.destroy.image');
    });
    Route::group(['prefix' => 'produtos'], function(){
        Route::get('/', 'ProductsController@index')->name('products.index');
        Route::get('/novo', 'ProductsController@create')->name('products.create');
        Route::post('/', 'ProductsController@store')->name('products.store');
        Route::get('/editar/{product}', 'ProductsController@edit')->name('products.edit');
        Route::patch('/{product}', 'ProductsController@update')->name('products.update');
        Route::patch('/status/{product}', 'ProductsController@updateStatus')->name('products.status');
        Route::delete('/{product}', 'ProductsController@destroy')->name('products.destroy');
        Route::post('/image/{product}', 'ProductsController@destroyImage')->name('products.destroy.image');
    });
    Route::group(['prefix' => 'kits'], function(){
        Route::get('/', 'KitsController@index')->name('kits.index');
        Route::get('/novo', 'KitsController@create')->name('kits.create');
        Route::post('/', 'KitsController@store')->name('kits.store');
        Route::get('/editar/{kit}', 'KitsController@edit')->name('kits.edit');
        Route::patch('/{kit}', 'KitsController@update')->name('kits.update');
        Route::patch('/status/{kit}', 'KitsController@updateStatus')->name('kits.status');
        Route::delete('/{kit}', 'KitsController@destroy')->name('kits.destroy');
        Route::post('/image/{kit}', 'KitsController@destroyImage')->name('kits.destroy.image');
    });
    Route::group(['prefix' => 'textos'], function(){
        Route::get('/', 'TextsController@index')->name('texts.index');
        Route::get('/novo', 'TextsController@create')->name('texts.create');
        Route::post('/', 'TextsController@store')->name('texts.store');
        Route::get('/editar/{text}', 'TextsController@edit')->name('texts.edit');
        Route::patch('/{text}', 'TextsController@update')->name('texts.update');
        Route::patch('/status/{text}', 'TextsController@updateStatus')->name('texts.status');
        Route::delete('/{text}', 'TextsController@destroy')->name('texts.destroy');
    });
    Route::group(['prefix' => 'usuarios'], function(){
        Route::group(['middleware' => 'isAdmin'], function(){
            Route::get('/', 'UsersController@index')->name('users.index');
            Route::get('/novo', 'UsersController@create')->name('users.create');
            Route::post('/', 'UsersController@store')->name('users.store');
            Route::get('/editar/{user}', 'UsersController@edit')->name('users.edit');
            Route::patch('/status/{user}', 'UsersController@updateStatus')->name('users.status');
            Route::delete('/{user}', 'UsersController@destroy')->name('users.destroy');
        });
        Route::post('/image/{user}', 'UsersController@destroyImage')->name('users.destroy.image');
        Route::get('/senha', 'UsersController@editPassword')->name('users.edit.password');
        Route::patch('/senha/update', 'UsersController@updatePassword')->name('users.update.password');
        Route::get('/perfil','UsersController@editProfile')->name('users.edit.profile');
        Route::patch('/{user}', 'UsersController@update')->name('users.update');
    });
    Route::group(['prefix' => 'turmas'], function(){
        Route::get('/', 'TeamsController@index')->name('teams.index');
        Route::get('/novo', 'TeamsController@create')->name('teams.create');
        Route::post('/', 'TeamsController@store')->name('teams.store');
        Route::get('/editar/{team}', 'TeamsController@edit')->name('teams.edit');
        Route::patch('/{team}', 'TeamsController@update')->name('teams.update');
        Route::patch('/status/{team}', 'TeamsController@updateStatus')->name('teams.status');
        Route::delete('/{team}', 'TeamsController@destroy')->name('teams.destroy');
        Route::post('/image/{team}', 'TeamsController@destroyImage')->name('teams.destroy.image');
    });
    Route::group(['prefix' => 'cupons'], function(){
        Route::get('/', 'CouponsController@index')->name('coupons.index');
        Route::get('/novo', 'CouponsController@create')->name('coupons.create');
        Route::post('/', 'CouponsController@store')->name('coupons.store');
        Route::get('/editar/{coupon}', 'CouponsController@edit')->name('coupons.edit');
        Route::patch('/{coupon}', 'CouponsController@update')->name('coupons.update');
        Route::patch('/status/{coupon}', 'CouponsController@updateStatus')->name('coupons.status');
        Route::delete('/{coupon}', 'CouponsController@destroy')->name('coupons.destroy');
    });

    Route::group(['prefix' => 'logs'], function(){
        Route::get('/', 'LogsController@index')->name('logs.index');
    });

    Route::group(['prefix' => 'pedidos'], function(){
        Route::group(['middleware' => 'isAdmin'], function(){

        });
        Route::get('/editar/{order}', 'OrdersController@edit')->name('orders.edit');
        Route::delete('/{order}', 'OrdersController@destroy')->name('orders.destroy');
        Route::patch('/pay/{order}', 'OrdersController@pay')->name('orders.pay');
        Route::patch('/update-status/{order}', 'OrdersController@updateStatus')->name('orders.update.status');

        Route::get('/order/kits', 'OrdersController@getKits')->name('orders.kits');
        Route::post('{order}/kit', 'OrdersController@storeKit')->name('orders.store.kit');
        Route::delete('/order/{order}/kit/{kit}', 'OrdersController@destroyKit')->name('orders.kit.destroy');
        Route::patch('{order}/update/kit/', 'OrdersController@updateKit')->name('orders.update.kit');

        Route::get('/order/products', 'OrdersController@getProducts')->name('orders.products');
        Route::post('{order}/product', 'OrdersController@storeProduct')->name('orders.store.product');
        Route::delete('order/{order}/product/{product}', 'OrdersController@destroyproduct')->name('orders.product.destroy');
        Route::patch('{order}/update/product', 'OrdersController@updateProduct')->name('orders.update.product');

        Route::get('/order/students', 'OrdersController@getStudents')->name('orders.students');
        Route::patch('/editar/{order}', 'OrdersController@updateRelative')->name('orders.update.relative');
        Route::get('/', 'OrdersController@index')->name('orders.index');
        Route::post('/export', 'OrdersController@export')->name('orders.export');
        Route::get('/filter/{filter}', 'OrdersController@filter')->name('orders.filter');
    });

    Route::group(['prefix' => 'configuracoes'], function(){
        Route::group(['middleware' => 'isAdmin'], function(){

            Route::get('/', 'SettingsController@index')->name('settings.index');
            Route::patch('/{setting}', 'SettingsController@update')->name('settings.update');
            Route::get('/backup', 'SettingsController@backup')->name('settings.backup');
            Route::post('/backup/download', 'SettingsController@downloadBackup')->name('settings.backup.download');
            Route::get('/restaurar', 'SettingsController@showRestore')->name('settings.restore');
            Route::post('/restaurar', 'SettingsController@restore')->name('settings.restore.exec');
            Route::get('/zerar', 'SettingsController@showEmpty')->name('settings.empty');
            Route::post('/zerar', 'SettingsController@emptyDatabase')->name('settings.empty.exec');
        });
        Route::get('/sincronizar-fotos', 'SettingsController@syncPhotos')->name('settings.sync.photos');
        Route::get('/eventos', 'SettingsController@log')->name('settings.log');
    });

    Route::group(['prefix' => 'nao-receber'], function(){
        Route::get('/', 'NotReceivesController@index')->name('notreceive.index');
        Route::get('/novo', 'NotReceivesController@create')->name('notreceive.create');
        Route::post('/', 'NotReceivesController@store')->name('notreceive.store');
        Route::get('/editar/{nr}', 'NotReceivesController@edit')->name('notreceive.edit');
        Route::patch('/{nr}', 'NotReceivesController@update')->name('notreceive.update');
        Route::delete('/{nr}', 'NotReceivesController@destroy')->name('notreceive.destroy');
    });


});

Auth::routes(['register'=> false]);
